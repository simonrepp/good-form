use clap::Clap;
use gfx_hal::Instance;
use winit::{
    dpi::LogicalSize,
    event::{
        ElementState,
        Event,
        KeyboardInput as KeyboardInputData,
        MouseButton,
        MouseScrollDelta,
        VirtualKeyCode,
        WindowEvent
    },
    event_loop::{ControlFlow, EventLoop},
    window::{CursorIcon, WindowBuilder}
};

#[cfg(feature = "gl")]
use {
    gfx_backend_gl as gfx_backend,
    gfx_hal::format::{AsFormat, Rgba8Srgb}
};

#[cfg(feature = "metal")]
use gfx_backend_metal as gfx_backend;

#[cfg(feature = "vulkan")]
use gfx_backend_vulkan as gfx_backend;

mod args;
mod cli;
mod constants;
mod engine;
mod input_state;

use args::Args;
use engine::{
    regions::{Button, Region},
    state::{cursor::CursorStyle, operation::Operation},
    Engine
};
use input_state::InputState;

fn main() {
    env_logger::init();

    let args = Args::parse();

    if args.list_hosts {
        cli::list_hosts();
        return;
    } else if args.list_devices {
        cli::list_devices();
        return;
    }

    let event_loop = EventLoop::new();
    let window_builder = WindowBuilder::new().with_title("Good Form")
                                             .with_inner_size(LogicalSize {
                                                 width: 1280.0,
                                                 height: 720.0
                                             });

     #[cfg(not(feature = "gl"))]
     let (window, instance, mut adapters, surface) = {
         let window = window_builder.build(&event_loop).unwrap();
         let instance = gfx_backend::Instance::create("Good Form", 1).expect("Failed to create an instance!");
         let surface = unsafe {
             instance.create_surface(&window).expect("Failed to create a surface!")
         };
         let adapters = instance.enumerate_adapters();

         (window, Some(instance), adapters, surface)
     };

    #[cfg(feature = "gl")]
    let (window, instance, mut adapters, surface) = {
        let (window, surface) = {
            let context_builder = gfx_backend::config_context(
                gfx_backend::glutin::ContextBuilder::new(),
                Rgba8Srgb::SELF,
                None
            ).with_vsync(true);
            let windowed_context = context_builder.build_windowed(window_builder, &event_loop).unwrap();
            let (context, window) = unsafe {
                windowed_context.make_current()
                                .expect("Unable to make context current")
                                .split()
            };
            let surface = gfx_backend::Surface::from_context(context);
            (window, surface)
        };

        let adapters = surface.enumerate_adapters();
        (window, None, adapters, surface)
    };

    let adapter = adapters.remove(0);

    let mut engine = Engine::initialize(instance, surface, adapter, args);
    let mut input_state = InputState::cleared();

    event_loop.run(move |event, _, control_flow| {
        use WindowEvent::*;

        *control_flow = ControlFlow::Poll;

        // TODO: As an optimization at some point we could conditionally (in some application states)
        //       set the control flow to Wait, i.e. not continuously running the event loop but
        //       instead idling until there are new events to process (which then could change the
        //       control flow back into a continuous loop (e.g. when triggering playback where constant
        //       re-rendering is needed)
        // *control_flow = ControlFlow::Wait;

        match event {
            Event::WindowEvent {
                event,
                ..
            } => match event {
                Resized(physical_size) => {
                    engine.update_window_size(physical_size.width, physical_size.height);
                }
                CursorMoved { position, .. } => {
                    engine.update_cursor_position(position.x as f32, position.y as f32);

                    if input_state.left_drag {
                        match engine.state.operation {
                            Operation::MovingMarker(marker) => engine.move_marker_to_cursor(marker),
                            Operation::Seeking => engine.seek_to_cursor(),
                            Operation::Selecting => engine.select_to_cursor(),
                            _ => ()
                        }
                    }
                }
                MouseInput {
                    button: MouseButton::Left,
                    state: ElementState::Pressed,
                    ..
                } => {
                    let is_double_click = input_state.left_press();

                    if engine.state.operation == Operation::None {
                        match engine.state.cursor.region {
                            Region::Button(button) => {
                                engine.begin_pressing_button(button);
                            }
                            Region::Marker(marker) => {
                                if is_double_click {
                                    engine.play_from_marker(marker);
                                } else {
                                    engine.begin_moving_marker(marker);
                                }
                            }
                            Region::Waveform => {
                                if input_state.shift {
                                    engine.begin_selecting();
                                } else {
                                    engine.begin_seeking();
                                }
                            }
                            _ => ()
                        }
                    }
                }
                MouseInput {
                    button: MouseButton::Left,
                    state: ElementState::Released,
                    ..
                } => {
                    input_state.left_drag = false;

                    match engine.state.operation {
                        Operation::MovingMarker(_) => engine.end_moving_marker(),
                        Operation::PressingButton(pressed_button) => {
                            match (&engine.state.cursor.region, pressed_button) {
                                (Region::Button(Button::Engine), Button::Engine) => {
                                    if input_state.shift {
                                        engine.restart_playback_engine(true);
                                    } else {
                                        engine.restart_playback_engine(false);
                                    }
                                }
                                (Region::Button(Button::Play), Button::Play) => {
                                    engine.toggle_playing();
                                }
                                (Region::Button(Button::Stop), Button::Stop) => {
                                    engine.stop_playing();
                                }
                                _ => ()
                            }

                            engine.end_pressing_button();
                        }
                        Operation::Seeking => engine.end_seeking(),
                        Operation::Selecting => engine.end_selecting(),
                        _ => ()
                    }
                }
                MouseWheel {
                    delta: MouseScrollDelta::LineDelta(_x, y),
                    ..
                } => {
                    if engine.state.operation == Operation::None {
                        if engine.state.cursor.region == Region::Waveform {
                            if y > 0.0 {
                                engine.select_previous_location();
                            } else {
                                engine.select_next_location();
                            }
                        }
                    }

                    // TODO: Re-use at some point
                    // if y > 0.0 {
                    //     state.zoom += 0.5;
                    // } else {
                    //     state.zoom -= 0.5;
                    //     if state.zoom < 1.0 { state.zoom = 1.0; }
                    // }
                }
                KeyboardInput {
                    input: KeyboardInputData {
                        state: ElementState::Pressed,
                        virtual_keycode: Some(keycode),
                        ..
                    },
                    ..
                } => {
                    use VirtualKeyCode::*;

                    if engine.state.operation == Operation::None {
                        match keycode {
                            F5 => {
                                if input_state.shift {
                                    engine.restart_playback_engine(true);
                                } else {
                                    engine.restart_playback_engine(false);
                                }
                            }
                            Home => {
                                engine.jump_to_start();
                            }
                            End => {
                                engine.jump_to_end();
                            }
                            Return => {
                                if input_state.shift {
                                    engine.open_selected_location_and_queue_following();
                                } else {
                                    engine.open_selected_location();
                                }
                            }
                            Up => {
                                if input_state.alt {
                                    engine.ascend_location();

                                } else {
                                    engine.select_previous_location();
                                }
                            }
                            Down => {
                                if input_state.alt {
                                    engine.descend_location();
                                } else {
                                    engine.select_next_location();
                                }
                            }
                            Left => {
                                if input_state.ctrl {
                                    engine.jump_seconds_backward(20);
                                } else {
                                    engine.jump_seconds_backward(5);
                                }
                            }
                            Right => {
                                if input_state.ctrl {
                                    engine.jump_seconds_forward(20);
                                } else {
                                    engine.jump_seconds_forward(5);
                                }
                            }
                            Space => {
                                engine.toggle_playing();
                            }
                            Tab => {
                                input_state.tab = true;

                                engine.begin_transport_swipe();
                            }
                            LShift => {
                                input_state.shift = true;

                                if input_state.left_drag {
                                    engine.begin_selecting();
                                }
                            }
                            LAlt => {
                                input_state.alt = true;
                            }
                            LControl => {
                                input_state.ctrl = true;
                            }
                            LWin => {
                                input_state.super_left = true;
                            }
                            RWin => {
                                input_state.super_right = true;
                            }
                            Add => {
                                if input_state.ctrl {
                                    engine.increase_interface_scaling();
                                }
                            }
                            Subtract => {
                                if input_state.ctrl {
                                    engine.decrease_interface_scaling();
                                }
                            }
                            E => {
                                if input_state.ctrl {
                                    engine.export();
                                }
                            }
                            Q => {
                                #[cfg(not(target_os = "macos"))]
                                let modifier = input_state.ctrl;

                                #[cfg(target_os = "macos")]
                                let modifier = input_state.ctrl ||
                                               input_state.super_left ||
                                               input_state.super_right;

                                if modifier {
                                    engine.persist_state();
                                    *control_flow = ControlFlow::Exit;
                                } else {
                                    engine.queue_selected_file();
                                }
                            }
                            R => {
                                engine.play_and_repeat();
                            }
                            T => {
                                engine.toggle_theme();
                            }
                            _ => ()
                        }
                    }
                }

                KeyboardInput {
                    input: KeyboardInputData {
                        state: ElementState::Released,
                        virtual_keycode: Some(keycode),
                        ..
                    },
                    ..
                } => {
                    use VirtualKeyCode::*;

                    match keycode {
                        Tab => {
                            input_state.tab = false;

                            if let Operation::TransportSwipe(_) = engine.state.operation {
                                engine.end_transport_swipe();
                            }
                        }
                        LShift => {
                            input_state.shift = false;
                        }
                        LAlt => {
                            input_state.alt = false;
                        }
                        LControl => {
                            input_state.ctrl = false;
                        }
                        LWin => {
                            input_state.super_left = false;
                        }
                        RWin => {
                            input_state.super_right = false;
                        }
                        _ => ()
                    }
                }
                HoveredFile(path) => {
                    engine.register_hovered_location(path);
                }
                DroppedFile(_) => {
                    engine.open_hovered_location();
                }
                HoveredFileCancelled => {
                    engine.discard_hovered_location();
                }
                CloseRequested => {
                    engine.persist_state();
                    *control_flow = ControlFlow::Exit;
                }
                ReceivedCharacter(_character) => {
                    // TODO: Draft a typing state and record input
                }
                _ => ()
            }
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                engine.receive_decoded(&window);
                engine.receive_playback_responses();
                engine.advance_playhead();

                if let Some(cursor_style) = engine.update_cursor_style() {
                    let cursor_icon = match cursor_style {
                        CursorStyle::HoveringButton => CursorIcon::Hand,
                        CursorStyle::HoveringMarker => CursorIcon::Hand,
                        CursorStyle::MovingMarker => CursorIcon::Move,
                        CursorStyle::Pointing => CursorIcon::Default,
                        CursorStyle::SelectingRange => CursorIcon::Text,
                        CursorStyle::SwipingLeft => CursorIcon::WResize,
                        CursorStyle::SwipingRight => CursorIcon::EResize,
                        CursorStyle::SwipingUp => CursorIcon::NResize,
                        CursorStyle::SwipingDown => CursorIcon::SResize
                    };

                    window.set_cursor_icon(cursor_icon);
                }

                engine.render();
            }
            _ => ()
        }
    });
}
