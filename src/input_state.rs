use std::time::Instant;

const DOUBLE_CLICK_THRESHOLD_SECONDS: f32 = 0.5;

pub struct InputState {
    pub alt: bool,
    pub ctrl: bool,
    pub last_left_pressed_instant: Option<Instant>,
    pub left_drag: bool,
    pub shift: bool,
    pub super_left: bool,
    pub super_right: bool,
    pub tab: bool
}

impl InputState {
    pub fn cleared() -> InputState {
        InputState {
            alt: false,
            ctrl: false,
            last_left_pressed_instant: None,
            left_drag: false,
            shift: false,
            super_left: false,
            super_right: false,
            tab: false
        }
    }

    /// Register a left mouse button press
    ///
    /// If it is recognized as (the second click of) a double click,
    /// this returns true and resets the double click state, otherwise
    /// it initiates a left_drag and registers the instant of what could
    /// turn out to be the first click of a double click.
    pub fn left_press(&mut self) -> bool {
        if let Some(instant) =  self.last_left_pressed_instant {
            if instant.elapsed().as_secs_f32() < DOUBLE_CLICK_THRESHOLD_SECONDS {
                self.last_left_pressed_instant = None;

                return true;
            }
        }

        self.last_left_pressed_instant = Some(Instant::now());
        self.left_drag = true;

        false
    }
}
