#[allow(dead_code)]
pub enum LogarithmicBase {
    Two,
    Ten
}

pub struct PeakPairIntermediate {
    pub pos: f32,
    pub neg: f32
}

/// An averaged down level range for a certain window of samples
///
/// I.e. the averaged maximum negative and positive amplitude value for that window.
/// Note that neg contains a positive value and values are stored in u16
/// for more efficient transfer to GPU texture memory.
#[repr(C)]
pub struct PeakPairAligned32 {
    pub pos: u16,
    pub neg: u16
}

// TODO: This is configurable to facilitate quick iteration and R&D on
//       different waveform representations, eventually it will go away
//       or be made dynamically customizable by the user through the interface.
const LOGARITHMIC_BASE: LogarithmicBase = LogarithmicBase::Ten;

pub fn downsample(samples: &Vec<Vec<f32>>) -> Vec<Vec<PeakPairAligned32>> {
    samples.iter()
           .map(|channel| downsample_channel(&channel))
           .collect()
}

/// Takes the samples of a channel and applies the following processing:
/// - Determine the largest absolute amplitude among all the sample values
/// - Group every [n] samples into a window, for which the average positive and negative amplitude is stored
/// - Determine the largest absolute average amplitude among all calculated windows
/// - For all windows the averaged amplitudes are now upscaled again so that the maximum absolute window amplitude
///   is identical to the largest absolute amplitude found in all discrete samples
fn downsample_channel(samples: &Vec<f32>) -> Vec<PeakPairAligned32> {
    let window_size = samples.len() / 4096; // TODO: Couple with renderer waveform image width asap

    let mut intermediate_pairs: Vec<PeakPairIntermediate> = vec![];

    let mut window_samples = 0;
    let mut window_accumulated_neg = 0.0;
    let mut window_accumulated_pos = 0.0;

    let mut sample_abs_max: f32 = 0.0;
    let mut window_abs_max: f32 = 0.0;

    for amplitude in samples {
        sample_abs_max = sample_abs_max.max(amplitude.abs());

        if window_samples > window_size {
            let intermediate_pair = PeakPairIntermediate {
                neg: window_accumulated_neg / window_samples as f32,
                pos: window_accumulated_pos / window_samples as f32,
            };

            window_abs_max = window_abs_max.max(intermediate_pair.pos).max(intermediate_pair.neg);

            intermediate_pairs.push(intermediate_pair);

            window_samples = 0;
            window_accumulated_neg = 0.0;
            window_accumulated_pos = 0.0;
        }

        if *amplitude >= 0.0 {
            window_accumulated_pos += amplitude;
        } else {
            window_accumulated_neg -= amplitude;
        }

        window_samples += 1;
    }

    let upscale = (sample_abs_max / window_abs_max) * std::u16::MAX as f32;

    let adjusted_pairs = intermediate_pairs.iter()
                                           .map(|pair| {
                                               match LOGARITHMIC_BASE {
                                                   LogarithmicBase::Two => PeakPairAligned32 {
                                                       neg: ((pair.neg * 2.0 + 1.0).log2() * upscale) as u16,
                                                       pos: ((pair.pos * 2.0 + 1.0).log2() * upscale) as u16,
                                                   },
                                                   LogarithmicBase::Ten => PeakPairAligned32 {
                                                       neg: ((pair.neg * 10.0 + 1.0).log10() * upscale) as u16,
                                                       pos: ((pair.pos * 10.0 + 1.0).log10() * upscale) as u16,
                                                   }
                                               }

                                            })
                                            .collect();

    adjusted_pairs
}
