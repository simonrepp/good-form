use std::{fs, sync::Arc};

use crate::args::Args;
use crate::engine::decoder::DecodedAudio;

mod persisted_state;

pub mod cursor;
pub mod location;
pub mod locations;
pub mod operation;
pub mod playback_device;
pub mod swipe;
pub mod theme;
pub mod timecode_range;

use cursor::Cursor;
use operation::Operation;
use location::HoveredLocation;
use persisted_state::PersistedState;
use playback_device::PlaybackDevice;
use theme::Theme;

pub use locations::Locations;
pub use timecode_range::TimecodeRange;

pub struct WindowSize {
    pub width: u32,
    pub height: u32
}

pub struct State {
    pub audio: Option<Arc<DecodedAudio>>,
    pub cursor: Cursor,
    pub end_timecode: f64,
    pub hovered_location: Option<HoveredLocation>,
    pub interface_scaling: f32,
    pub locations: Locations,
    pub operation: Operation,
    pub pause_point_timecode: Option<f64>,
    pub playback_device: PlaybackDevice,
    pub play_point_timecode: Option<f64>,
    pub playhead_timecode: f64,
    pub playing: bool,
    pub repeat: bool,
    pub selected_range: Option<TimecodeRange>,
    pub theme: Theme,
    pub window_size: WindowSize
}

impl State {
    /// Initializes the global application state by setting up
    /// static defaults, as well as handling the optional path
    /// argument (either a file or directory) for opening a
    /// file or directory right at startup.
    pub fn initialize(args: Args) -> State {
        let persisted_state = if args.factory_reset {
            PersistedState::purge();
            None
        } else {
            PersistedState::retrieve()
        };

        let path_arg = match args.path {
            Some(path) => match fs::canonicalize(&path) {
                Ok(absolute_path) => Some(absolute_path),
                Err(_) => None
            }
            None => None
        };

        let playback_device = if args.host_index.is_some() || args.device_index.is_some() {
            PlaybackDevice::user_defined(args.host_index, args.device_index)
        } else if let Some(state) = &persisted_state {
            state.playback_device
        } else {
            PlaybackDevice::Default
        };

        let window_size = WindowSize {
            width: 1280,
            height: 720
        };

        State {
            audio: None,
            cursor: Cursor::intialize(),
            end_timecode: 0.0,
            hovered_location: None,
            interface_scaling: persisted_state.as_ref().map_or(1.0, |state| state.interface_scaling),
            locations: Locations::new(path_arg, persisted_state.as_ref()),
            operation: Operation::None,
            pause_point_timecode: None,
            playback_device,
            play_point_timecode: None,
            playhead_timecode: 0.0,
            playing: false,
            repeat: false,
            selected_range: None,
            theme: persisted_state.map_or(Theme::Dark, |state| state.theme),
            window_size
        }
    }

    pub fn persist(&self) {
        if let Some(system_config_dir) = dirs::config_dir() {
            let config_dir = system_config_dir.join("good-form");

            if config_dir.is_dir() || fs::create_dir_all(&config_dir).is_ok() {
                let persisted_state = PersistedState::from(self);

                if let Ok(bytes) = bincode::serialize::<PersistedState>(&persisted_state) {
                    let state_file = config_dir.join("state.bincode");
                    fs::write(state_file, &bytes).ok();
                }
            }
        }
    }
}
