//! Exposes the high level interface for interacting with the gfx-hal based
//! rendering code - as of yet still in an early draft state

use gfx_hal::{
    adapter::Adapter,
    Backend,
    buffer::{self, SubRange},
    command::{
        BufferImageCopy,
        ClearColor,
        ClearValue,
        CommandBufferFlags,
        Level,
        SubpassContents
    },
    Features,
    format::{AsFormat, Aspects, ChannelType, Format, R8Unorm, Rg16Unorm},
    image::{
        Access,
        Extent,
        Filter,
        Layout,
        Offset,
        SamplerDesc,
        SubresourceLayers,
        SubresourceRange,
        WrapMode
    },
    memory::{Barrier, Dependencies, Segment},
    pass::{
        Attachment,
        AttachmentLoadOp,
        AttachmentOps,
        AttachmentStoreOp,
        SubpassDesc
    },
    pool::CommandPoolCreateFlags,
    prelude::*,
    pso::{
        Descriptor,
        DescriptorSetWrite,
        PipelineStage,
        Rect,
        ShaderStageFlags,
        Viewport
    },
    queue::{QueueGroup, Submission},
    window::{Extent2D, SwapchainConfig}
};
use std::{
    borrow::Borrow,
    iter,
    mem::{self, ManuallyDrop},
    ptr
};

use crate::engine::{
    downsampler::PeakPairAligned32,
    playback_engine::PlaybackEngine,
    regions::{Button, Marker, Region, Regions},
    state::{
        operation::Operation,
        swipe::SwipeDirection,
        theme::Theme,
        State
    }
};

mod buffers;
mod descriptor_set;
mod descriptor_sets;
mod geometry;
mod image;
mod pipeline;
mod pipelines;
mod shaders;
mod text_renderer;
mod theme_data;
mod glyph_atlas;

use buffers::Buffer;
use descriptor_sets::DescriptorSets;
use geometry::Geometry;
use image::Image;
use pipelines::Pipelines;
use text_renderer::TextRenderer;
use theme_data::{
    DARK_THEME_DATA,
    LIGHT_THEME_DATA,
    ThemeDataAligned112
 };
use glyph_atlas::GlyphAtlas;

#[repr(C)]
struct StableInterfaceGlobalsAligned128 {
    theme: ThemeDataAligned112,
    width: f32,
    height: f32
}

#[repr(C)]
struct VolatileInterfaceGlobalsAligned20 {
    mouse_x: f32,
    mouse_y: f32,
    playhead_x: f32,
    selected_range_start: f32,
    selected_range_end: f32
}

const COLOR_RANGE: SubresourceRange = SubresourceRange {
    aspects: Aspects::COLOR,
    levels: 0..1,
    layers: 0..1
};

const TEXT_DIMENSIONS: Extent2D = Extent2D {
    width: 1280,
    height: 720
};

const WAVEFORM_DIMENSIONS: Extent2D = Extent2D {
    width: 4096,
    height: 4096
};

const ICON_CLICKED_STATE: u32 = 2;
const ICON_DISABLED_STATE: u32 = 3;
const ICON_HOVERED_STATE: u32 = 1;
const ICON_NORMAL_STATE: u32 = 0;

const TIMECODE_ONE_HOUR: f64 = 3600.0;

fn format_timecode(timecode: f64, end_timecode: f64) -> String {
    let fraction = (timecode.fract() * 100.0) as u32;
    let second = timecode as u32 % 60;
    let minute = timecode as u32 / 60 % 60;

    if end_timecode >= TIMECODE_ONE_HOUR {
        let hour = timecode as u32 / 60 / 60;

        format!("{:02}:{:02}:{:02}.{:02}", hour, minute, second, fraction)
    } else {
        format!("{:02}:{:02}.{:02}", minute, second, fraction)
    }
}

pub struct Renderer<'a, B: Backend> {
    instance: Option<B::Instance>,
    device: B::Device,
    queue_group: QueueGroup<B>,
    descriptor_sets: DescriptorSets<B>,
    surface: ManuallyDrop<B::Surface>,
    adapter: Adapter<B>,
    format: Format,
    geometry: Geometry,
    text_renderer: TextRenderer<'a>,
    glyph_atlas: GlyphAtlas<'a>,
    clear_value: ClearValue,
    viewport: Viewport,
    render_pass: ManuallyDrop<B::RenderPass>,
    pipelines: Pipelines<B>,
    submission_complete_semaphores: Vec<B::Semaphore>,
    submission_complete_fences: Vec<B::Fence>,
    cmd_pools: Vec<B::CommandPool>,
    cmd_buffers: Vec<B::CommandBuffer>,
    stable_interface_globals_buffer: Buffer<B>,
    volatile_interface_globals_buffer: Buffer<B>,
    vertex_buffer: Buffer<B>,
    text_upload_buffer: Buffer<B>,
    text_image: Image<B>,
    glyph_atlas_upload_buffer: Buffer<B>,
    glyph_atlas_image: Image<B>,
    waveform_upload_buffer: Buffer<B>,
    waveform_image: Image<B>,
    sampler: ManuallyDrop<B::Sampler>,
    frames_in_flight: usize,
    frame: u64,
}

impl<'a, B> Renderer<'a, B>
where
    B: Backend
{
    pub fn new<'s>(
        instance: Option<B::Instance>,
        mut surface: B::Surface,
        adapter: Adapter<B>,
        regions: &'s Regions,
        state: &'s State
    ) -> Renderer<'a, B> {
        let mut glyph_atlas = GlyphAtlas::new(&regions);

        let geometry = Geometry::new(&regions, &mut glyph_atlas);

        let limits = adapter.physical_device.limits();

        // Build a new device and associated command queues
        let family = adapter.queue_families
                            .iter()
                            .find(|family| {
                                surface.supports_queue_family(family) &&
                                family.queue_type().supports_graphics()
                            })
                            .unwrap();
        let mut gpu = unsafe {
            adapter.physical_device
                   .open(&[(family, &[1.0])], Features::empty())
                   .unwrap()
        };
        let mut queue_group = gpu.queue_groups.pop().unwrap();
        let device = gpu.device;

        let mut command_pool = unsafe {
            device.create_command_pool(queue_group.family, CommandPoolCreateFlags::empty())
        }
        .expect("Can't create command pool");

        let descriptor_sets = DescriptorSets::new(&device);

        // Buffer allocations
        let non_coherent_alignment = limits.non_coherent_atom_size as u64;
        let vertex_buffer_len = geometry.byte_size;
        let padded_vertex_buffer_len = ((vertex_buffer_len + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        let vertex_buffer = Buffer::new(&adapter, &device, padded_vertex_buffer_len, buffer::Usage::VERTEX);

        unsafe {
            let mapping = device.map_memory(
                &vertex_buffer.memory,
                Segment { offset: 0, size: Some(padded_vertex_buffer_len) }  // TODO: padded_vertex_buffer_len == vertex_buffer.requirements.size?
            ).unwrap();

            ptr::copy_nonoverlapping(geometry.icons.as_ptr() as *const u8, mapping, geometry.buffer_subrange_icons.size.unwrap() as usize);
            ptr::copy_nonoverlapping(geometry.quads.as_ptr() as *const u8, mapping.offset(geometry.buffer_subrange_quads.offset as isize), geometry.buffer_subrange_quads.size.unwrap() as usize);
            ptr::copy_nonoverlapping(geometry.glyph_quads.as_ptr() as *const u8, mapping.offset(geometry.buffer_subrange_glyph_quads.offset as isize), geometry.buffer_subrange_glyph_quads.size.unwrap() as usize);

            device.flush_mapped_memory_ranges(iter::once((
                &*vertex_buffer.memory,
                Segment { offset: 0, size: Some(padded_vertex_buffer_len) }  // TODO: padded_vertex_buffer_len == vertex_buffer.requirements.size?
            ))).unwrap();
            device.unmap_memory(&vertex_buffer.memory);
        }

        // Stable interface globals buffer allocation

        let stable_interface_globals_buffer_len = mem::size_of::<StableInterfaceGlobalsAligned128>() as u64;
        let padded_stable_interface_globals_buffer_len = ((stable_interface_globals_buffer_len + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        let stable_interface_globals_buffer = Buffer::new(&adapter, &device, padded_stable_interface_globals_buffer_len, buffer::Usage::UNIFORM);

        let theme_data = match state.theme {
            Theme::Dark => DARK_THEME_DATA,
            Theme::Light => LIGHT_THEME_DATA
        };

        let clear_value = ClearValue {
            color: ClearColor {
                float32: [
                    theme_data.background.r,
                    theme_data.background.g,
                    theme_data.background.b,
                    1.0
                ]
            }
        };

        let stable_interface_globals = StableInterfaceGlobalsAligned128 {
            theme: theme_data,
            width: state.window_size.width as f32,
            height: state.window_size.height as f32
        };

        unsafe {
            let pointer = std::mem::transmute::<&StableInterfaceGlobalsAligned128, *const u8>(&stable_interface_globals);
            let mapping = device.map_memory(
                &stable_interface_globals_buffer.memory,
                 Segment { offset: 0, size: Some(padded_stable_interface_globals_buffer_len) }  // TODO: padded_stable_interface_globals_buffer_len == stable_interface_globals_buffer.requirements.size?
             ).unwrap();

            ptr::copy_nonoverlapping(pointer, mapping, stable_interface_globals_buffer_len as usize);

            device.flush_mapped_memory_ranges(iter::once((
                &*stable_interface_globals_buffer.memory,
                Segment { offset: 0, size: Some(padded_stable_interface_globals_buffer_len) }  // TODO: padded_stable_interface_globals_buffer_len == stable_interface_globals_buffer.requirements.size?
            ))).unwrap();
            device.unmap_memory(&stable_interface_globals_buffer.memory);
        }

        // Volatile interface globals buffer allocation

        let volatile_interface_globals_buffer_len = mem::size_of::<VolatileInterfaceGlobalsAligned20>() as u64;
        let padded_volatile_interface_globals_buffer_len = ((volatile_interface_globals_buffer_len + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;
        let volatile_interface_globals_buffer = Buffer::new(&adapter, &device, padded_volatile_interface_globals_buffer_len, buffer::Usage::UNIFORM);

        let volatile_interface_globals = VolatileInterfaceGlobalsAligned20 {
            mouse_x: 0.0,
            mouse_y: 0.0,
            playhead_x: 0.0,
            selected_range_start: -1.0,
            selected_range_end: -1.0
        };

        // TODO: Here and in all other Segment declarations it might be we don't even need to set the size, see https://docs.rs/gfx-hal/0.5.0/gfx_hal/memory/struct.Segment.html
        unsafe {
            let pointer = std::mem::transmute::<&VolatileInterfaceGlobalsAligned20, *const u8>(&volatile_interface_globals);
            let mapping = device.map_memory(
                &volatile_interface_globals_buffer.memory,
                Segment { offset: 0, size: Some(padded_volatile_interface_globals_buffer_len) }  // TODO: padded_volatile_interface_globals_buffer_len == volatile_interface_globals_buffer.requirements.size?
            ).unwrap();

            ptr::copy_nonoverlapping(pointer, mapping, volatile_interface_globals_buffer_len as usize);

            device.flush_mapped_memory_ranges(iter::once((
                &*volatile_interface_globals_buffer.memory,
                Segment { offset: 0, size: Some(padded_volatile_interface_globals_buffer_len) },  // TODO: padded_volatile_interface_globals_buffer_len == volatile_interface_globals_buffer.requirements.size?
            ))).unwrap();
            device.unmap_memory(&volatile_interface_globals_buffer.memory);
        }

        // Timecode glyphs pipeline resources

        let (glyph_atlas_canvas_width, glyph_atlas_canvas_height) = (glyph_atlas.canvas.size.width, glyph_atlas.canvas.size.height);
        let row_alignment_mask = limits.optimal_buffer_copy_pitch_alignment as u32 - 1;
        let glyph_atlas_image_stride = 1usize;
        let glyph_atlas_canvas_row_pitch = (glyph_atlas_canvas_width * glyph_atlas_image_stride as u32 + row_alignment_mask) & !row_alignment_mask;
        let glyph_atlas_canvas_upload_size = (glyph_atlas_canvas_height * glyph_atlas_canvas_row_pitch) as u64;
        let glyph_atlas_canvas_padded_upload_size = ((glyph_atlas_canvas_upload_size + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        let glyph_atlas_upload_buffer = Buffer::new(&adapter, &device, glyph_atlas_canvas_padded_upload_size, buffer::Usage::TRANSFER_SRC);

        // copy image data into staging buffer
        unsafe {
            let mapping = device.map_memory(
                &glyph_atlas_upload_buffer.memory,
                Segment { offset: 0, size: Some(glyph_atlas_canvas_padded_upload_size) }  // TODO: glyph_atlas_canvas_padded_upload_size == glyph_atlas_upload_buffer.requirements.size?
            ).unwrap();

            for y in 0..glyph_atlas_canvas_height as usize {
                let row = &(*glyph_atlas.canvas.pixels)[y * (glyph_atlas_canvas_width as usize)];
                ptr::copy_nonoverlapping(
                    row,
                    mapping.offset(y as isize * glyph_atlas_canvas_row_pitch as isize),
                    glyph_atlas_canvas_width as usize * glyph_atlas_image_stride
                );

            }

            device.flush_mapped_memory_ranges(iter::once((
                &*glyph_atlas_upload_buffer.memory,
                Segment { offset: 0, size: Some(glyph_atlas_canvas_padded_upload_size) }  // TODO: glyph_atlas_canvas_padded_upload_size == glyph_atlas_upload_buffer.requirements.size?
            ))).unwrap();
            device.unmap_memory(&glyph_atlas_upload_buffer.memory);
        }

        let glyph_atlas_image = Image::new(&adapter, &device, R8Unorm::SELF, glyph_atlas_canvas_width, glyph_atlas_canvas_height);

        // text pipeline resources

        let text_image_stride = 4usize;
        let text_row_pitch = (TEXT_DIMENSIONS.width * text_image_stride as u32 + row_alignment_mask) & !row_alignment_mask;
        let text_upload_size = (TEXT_DIMENSIONS.height * text_row_pitch) as u64;
        let text_padded_upload_size = ((text_upload_size + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        let text_upload_buffer = Buffer::new(&adapter, &device, text_padded_upload_size, buffer::Usage::TRANSFER_SRC);
        let text_image = Image::new(&adapter, &device, R8Unorm::SELF, TEXT_DIMENSIONS.width, TEXT_DIMENSIONS.height);

        // waveform pipeline resources

        let waveform_image_stride = 4usize;
        let waveform_row_pitch = (WAVEFORM_DIMENSIONS.width * waveform_image_stride as u32 + row_alignment_mask) & !row_alignment_mask;
        let waveform_upload_size = (WAVEFORM_DIMENSIONS.height * waveform_row_pitch) as u64;
        let waveform_padded_upload_size = ((waveform_upload_size + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        let waveform_upload_buffer = Buffer::new(&adapter, &device, waveform_padded_upload_size, buffer::Usage::TRANSFER_SRC);
        let waveform_image = Image::new(&adapter, &device, Rg16Unorm::SELF, WAVEFORM_DIMENSIONS.width, WAVEFORM_DIMENSIONS.height);

        let sampler = ManuallyDrop::new(
            unsafe {
                device.create_sampler(&SamplerDesc::new(Filter::Linear, WrapMode::Clamp))
            }
            .expect("Can't create sampler"),
        );

        unsafe {
            device.write_descriptor_sets(vec![
                DescriptorSetWrite {
                    set: &descriptor_sets.stable_interface_globals.set,
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Buffer(
                        &*stable_interface_globals_buffer.buffer,
                        SubRange::WHOLE
                    ))
                },
                DescriptorSetWrite {
                    set: &descriptor_sets.volatile_interface_globals.set,
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Buffer(
                        &*volatile_interface_globals_buffer.buffer,
                        SubRange::WHOLE
                    ))
                },
                DescriptorSetWrite {
                    set: &descriptor_sets.text.set,
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Image(&*text_image.image_view, Layout::ShaderReadOnlyOptimal))
                },
                DescriptorSetWrite {
                    set: &descriptor_sets.text.set,
                    binding: 1,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Sampler(&*sampler))
                },
                DescriptorSetWrite {
                    set: &descriptor_sets.glyph_atlas.set,
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Image(&*glyph_atlas_image.image_view, Layout::ShaderReadOnlyOptimal))
                },
                DescriptorSetWrite {
                    set: &descriptor_sets.glyph_atlas.set,
                    binding: 1,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Sampler(&*sampler))
                },
                DescriptorSetWrite {
                    set: &descriptor_sets.waveform.set,
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Image(&*waveform_image.image_view, Layout::ShaderReadOnlyOptimal))
                },
                DescriptorSetWrite {
                    set: &descriptor_sets.waveform.set,
                    binding: 1,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Sampler(&*sampler))
                }
            ]);
        }

        // copy image staging buffer to texture
        let mut copy_fence = device.create_fence(false).expect("Could not create fence");
        unsafe {
            let mut cmd_buffer = command_pool.allocate_one(Level::Primary);
            cmd_buffer.begin_primary(CommandBufferFlags::ONE_TIME_SUBMIT);

            let image_barrier = Barrier::Image {
                states: (Access::empty(), Layout::Undefined)..(Access::TRANSFER_WRITE, Layout::TransferDstOptimal),
                target: &*glyph_atlas_image.image,
                families: None,
                range: COLOR_RANGE.clone(),
            };

            cmd_buffer.pipeline_barrier(
                PipelineStage::TOP_OF_PIPE..PipelineStage::TRANSFER,
                Dependencies::empty(),
                &[image_barrier],
            );

            cmd_buffer.copy_buffer_to_image(
                &glyph_atlas_upload_buffer.buffer,
                &glyph_atlas_image.image,
                Layout::TransferDstOptimal,
                &[BufferImageCopy {
                    buffer_offset: 0,
                    buffer_width: glyph_atlas_canvas_row_pitch / (glyph_atlas_image_stride as u32),
                    buffer_height: glyph_atlas_canvas_height as u32,
                    image_layers: SubresourceLayers {
                        aspects: Aspects::COLOR,
                        level: 0,
                        layers: 0..1,
                    },
                    image_offset: Offset { x: 0, y: 0, z: 0 },
                    image_extent: Extent {
                        width: glyph_atlas_canvas_width,
                        height: glyph_atlas_canvas_height,
                        depth: 1,
                    },
                }],
            );

            let image_barrier = Barrier::Image {
                states: (Access::TRANSFER_WRITE, Layout::TransferDstOptimal)..(Access::SHADER_READ, Layout::ShaderReadOnlyOptimal),
                target: &*glyph_atlas_image.image,
                families: None,
                range: COLOR_RANGE.clone(),
            };
            cmd_buffer.pipeline_barrier(
                PipelineStage::TRANSFER..PipelineStage::FRAGMENT_SHADER,
                Dependencies::empty(),
                &[image_barrier],
            );

            cmd_buffer.finish();

            queue_group.queues[0].submit_without_semaphores(
                Some(&cmd_buffer),
                Some(&mut copy_fence)
            );

            device.wait_for_fence(&copy_fence, !0)
                  .expect("Can't wait for fence");
        }

        unsafe {
            device.destroy_fence(copy_fence);
        }

        let caps = surface.capabilities(&adapter.physical_device);
        let formats = surface.supported_formats(&adapter.physical_device);

        let format = formats.map_or(Format::Rgba8Srgb, |formats| {
            formats
                .iter()
                .find(|format| format.base_format().1 == ChannelType::Srgb)
                .map(|format| *format)
                .unwrap_or(formats[0])
        });

        let default_extent = Extent2D {
            height: state.window_size.height,
            width: state.window_size.width
        };
        let swap_config = SwapchainConfig::from_caps(&caps, format, default_extent);

        let extent = swap_config.extent;
        unsafe {
            surface
                .configure_swapchain(&device, swap_config)
                .expect("Can't configure swapchain");
        };

        let render_pass = {
            let attachment = Attachment {
                format: Some(format),
                samples: 1,
                ops: AttachmentOps::new(
                    AttachmentLoadOp::Clear,
                    AttachmentStoreOp::Store,
                ),
                stencil_ops: AttachmentOps::DONT_CARE,
                layouts: Layout::Undefined..Layout::Present,
            };

            let subpass = SubpassDesc {
                colors: &[(0, Layout::ColorAttachmentOptimal)],
                depth_stencil: None,
                inputs: &[],
                resolves: &[],
                preserves: &[],
            };

            ManuallyDrop::new(
                unsafe { device.create_render_pass(&[attachment], &[subpass], &[]) }
                    .expect("Can't create render pass"),
            )
        };

        // Define maximum number of frames we want to be able to be "in flight" (being computed
        // simultaneously) at once
        let frames_in_flight = 3;

        // The number of the rest of the resources is based on the frames in flight.
        let mut submission_complete_semaphores = Vec::with_capacity(frames_in_flight);
        let mut submission_complete_fences = Vec::with_capacity(frames_in_flight);
        // Note: We don't really need a different command pool per frame in such a simple demo like this,
        // but in a more 'real' application, it's generally seen as optimal to have one command pool per
        // thread per frame. There is a flag that lets a command pool reset individual command buffers
        // which are created from it, but by default the whole pool (and therefore all buffers in it)
        // must be reset at once. Furthermore, it is often the case that resetting a whole pool is actually
        // faster and more efficient for the hardware than resetting individual command buffers, so it's
        // usually best to just make a command pool for each set of buffers which need to be reset at the
        // same time (each frame). In our case, each pool will only have one command buffer created from it,
        // though.
        let mut cmd_pools = Vec::with_capacity(frames_in_flight);
        let mut cmd_buffers = Vec::with_capacity(frames_in_flight);

        cmd_pools.push(command_pool);
        for _ in 1..frames_in_flight {
            unsafe {
                cmd_pools.push(
                    device.create_command_pool(
                        queue_group.family,
                        CommandPoolCreateFlags::empty(),
                    )
                    .expect("Can't create command pool")
                );
            }
        }

        for i in 0..frames_in_flight {
            submission_complete_semaphores.push(
                device.create_semaphore()
                      .expect("Could not create semaphore")
            );
            submission_complete_fences.push(
                device.create_fence(true)
                      .expect("Could not create fence")
            );
            cmd_buffers.push(unsafe {
                cmd_pools[i].allocate_one(Level::Primary)
            });
        }

        let pipelines = Pipelines::new(&device, &render_pass, &descriptor_sets);

        // Rendering setup
        let viewport = Viewport {
            rect: Rect {
                x: 0,
                y: 0,
                w: extent.width as _,
                h: extent.height as _,
            },
            depth: 0.0..1.0,
        };

        Renderer {
            instance,
            device,
            queue_group,
            descriptor_sets,
            surface: ManuallyDrop::new(surface),
            adapter,
            format,
            clear_value,
            glyph_atlas,
            text_renderer: TextRenderer::new(),
            geometry,
            viewport,
            render_pass,
            pipelines,
            submission_complete_semaphores,
            submission_complete_fences,
            cmd_pools,
            cmd_buffers,
            stable_interface_globals_buffer,
            volatile_interface_globals_buffer,
            vertex_buffer,
            text_upload_buffer,
            text_image,
            glyph_atlas_upload_buffer,
            glyph_atlas_image,
            waveform_upload_buffer,
            waveform_image,
            sampler,
            frames_in_flight,
            frame: 0,
        }
    }

    pub fn recreate_swapchain(&mut self, state: &State) {
        let caps = self.surface.capabilities(&self.adapter.physical_device);
        let default_extent = Extent2D {
            height: state.window_size.height,
            width: state.window_size.width
        };
        let swap_config = SwapchainConfig::from_caps(&caps, self.format, default_extent);

        let extent = swap_config.extent.to_extent();

        unsafe {
            self.surface
                .configure_swapchain(&self.device, swap_config)
                .expect("Can't create swapchain");
        }

        self.viewport.rect.w = extent.width as _;
        self.viewport.rect.h = extent.height as _;
    }

    pub fn render(&mut self, playback_engine: &PlaybackEngine, regions: &Regions, state: &State) {
        self.update_volatile_interface_globals(&state);

        let surface_image = unsafe {
            match self.surface.acquire_image(!0) {
                Ok((image, _)) => image,
                Err(_) => {
                    self.recreate_swapchain(state);
                    return;
                }
            }
        };

        let framebuffer = unsafe {
            self.device.create_framebuffer(
                &self.render_pass,
                iter::once(surface_image.borrow()),
                Extent {
                    width: state.window_size.width,
                    height: state.window_size.height,
                    depth: 1,
                },
            )
            .unwrap()
        };

        // Compute index into our resource ring buffers based on the frame number
        // and number of frames in flight. Pay close attention to where this index is needed
        // versus when the swapchain image index we got from acquire_image is needed.
        let frame_idx = self.frame as usize % self.frames_in_flight;

        // Wait for the fence of the previous submission of this frame and reset it; ensures we are
        // submitting only up to maximum number of frames_in_flight if we are submitting faster than
        // the gpu can keep up with. This would also guarantee that any resources which need to be
        // updated with a CPU->GPU data copy are not in use by the GPU, so we can perform those updates.
        // In this case there are none to be done, however.
        unsafe {
            let fence = &self.submission_complete_fences[frame_idx];
            self.device
                .wait_for_fence(fence, !0)
                .expect("Failed to wait for fence");
            self.device
                .reset_fence(fence)
                .expect("Failed to reset fence");
            self.cmd_pools[frame_idx].reset(false);
        }

        // Rendering
        let cmd_buffer = &mut self.cmd_buffers[frame_idx];
        unsafe {
            cmd_buffer.begin_primary(CommandBufferFlags::ONE_TIME_SUBMIT);

            cmd_buffer.set_viewports(0, &[self.viewport.clone()]);
            cmd_buffer.set_scissors(0, &[self.viewport.rect]);

            cmd_buffer.begin_render_pass(
                &self.render_pass,
                &framebuffer,
                self.viewport.rect,
                &[self.clear_value],
                SubpassContents::Inline,
            );

            // Draw surfaces

            cmd_buffer.bind_vertex_buffers(0, iter::once((&*self.vertex_buffer.buffer, self.geometry.buffer_subrange_quads.clone())));

            cmd_buffer.bind_graphics_pipeline(&self.pipelines.surface_pipeline.pipeline);
            cmd_buffer.bind_graphics_descriptor_sets(
                &self.pipelines.surface_pipeline.pipeline_layout,
                0,
                iter::once(&self.descriptor_sets.stable_interface_globals.set),
                &[]
            );

            cmd_buffer.draw(Geometry::QUADS_RANGE_SURFACES, 0..1);

            // Draw waveform

            if let Some(ref audio) = state.audio {
                cmd_buffer.bind_graphics_pipeline(&self.pipelines.waveform_pipeline.pipeline);
                cmd_buffer.bind_graphics_descriptor_sets(
                    &self.pipelines.waveform_pipeline.pipeline_layout,
                    0,
                    vec![
                        &self.descriptor_sets.stable_interface_globals.set,
                        &self.descriptor_sets.volatile_interface_globals.set,
                        &self.descriptor_sets.waveform.set
                    ],
                    &[]
                );

                match audio.num_channels {
                    1 => {
                        cmd_buffer.push_graphics_constants(
                            &self.pipelines.waveform_pipeline.pipeline_layout,
                            ShaderStageFlags::FRAGMENT,
                            0,
                            &[0]
                        );
                        cmd_buffer.draw(Geometry::QUADS_RANGE_MONO_WAVEFORM, 0..1);
                    }
                    2 => {
                        cmd_buffer.push_graphics_constants(
                            &self.pipelines.waveform_pipeline.pipeline_layout,
                            ShaderStageFlags::FRAGMENT,
                            0,
                            &[0]
                        );
                        cmd_buffer.draw(Geometry::QUADS_RANGE_STEREO_LEFT_WAVEFORM, 0..1);

                        cmd_buffer.push_graphics_constants(
                            &self.pipelines.waveform_pipeline.pipeline_layout,
                            ShaderStageFlags::FRAGMENT,
                            0,
                            &[1]
                        );
                        cmd_buffer.draw(Geometry::QUADS_RANGE_STEREO_RIGHT_WAVEFORM, 0..1);
                    }
                    _ => unimplemented!()
                }
            }


            // Draw slow changing text (interface text for locations state and browser)

            cmd_buffer.bind_graphics_pipeline(&self.pipelines.text_pipeline.pipeline);
            cmd_buffer.bind_graphics_descriptor_sets(
                &self.pipelines.text_pipeline.pipeline_layout,
                0,
                vec![
                    &self.descriptor_sets.stable_interface_globals.set,
                    &self.descriptor_sets.text.set
                ],
                &[]
            );

            cmd_buffer.draw(Geometry::QUADS_RANGE_TEXT, 0..1);


            // Draw glyph-atlas based texts

            cmd_buffer.bind_vertex_buffers(0, iter::once((&*self.vertex_buffer.buffer, self.geometry.buffer_subrange_glyph_quads.clone())));
            cmd_buffer.bind_graphics_pipeline(&self.pipelines.glyph_atlas_pipeline.pipeline);
            cmd_buffer.bind_graphics_descriptor_sets(
                &self.pipelines.glyph_atlas_pipeline.pipeline_layout,
                0,
                vec![
                    &self.descriptor_sets.stable_interface_globals.set,
                    &self.descriptor_sets.glyph_atlas.set
                ],
                &[]
            );

            // Draw timecode

            let timecode_string = format_timecode(state.playhead_timecode, state.end_timecode);
            let timecode_shaping = self.glyph_atlas.shape(timecode_string, true);

            let playhead_x = ((state.playhead_timecode / state.end_timecode) * state.window_size.width as f64) as f32;
            let mut text_cursor_x: f32 = 0.0;

            for shaped_glyph in timecode_shaping {
                cmd_buffer.push_graphics_constants(
                    &self.pipelines.glyph_atlas_pipeline.pipeline_layout,
                    ShaderStageFlags::VERTEX,
                    0,
                    &[
                        (playhead_x + 16.0 + text_cursor_x).to_bits(),
                        (regions.labels.timecode_y).to_bits()
                    ]
                );

                let glyph = self.glyph_atlas.get_rendered_glyph(shaped_glyph.codepoint);

                cmd_buffer.draw(glyph.vertex_range.start..glyph.vertex_range.end, 0..1);

                text_cursor_x += shaped_glyph.x_advance;
            }

            // Draw play state info text

            let playback_device_label_string = match playback_engine {
                PlaybackEngine::Crashed(_) => String::from("Crashed"),
                PlaybackEngine::Online { device_name, .. } => device_name.to_string(),
                PlaybackEngine::Offline => String::from("Offline")
            };

            let playback_device_label_shaping = self.glyph_atlas.shape(playback_device_label_string, false);

            let playback_device_label_width = playback_device_label_shaping.iter().fold(0.0, |width, shaped_glyph| width + shaped_glyph.x_advance);

            let mut text_cursor_x: f32 = regions.labels.playback_device.x - playback_device_label_width;

            for shaped_glyph in playback_device_label_shaping {
                cmd_buffer.push_graphics_constants(
                    &self.pipelines.glyph_atlas_pipeline.pipeline_layout,
                    ShaderStageFlags::VERTEX,
                    0,
                    &[
                        text_cursor_x.to_bits(),
                        regions.labels.playback_device.y.to_bits()
                    ]
                );

                let glyph = self.glyph_atlas.get_rendered_glyph(shaped_glyph.codepoint);

                cmd_buffer.draw(glyph.vertex_range.start..glyph.vertex_range.end, 0..1);

                text_cursor_x += shaped_glyph.x_advance;
            }

            // Draw swipe widget

            cmd_buffer.bind_vertex_buffers(0, iter::once((&*self.vertex_buffer.buffer, self.geometry.buffer_subrange_quads.clone())));

            if let Operation::TransportSwipe(ref swipe) = state.operation {
                let direction: u32 = match swipe.direction {
                    SwipeDirection::None => 0,
                    SwipeDirection::Left => 1,
                    SwipeDirection::Right => 2,
                    SwipeDirection::Up => 3,
                    SwipeDirection::Down => 4
                };

                cmd_buffer.bind_graphics_pipeline(&self.pipelines.swipe_widget_pipeline.pipeline);
                cmd_buffer.bind_graphics_descriptor_sets(
                    &self.pipelines.swipe_widget_pipeline.pipeline_layout,
                    0,
                    iter::once(&self.descriptor_sets.stable_interface_globals.set),
                    &[]
                );
                cmd_buffer.push_graphics_constants(
                    &self.pipelines.swipe_widget_pipeline.pipeline_layout,
                    ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                    0,
                    &[
                        (swipe.anchor.x as f32 - 140.0).to_bits(),
                        (swipe.anchor.y as f32 - 140.0).to_bits(),
                        direction
                    ]
                );

                cmd_buffer.draw(Geometry::QUADS_RANGE_SWIPE_WIDGET, 0..1);
            }


            // TODO: Pick up again at a later point
            // Draw input widget
            // cmd_buffer.bind_graphics_pipeline(&self.pipelines.widget_pipeline.pipeline);
            // cmd_buffer.bind_graphics_descriptor_sets(
            //     &self.pipelines.widget_pipeline.pipeline_layout,
            //     0,
            //     iter::once(&self.descriptor_sets.stable_interface_globals.set),
            //     &[]
            // );
            //
            // cmd_buffer.draw(Geometry::QUADS_RANGE_INPUT_WIDGET, 0..1);


            // Draw icons

            cmd_buffer.bind_vertex_buffers(0, iter::once((&*self.vertex_buffer.buffer, self.geometry.buffer_subrange_icons.clone())));

            cmd_buffer.bind_graphics_pipeline(&self.pipelines.icon_pipeline.pipeline);
            cmd_buffer.bind_graphics_descriptor_sets(
                &self.pipelines.icon_pipeline.pipeline_layout,
                0,
                iter::once(&self.descriptor_sets.stable_interface_globals.set),
                &[]
            );

            // Selection markers

            if let Some(markers) = &regions.markers.selection {
                let icon_state = if let Region::Marker(Marker::SelectionStart) = state.cursor.region {
                    match state.operation {
                        Operation::MovingMarker(Marker::SelectionStart) => ICON_HOVERED_STATE,
                        Operation::MovingMarker(_) => ICON_NORMAL_STATE,
                        _ => ICON_HOVERED_STATE
                    }
                } else {
                    ICON_NORMAL_STATE
                };

                cmd_buffer.push_graphics_constants(
                    &self.pipelines.icon_pipeline.pipeline_layout,
                    ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                    0,
                    &[
                        markers.start.x.to_bits(),
                        markers.start.y.to_bits(),
                        icon_state
                    ]
                );

                cmd_buffer.draw(
                    self.geometry.vertex_range_icon_selection_start.start..self.geometry.vertex_range_icon_selection_start.end,
                    0..1
                );

                let icon_state = if let Region::Marker(Marker::SelectionEnd) = state.cursor.region {
                    match state.operation {
                        Operation::MovingMarker(Marker::SelectionEnd) => ICON_HOVERED_STATE,
                        Operation::MovingMarker(_) => ICON_NORMAL_STATE,
                        _ => ICON_HOVERED_STATE
                    }
                } else {
                    ICON_NORMAL_STATE
                };

                cmd_buffer.push_graphics_constants(
                    &self.pipelines.icon_pipeline.pipeline_layout,
                    ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                    0,
                    &[
                        markers.end.x.to_bits(),
                        markers.end.y.to_bits(),
                        icon_state
                    ]
                );

                cmd_buffer.draw(
                    self.geometry.vertex_range_icon_selection_end.start..self.geometry.vertex_range_icon_selection_end.end,
                    0..1
                );
            }

            // Play point marker

            if let Some(origin) = &regions.markers.play_point {
                let icon_state = if let Region::Marker(Marker::PlayPoint) = state.cursor.region {
                    match state.operation {
                        Operation::MovingMarker(Marker::PlayPoint) => ICON_HOVERED_STATE,
                        Operation::MovingMarker(_) => ICON_NORMAL_STATE,
                        _ => ICON_HOVERED_STATE
                    }
                } else {
                    ICON_NORMAL_STATE
                };

                cmd_buffer.push_graphics_constants(
                    &self.pipelines.icon_pipeline.pipeline_layout,
                    ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                    0,
                    &[
                        origin.x.to_bits(),
                        origin.y.to_bits(),
                        icon_state
                    ]
                );

                cmd_buffer.draw(
                    self.geometry.vertex_range_icon_play.start..self.geometry.vertex_range_icon_play.end,
                    0..1
                );
            }

            // Pause point marker

            if let Some(origin) = &regions.markers.pause_point {
                let icon_state = if let Region::Marker(Marker::PausePoint) = state.cursor.region {
                    match state.operation {
                        Operation::MovingMarker(Marker::PausePoint) => ICON_HOVERED_STATE,
                        Operation::MovingMarker(_) => ICON_NORMAL_STATE,
                        _ => ICON_HOVERED_STATE
                    }
                } else {
                    ICON_NORMAL_STATE
                };

                cmd_buffer.push_graphics_constants(
                    &self.pipelines.icon_pipeline.pipeline_layout,
                    ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                    0,
                    &[
                        origin.x.to_bits(),
                        origin.y.to_bits(),
                        icon_state
                    ]
                );

                cmd_buffer.draw(
                    self.geometry.vertex_range_icon_pause.start..self.geometry.vertex_range_icon_pause.end,
                    0..1
                );
            }

            // Play button

            let icon_state = if let Region::Button(Button::Play) = state.cursor.region {
                if let Operation::PressingButton(Button::Play) = state.operation {
                    ICON_CLICKED_STATE
                } else {
                    ICON_HOVERED_STATE
                }
            } else {
                ICON_NORMAL_STATE
            };

            cmd_buffer.push_graphics_constants(
                &self.pipelines.icon_pipeline.pipeline_layout,
                ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                0,
                &[
                    (regions.buttons.play.x).to_bits(),
                    (regions.buttons.play.y).to_bits(),
                    icon_state
                ]
            );

            let vertex_range = if state.playing {
                &self.geometry.vertex_range_icon_pause
            } else {
                &self.geometry.vertex_range_icon_play
            };

            cmd_buffer.draw(vertex_range.start..vertex_range.end, 0..1);

            // Stop button

            let icon_state = if let Region::Button(Button::Stop) = state.cursor.region {
                if let Operation::PressingButton(Button::Stop) = state.operation {
                    ICON_CLICKED_STATE
                } else {
                    ICON_HOVERED_STATE
                }
            } else {
                ICON_NORMAL_STATE
            };

            cmd_buffer.push_graphics_constants(
                &self.pipelines.icon_pipeline.pipeline_layout,
                ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                0,
                &[
                    (regions.buttons.stop.x).to_bits(),
                    (regions.buttons.stop.y).to_bits(),
                    icon_state
                ]
            );

            cmd_buffer.draw(
                self.geometry.vertex_range_icon_stop.start..self.geometry.vertex_range_icon_stop.end,
                0..1
            );

            // Engine button

            let icon_state = if let Region::Button(Button::Engine) = state.cursor.region {
                if let Operation::PressingButton(Button::Engine) = state.operation {
                    ICON_CLICKED_STATE
                } else {
                    ICON_HOVERED_STATE
                }
            } else if let PlaybackEngine::Online { .. } = playback_engine {
                ICON_NORMAL_STATE
            } else {
                ICON_DISABLED_STATE
            };

            cmd_buffer.push_graphics_constants(
                &self.pipelines.icon_pipeline.pipeline_layout,
                ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                0,
                &[
                    (regions.buttons.engine.x).to_bits(),
                    (regions.buttons.engine.y).to_bits(),
                    icon_state
                ]
            );

            cmd_buffer.draw(
                self.geometry.vertex_range_icon_engine.start..self.geometry.vertex_range_icon_engine.end,
                0..1
            );

            // Draw swipe widget text

            if let Operation::TransportSwipe(ref swipe) = state.operation {
                cmd_buffer.bind_vertex_buffers(0, iter::once((&*self.vertex_buffer.buffer, self.geometry.buffer_subrange_glyph_quads.clone())));
                cmd_buffer.bind_graphics_pipeline(&self.pipelines.glyph_atlas_pipeline.pipeline);
                cmd_buffer.bind_graphics_descriptor_sets(
                    &self.pipelines.glyph_atlas_pipeline.pipeline_layout,
                    0,
                    vec![
                        &self.descriptor_sets.stable_interface_globals.set,
                        &self.descriptor_sets.glyph_atlas.set
                    ],
                    &[]
                );

                let label = match swipe.direction {
                    SwipeDirection::None => String::from("Keep position"),
                    SwipeDirection::Left => String::from("Jump to play point"),
                    SwipeDirection::Right => String::from("Jump to pause point"),
                    SwipeDirection::Up => String::from("Jump to start"),
                    SwipeDirection::Down => String::from("Jump to end")
                };

                let label_shaping = self.glyph_atlas.shape(label, false);
                let label_width = label_shaping.iter().fold(0.0, |width, shaped_glyph| width + shaped_glyph.x_advance);

                let (mut text_cursor_x, text_cursor_y) = match swipe.direction {
                    SwipeDirection::None => (swipe.anchor.x as f32 - label_width / 2.0, swipe.anchor.y as f32 - regions.proportions.font_size * 1.5),
                    SwipeDirection::Left => (swipe.anchor.x as f32 - 140.0 - 16.0 - label_width, swipe.anchor.y as f32 - regions.proportions.font_size / 1.5),
                    SwipeDirection::Right => (swipe.anchor.x as f32 + 140.0 + 16.0, swipe.anchor.y as f32 - regions.proportions.font_size / 1.5),
                    SwipeDirection::Up => (swipe.anchor.x as f32 - label_width / 2.0, swipe.anchor.y as f32 - 140.0 - 32.0),
                    SwipeDirection::Down => (swipe.anchor.x as f32 - label_width / 2.0, swipe.anchor.y as f32 + 140.0 + 32.0 - regions.proportions.font_size)
                };

                for shaped_glyph in label_shaping {
                    cmd_buffer.push_graphics_constants(
                        &self.pipelines.glyph_atlas_pipeline.pipeline_layout,
                        ShaderStageFlags::VERTEX,
                        0,
                        &[
                            text_cursor_x.to_bits(),
                            text_cursor_y.to_bits()
                        ]
                    );

                    let glyph = self.glyph_atlas.get_rendered_glyph(shaped_glyph.codepoint);

                    cmd_buffer.draw(glyph.vertex_range.start..glyph.vertex_range.end, 0..1);

                    text_cursor_x += shaped_glyph.x_advance;
                }
            }

            // Draw tooltip text

            if let Region::Button(Button::Engine) = state.cursor.region {
                // TODO: Optimize away/combine with binding above
                cmd_buffer.bind_vertex_buffers(0, iter::once((&*self.vertex_buffer.buffer, self.geometry.buffer_subrange_glyph_quads.clone())));
                cmd_buffer.bind_graphics_pipeline(&self.pipelines.glyph_atlas_pipeline.pipeline);
                cmd_buffer.bind_graphics_descriptor_sets(
                    &self.pipelines.glyph_atlas_pipeline.pipeline_layout,
                    0,
                    vec![
                        &self.descriptor_sets.stable_interface_globals.set,
                        &self.descriptor_sets.glyph_atlas.set
                    ],
                    &[]
                );

                let tooltip = String::from("Toggle playback engine (Shortcut: F5), Shift-click to restart with defaults (Shortcut: Shift + F5)");

                let tooltip_shaping = self.glyph_atlas.shape(tooltip, false);
                let tooltip_width = tooltip_shaping.iter().fold(0.0, |width, shaped_glyph| width + shaped_glyph.x_advance);

                let mut text_cursor_x = regions.buttons.engine.x - tooltip_width - 5.0;
                let text_cursor_y = regions.buttons.engine.y - regions.proportions.font_size - 5.0;

                for shaped_glyph in tooltip_shaping {
                    cmd_buffer.push_graphics_constants(
                        &self.pipelines.glyph_atlas_pipeline.pipeline_layout,
                        ShaderStageFlags::VERTEX,
                        0,
                        &[
                            text_cursor_x.to_bits(),
                            text_cursor_y.to_bits()
                        ]
                    );

                    let glyph = self.glyph_atlas.get_rendered_glyph(shaped_glyph.codepoint);

                    cmd_buffer.draw(glyph.vertex_range.start..glyph.vertex_range.end, 0..1);

                    text_cursor_x += shaped_glyph.x_advance;
                }
            }

            cmd_buffer.end_render_pass();
            cmd_buffer.finish();

            let submission = Submission {
                command_buffers: iter::once(&*cmd_buffer),
                wait_semaphores: None,
                signal_semaphores: iter::once(&self.submission_complete_semaphores[frame_idx])
            };
            self.queue_group.queues[0].submit(
                submission,
                Some(&self.submission_complete_fences[frame_idx])
            );

            // present frame
            let result = self.queue_group.queues[0].present_surface(
                &mut self.surface,
                surface_image,
                Some(&self.submission_complete_semaphores[frame_idx])
            );

            self.device.destroy_framebuffer(framebuffer);

            if result.is_err() {
                self.recreate_swapchain(state);
            }
        }

        self.frame += 1;
    }

    pub fn resize(&mut self, regions: &Regions, state: &State) {
        self.update_geometry(regions);
        self.update_stable_interface_globals(state);
        self.recreate_swapchain(state);
    }

    pub fn update_geometry(&mut self, regions: &Regions) {
        self.geometry.update(regions);

        unsafe {
            let mapping = self.device.map_memory(
                &self.vertex_buffer.memory,
                Segment {
                    offset: self.geometry.buffer_subrange_quads.offset,
                    size: self.geometry.buffer_subrange_quads.size
                }
            ).unwrap();

            ptr::copy_nonoverlapping(
                self.geometry.quads.as_ptr() as *const u8,
                mapping,
                self.geometry.buffer_subrange_quads.size.unwrap() as usize
            );

            self.device.flush_mapped_memory_ranges(iter::once((
                &*self.vertex_buffer.memory,
                Segment {
                    offset: self.geometry.buffer_subrange_quads.offset,
                    size: self.geometry.buffer_subrange_quads.size
                }
            ))).unwrap();

            self.device.unmap_memory(&self.vertex_buffer.memory);
        }
    }

    fn update_stable_interface_globals(&mut self, state: &State) {
        let theme_data = match state.theme {
            Theme::Dark => DARK_THEME_DATA,
            Theme::Light => LIGHT_THEME_DATA
        };

        self.clear_value = ClearValue {
            color: ClearColor {
                float32: [
                    theme_data.background.r,
                    theme_data.background.g,
                    theme_data.background.b,
                    1.0
                ]
            }
        };

        let non_coherent_alignment = self.adapter.physical_device.limits().non_coherent_atom_size as u64;
        let stable_interface_globals_buffer_len = mem::size_of::<StableInterfaceGlobalsAligned128>() as u64;
        let padded_stable_interface_globals_buffer_len = ((stable_interface_globals_buffer_len + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        let stable_interface_globals = StableInterfaceGlobalsAligned128 {
            theme: theme_data,
            width: state.window_size.width as f32,
            height: state.window_size.height as f32,
        };

        unsafe {
            let pointer = std::mem::transmute::<&StableInterfaceGlobalsAligned128, *const u8>(&stable_interface_globals);
            let mapping = self.device.map_memory(
                &self.stable_interface_globals_buffer.memory,
                Segment { offset: 0, size: Some(padded_stable_interface_globals_buffer_len) }  // TODO: padded_stable_interface_globals_buffer_len == stable_interface_globals_buffer.requirements.size?
            ).unwrap();

            ptr::copy_nonoverlapping(pointer, mapping, stable_interface_globals_buffer_len as usize);

            self.device.flush_mapped_memory_ranges(iter::once((
                &*self.stable_interface_globals_buffer.memory,
                Segment { offset: 0, size: Some(padded_stable_interface_globals_buffer_len) }  // TODO: padded_stable_interface_globals_buffer_len == stable_interface_globals_buffer.requirements.size?
            ))).unwrap();
            self.device.unmap_memory(&self.stable_interface_globals_buffer.memory);
        }
    }

    pub fn update_theme(&mut self, state: &State) {
        self.update_stable_interface_globals(state);
    }

    pub fn update_volatile_interface_globals(&mut self, state: &State) {
        let non_coherent_alignment = self.adapter.physical_device.limits().non_coherent_atom_size as u64;
        let volatile_interface_globals_buffer_len = mem::size_of::<VolatileInterfaceGlobalsAligned20>() as u64;
        let padded_volatile_interface_globals_buffer_len = ((volatile_interface_globals_buffer_len + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        let (selected_range_start, selected_range_end) = match state.selected_range {
            Some(ref range) => (
                ((range.earlier_end() / state.end_timecode) * state.window_size.width as f64) as f32,
                ((range.later_end() / state.end_timecode) * state.window_size.width as f64) as f32
            ),
            None => (-1.0, -1.0)
        };

        let volatile_interface_globals = VolatileInterfaceGlobalsAligned20 {
            mouse_x: state.cursor.x as f32,
            mouse_y: state.cursor.y as f32,
            playhead_x: ((state.playhead_timecode / state.end_timecode) * state.window_size.width as f64) as f32,
            selected_range_start,
            selected_range_end
        };

        unsafe {
            let pointer = std::mem::transmute::<&VolatileInterfaceGlobalsAligned20, *const u8>(&volatile_interface_globals);
            let mapping = self.device.map_memory(
                &self.volatile_interface_globals_buffer.memory,
                Segment { offset: 0, size: Some(padded_volatile_interface_globals_buffer_len) }  // TODO: padded_volatile_interface_globals_buffer_len == volatile_interface_globals_buffer.requirements.size?
            ).unwrap();

            ptr::copy_nonoverlapping(pointer, mapping, volatile_interface_globals_buffer_len as usize);

            self.device.flush_mapped_memory_ranges(iter::once((
                &*self.volatile_interface_globals_buffer.memory,
                Segment { offset: 0, size: Some(padded_volatile_interface_globals_buffer_len) }  // TODO: padded_volatile_interface_globals_buffer_len == volatile_interface_globals_buffer.requirements.size?
            ))).unwrap();
            self.device.unmap_memory(&self.volatile_interface_globals_buffer.memory);
        }
    }

    pub fn update_text(&mut self, lines: Vec<String>) {
        self.text_renderer.render_canvas(lines);
        self.update_text_image();
    }

    pub fn update_text_image(&mut self) {
        let (canvas_width, canvas_height) = (
            self.text_renderer.canvas.size.width,
            self.text_renderer.canvas.size.height
        );

        if canvas_width > TEXT_DIMENSIONS.width || canvas_height > TEXT_DIMENSIONS.height {
            panic!("The generated canvas does not fit within the fixed TEXT_DIMENSIONS boundaries - continue towards a sensible implementation");
        }

        let limits = self.adapter.physical_device.limits();

        let non_coherent_alignment = limits.non_coherent_atom_size as u64;
        let row_alignment_mask = limits.optimal_buffer_copy_pitch_alignment as u32 - 1;
        let text_image_stride = 1usize;
        let text_row_pitch = (TEXT_DIMENSIONS.width * text_image_stride as u32 + row_alignment_mask) & !row_alignment_mask;
        let text_upload_size = (TEXT_DIMENSIONS.height * text_row_pitch) as u64;
        let text_padded_upload_size = ((text_upload_size + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        unsafe {
            let mapping = self.device.map_memory(
                &self.text_upload_buffer.memory,
                Segment { offset: 0, size: Some(text_padded_upload_size) }  // TODO: text_padded_upload_size == text_upload_buffer.requirements.size?
            ).unwrap();

            for y in 0..canvas_height as usize {
                let row = &(*self.text_renderer.canvas.pixels)[y * (canvas_width as usize)];
                ptr::copy_nonoverlapping(
                    row,
                    mapping.offset(y as isize * text_row_pitch as isize),
                    canvas_width as usize * text_image_stride
                );

            }

            self.device.flush_mapped_memory_ranges(iter::once((
                &*self.text_upload_buffer.memory,
                Segment { offset: 0, size: Some(text_padded_upload_size) }  // TODO: text_padded_upload_size == text_upload_buffer.requirements.size?
            ))).unwrap();
            self.device.unmap_memory(&self.text_upload_buffer.memory);
        }

        let mut command_pool = unsafe {
            self.device.create_command_pool(self.queue_group.family, CommandPoolCreateFlags::empty())
        }
        .expect("Can't create command pool");

        let mut copy_fence = self.device.create_fence(false).expect("Could not create fence");

        unsafe {
            let mut cmd_buffer = command_pool.allocate_one(Level::Primary);
            cmd_buffer.begin_primary(CommandBufferFlags::ONE_TIME_SUBMIT);

            let image_barrier = Barrier::Image {
                states: (Access::empty(), Layout::Undefined)..(Access::TRANSFER_WRITE, Layout::TransferDstOptimal),
                target: &*self.text_image.image,
                families: None,
                range: COLOR_RANGE.clone()
            };

            cmd_buffer.pipeline_barrier(
                PipelineStage::TOP_OF_PIPE..PipelineStage::TRANSFER,
                Dependencies::empty(),
                &[image_barrier]
            );

            cmd_buffer.copy_buffer_to_image(
                &self.text_upload_buffer.buffer,
                &self.text_image.image,
                Layout::TransferDstOptimal,
                &[BufferImageCopy {
                    buffer_offset: 0,
                    buffer_width: text_row_pitch / (text_image_stride as u32),
                    buffer_height: TEXT_DIMENSIONS.height as u32,
                    image_layers: SubresourceLayers {
                        aspects: Aspects::COLOR,
                        level: 0,
                        layers: 0..1,
                    },
                    image_offset: Offset { x: 0, y: 0, z: 0 },
                    image_extent: Extent {
                        width: TEXT_DIMENSIONS.width,
                        height: TEXT_DIMENSIONS.height,
                        depth: 1,
                    },
                }],
            );

            let image_barrier = Barrier::Image {
                states: (Access::TRANSFER_WRITE, Layout::TransferDstOptimal)..(Access::SHADER_READ, Layout::ShaderReadOnlyOptimal),
                target: &*self.text_image.image,
                families: None,
                range: COLOR_RANGE.clone()
            };

            cmd_buffer.pipeline_barrier(
                PipelineStage::TRANSFER..PipelineStage::FRAGMENT_SHADER,
                Dependencies::empty(),
                &[image_barrier]
            );

            cmd_buffer.finish();

            self.queue_group.queues[0].submit_without_semaphores(
                Some(&cmd_buffer),
                Some(&mut copy_fence)
            );

            self.device.wait_for_fence(&copy_fence, !0)
                       .expect("Can't wait for fence");

           self.device.destroy_fence(copy_fence);
           self.device.destroy_command_pool(command_pool);
        }
    }

    pub fn update_waveform_image(&mut self, peaks: Vec<Vec<PeakPairAligned32>>) {
        let limits = self.adapter.physical_device.limits();

        let non_coherent_alignment = limits.non_coherent_atom_size as u64;
        let row_alignment_mask = limits.optimal_buffer_copy_pitch_alignment as u32 - 1;
        let waveform_image_stride = 4usize;
        let waveform_row_pitch = (WAVEFORM_DIMENSIONS.width * waveform_image_stride as u32 + row_alignment_mask) & !row_alignment_mask;
        let waveform_upload_size = (WAVEFORM_DIMENSIONS.height * waveform_row_pitch) as u64;
        let waveform_padded_upload_size = ((waveform_upload_size + non_coherent_alignment - 1) / non_coherent_alignment) * non_coherent_alignment;

        unsafe {
            // TODO: We are only writing to a small portion of the memory, would make sense to only map and flush that range
            let mapping = self.device.map_memory(
                &self.waveform_upload_buffer.memory,
                Segment { offset: 0, size: Some(waveform_padded_upload_size) }  // TODO: waveform_padded_upload_size == waveform_upload_buffer.requirements.size?
            ).unwrap();

            for (index, channel) in peaks.iter().enumerate() {
                ptr::copy_nonoverlapping(
                    channel.as_ptr() as *const u8,
                    mapping.offset(index as isize * waveform_row_pitch as isize),
                    WAVEFORM_DIMENSIONS.width as usize * waveform_image_stride
                );
            }

            self.device.flush_mapped_memory_ranges(iter::once((
                &*self.waveform_upload_buffer.memory,
                Segment { offset: 0, size: Some(waveform_padded_upload_size) }  // TODO: waveform_padded_upload_size == waveform_upload_buffer.requirements.size?
            ))).unwrap();
            self.device.unmap_memory(&self.waveform_upload_buffer.memory);
        }

        let mut command_pool = unsafe {
            self.device.create_command_pool(self.queue_group.family, CommandPoolCreateFlags::empty())
        }
        .expect("Can't create command pool");

        let mut copy_fence = self.device.create_fence(false).expect("Could not create fence");

        unsafe {
            let mut cmd_buffer = command_pool.allocate_one(Level::Primary);
            cmd_buffer.begin_primary(CommandBufferFlags::ONE_TIME_SUBMIT);

            let image_barrier = Barrier::Image {
                states: (Access::empty(), Layout::Undefined)..(Access::TRANSFER_WRITE, Layout::TransferDstOptimal),
                target: &*self.waveform_image.image,
                families: None,
                range: COLOR_RANGE.clone()
            };

            cmd_buffer.pipeline_barrier(
                PipelineStage::TOP_OF_PIPE..PipelineStage::TRANSFER,
                Dependencies::empty(),
                &[image_barrier]
            );

            cmd_buffer.copy_buffer_to_image(
                &self.waveform_upload_buffer.buffer,
                &self.waveform_image.image,
                Layout::TransferDstOptimal,
                &[BufferImageCopy {
                    buffer_offset: 0,
                    buffer_width: waveform_row_pitch / (waveform_image_stride as u32),
                    buffer_height: WAVEFORM_DIMENSIONS.height as u32,
                    image_layers: SubresourceLayers {
                        aspects: Aspects::COLOR,
                        level: 0,
                        layers: 0..1,
                    },
                    image_offset: Offset { x: 0, y: 0, z: 0 },
                    image_extent: Extent {
                        width: WAVEFORM_DIMENSIONS.width,
                        height: WAVEFORM_DIMENSIONS.height,
                        depth: 1,
                    },
                }],
            );

            let image_barrier = Barrier::Image {
                states: (Access::TRANSFER_WRITE, Layout::TransferDstOptimal)..(Access::SHADER_READ, Layout::ShaderReadOnlyOptimal),
                target: &*self.waveform_image.image,
                families: None,
                range: COLOR_RANGE.clone()
            };

            cmd_buffer.pipeline_barrier(
                PipelineStage::TRANSFER..PipelineStage::FRAGMENT_SHADER,
                Dependencies::empty(),
                &[image_barrier]
            );

            cmd_buffer.finish();

            self.queue_group.queues[0].submit_without_semaphores(
                Some(&cmd_buffer),
                Some(&mut copy_fence)
            );

            self.device.wait_for_fence(&copy_fence, !0)
                       .expect("Can't wait for fence");

            self.device.destroy_fence(copy_fence);

            // TODO: Maybe we can/should optimize this by keeping around a persistent command pool
            //       for buffer updates, which is created alongside the Renderer and disposed of
            //       along with all the command pools used for the frames in flight.
            //       If we do that we also need to call reset() on it after each usage.
            self.device.destroy_command_pool(command_pool);
        }
    }
}

impl<'a, B> Drop for Renderer<'a, B>
where
    B: Backend,
{
    fn drop(&mut self) {
        self.device.wait_idle().unwrap();

        unsafe {
            // TODO: When ManuallyDrop::take (soon to be renamed to ManuallyDrop::read) is stabilized we should use that instead.
            //       Update: This has now happened, investigate at some point - https://blog.rust-lang.org/2020/03/12/Rust-1.42.html#stabilized-apis

            self.descriptor_sets.manually_drop(&self.device);

            self.stable_interface_globals_buffer.manually_drop(&self.device);
            self.volatile_interface_globals_buffer.manually_drop(&self.device);
            self.vertex_buffer.manually_drop(&self.device);
            self.text_upload_buffer.manually_drop(&self.device);
            self.glyph_atlas_upload_buffer.manually_drop(&self.device);
            self.waveform_upload_buffer.manually_drop(&self.device);

            self.waveform_image.manually_drop(&self.device);
            self.text_image.manually_drop(&self.device);
            self.glyph_atlas_image.manually_drop(&self.device);

            self.device.destroy_sampler(ManuallyDrop::into_inner(ptr::read(&self.sampler)));

            for pool in self.cmd_pools.drain(..) {
                self.device.destroy_command_pool(pool);
            }

            for semaphore in self.submission_complete_semaphores.drain(..) {
                self.device.destroy_semaphore(semaphore);
            }

            for fence in self.submission_complete_fences.drain(..) {
                self.device.destroy_fence(fence);
            }

            self.device.destroy_render_pass(ManuallyDrop::into_inner(ptr::read(&self.render_pass)));
            self.surface.unconfigure_swapchain(&self.device);
            self.pipelines.manually_drop(&self.device);

            if let Some(instance) = &self.instance {
                let surface = ManuallyDrop::into_inner(ptr::read(&self.surface));
                instance.destroy_surface(surface);
            }
        }
    }
}
