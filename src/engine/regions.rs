use crate::engine::state::State;

pub struct Origin {
    pub x: f32,
    pub y: f32
}

pub struct Size {
    pub width: f32,
    pub height: f32
}

#[derive(Clone, Copy, PartialEq)]
pub enum Button {
    Engine,
    Play,
    Stop
}

#[derive(Clone, Copy, PartialEq)]
pub enum Marker {
    PausePoint,
    PlayPoint,
    SelectionEnd,
    SelectionStart
}

#[derive(PartialEq)]
pub enum Region {
    Button(Button),
    Marker(Marker),
    Outside,
    StatusBar,
    TopBar,
    Waveform
}

pub struct Areas {
    pub status_bar: Origin,
    pub top_bar: Origin,
    pub waveform: Origin
}

pub struct Buttons {
    pub engine: Origin,
    pub play: Origin,
    pub stop: Origin
}

pub struct Labels {
    pub playback_device: Origin,
    pub timecode_y: f32,
}

pub struct SelectionMarkers {
    pub end: Origin,
    pub start: Origin
}

pub struct Markers {
    pub pause_point: Option<Origin>,
    pub play_point: Option<Origin>,
    pub selection: Option<SelectionMarkers>
}

pub struct Proportions {
    pub base_unit: f32,
    // TODO: Just a temporary workaround used for proper
    //       vertical text centering, revisit this soon.
    pub font_baseline_correction: f32,
    pub font_size: f32,
    pub padding: f32,
    pub square_button_width: f32,
    pub square_icon_width: f32,
    pub status_bar_height: f32,
    pub top_bar_height: f32
}

pub struct Regions {
    pub areas: Areas,
    pub buttons: Buttons,
    pub labels: Labels,
    pub markers: Markers,
    pub proportions: Proportions,
    pub window_size: Size
}

impl Regions {
    pub fn intialize(state: &State) -> Regions {
        let base_unit = 1.0 * state.interface_scaling;

        let padding = 4.0 * base_unit;
        let font_size = 18.0 * base_unit;
        let font_baseline_correction = 2.0 * base_unit;
        let square_icon_width = 20.0 * base_unit;

        let square_button_width = square_icon_width;

        let status_bar_height = square_button_width + 2.0 * padding;
        let top_bar_height = square_button_width + 2.0 * padding;

        let play_button = Origin {
            x: state.window_size.width as f32 / 2.0 - square_button_width - 2.0 * padding,
            y: padding
        };

        let stop_button = Origin {
            x: play_button.x + square_button_width + 2.0 * padding,
            y: padding
        };

        Regions {
            areas: Areas {
                status_bar: Origin {
                    x: 0.0,
                    y: state.window_size.height as f32 - status_bar_height
                },
                top_bar: Origin {
                    x: 0.0,
                    y: 0.0
                },
                waveform: Origin {
                    x: 0.0,
                    y: top_bar_height
                }
            },
            buttons: Buttons {
                engine: Origin {
                    x: state.window_size.width as f32 - padding - square_button_width,
                    y: state.window_size.height as f32 - padding - square_button_width
                },
                play: play_button,
                stop: stop_button
            },
            labels: Labels {
                playback_device: Origin {
                    x: state.window_size.width as f32 - 2.0 * padding - square_button_width,
                    y: state.window_size.height as f32 - padding - square_button_width / 2.0 - font_size / 2.0 - font_baseline_correction
                },
                timecode_y: state.window_size.height as f32 - status_bar_height - 32.0 * base_unit
            },
            markers: Markers {
                pause_point: None,
                play_point: None,
                selection: None
            },
            proportions: Proportions {
                base_unit,
                font_baseline_correction,
                font_size,
                padding,
                square_button_width,
                square_icon_width,
                status_bar_height,
                top_bar_height
            },
            window_size: Size {
                width: state.window_size.width as f32,
                height: state.window_size.height as f32
            }
        }
    }

    pub fn region_at(&mut self, x: f32, y: f32) -> Region {
        if y < self.areas.waveform.y {
            if x < self.buttons.stop.x {
                if x >= self.buttons.play.x &&
                   x <= self.buttons.play.x + self.proportions.square_button_width &&
                   y >= self.buttons.play.y &&
                   y <= self.buttons.play.y + self.proportions.square_button_width {
                    Region::Button(Button::Play)
                } else {
                    Region::TopBar
                }
            } else if
                x <= self.buttons.stop.x + self.proportions.square_button_width &&
                y >= self.buttons.stop.y &&
                y <= self.buttons.stop.y + self.proportions.square_button_width
            {
                Region::Button(Button::Stop)
            } else {
                Region::TopBar
            }
        } else if y < self.areas.status_bar.y {
            if let Some(origin) = &self.markers.play_point {
                if x >= origin.x && x <= origin.x + self.proportions.square_button_width &&
                   y >= origin.y && y <= origin.y + self.proportions.square_button_width {
                    return Region::Marker(Marker::PlayPoint);
                }
            }

            if let Some(origin) = &self.markers.pause_point {
                if x >= origin.x && x <= origin.x + self.proportions.square_button_width &&
                   y >= origin.y && y <= origin.y + self.proportions.square_button_width {
                    return Region::Marker(Marker::PausePoint);
                }
            }

            if let Some(markers) = &self.markers.selection {
                if x >= markers.start.x && x <= markers.start.x + self.proportions.square_button_width &&
                   y >= markers.start.y && y <= markers.start.y + self.proportions.square_button_width {
                    return Region::Marker(Marker::SelectionStart);
                }

                if x >= markers.end.x && x <= markers.end.x + self.proportions.square_button_width &&
                   y >= markers.end.y && y <= markers.end.y + self.proportions.square_button_width {
                    return Region::Marker(Marker::SelectionEnd);
                }
            }

            Region::Waveform
        } else {
            if x >= self.buttons.engine.x &&
               x <= self.buttons.engine.x + self.proportions.square_button_width &&
               y >= self.buttons.engine.y &&
               y <= self.buttons.engine.y + self.proportions.square_button_width {
                Region::Button(Button::Engine)
            } else {
                Region::StatusBar
            }
        }
    }

    pub fn update_all(&mut self, state: &State) {
        self.buttons.engine.x = state.window_size.width as f32 - self.proportions.padding - self.proportions.square_button_width;
        self.buttons.engine.y = state.window_size.height as f32 - self.proportions.padding - self.proportions.square_button_width;
        self.buttons.play.x = state.window_size.width as f32 / 2.0 - self.proportions.square_button_width - 2.0 * self.proportions.padding;
        self.labels.playback_device.x = state.window_size.width as f32 - 2.0 * self.proportions.padding - self.proportions.square_button_width;
        self.labels.playback_device.y = state.window_size.height as f32 - self.proportions.padding - self.proportions.square_button_width / 2.0 - self.proportions.font_size / 2.0 - self.proportions.font_baseline_correction;
        self.labels.timecode_y = state.window_size.height as f32 - self.proportions.status_bar_height - 32.0 * self.proportions.base_unit;
        self.areas.status_bar.y = state.window_size.height as f32 - self.proportions.status_bar_height;
        self.areas.waveform.y = self.proportions.top_bar_height;
        self.buttons.stop.x = self.buttons.play.x + self.proportions.square_button_width + 2.0 * self.proportions.padding;
        self.window_size.width = state.window_size.width as f32;
        self.window_size.height = state.window_size.height as f32;

        self.update_markers(state);
    }

    pub fn update_markers(&mut self, state: &State) {
        self.update_selection_markers(state);
        self.update_transport_markers(state);
    }

    pub fn update_selection_markers(&mut self, state: &State) {
        match (&state.selected_range, &mut self.markers.selection) {
            (Some(range), Some(markers)) => {
                markers.end.x = (range.later_end() / state.end_timecode * self.window_size.width as f64) as f32 - (self.proportions.square_icon_width / 2.0);
                markers.end.y = self.areas.waveform.y + (self.areas.status_bar.y - self.areas.waveform.y) / 2.0 - self.proportions.square_icon_width / 2.0;
                markers.start.x = (range.earlier_end() / state.end_timecode * self.window_size.width as f64) as f32 - (self.proportions.square_icon_width / 2.0);
                markers.start.y = self.areas.waveform.y + (self.areas.status_bar.y - self.areas.waveform.y) / 2.0 - self.proportions.square_icon_width / 2.0;
            }
            (Some(range), None) => {
                self.markers.selection = Some(SelectionMarkers {
                    end: Origin {
                        x: (range.later_end() / state.end_timecode * self.window_size.width as f64) as f32 - (self.proportions.square_icon_width / 2.0),
                        y: self.areas.waveform.y + (self.areas.status_bar.y - self.areas.waveform.y) / 2.0 - self.proportions.square_icon_width / 2.0
                    },
                    start: Origin {
                        x: (range.earlier_end() / state.end_timecode * self.window_size.width as f64) as f32 - (self.proportions.square_icon_width / 2.0),
                        y: self.areas.waveform.y + (self.areas.status_bar.y - self.areas.waveform.y) / 2.0 - self.proportions.square_icon_width / 2.0
                    }
                });
            }
            (None, Some(_)) => {
                self.markers.selection = None;
            }
            (None, None) => ()
        }
    }

    pub fn update_transport_markers(&mut self, state: &State) {
        self.markers.pause_point = state.pause_point_timecode.map(|timecode| {
            Origin {
                x: (timecode / state.end_timecode * self.window_size.width as f64) as f32 - (self.proportions.square_icon_width / 2.0),
                y: self.areas.waveform.y + 10.0
            }
        });

        self.markers.play_point = state.play_point_timecode.map(|timecode| {
            Origin {
                x: (timecode / state.end_timecode * self.window_size.width as f64) as f32 - (self.proportions.square_icon_width / 2.0),
                y: self.areas.waveform.y + 10.0
            }
        });
    }

    pub fn update_proportions(&mut self, state: &State) {
        self.proportions.base_unit = 1.0 * state.interface_scaling;

        self.proportions.font_baseline_correction = 2.0 * self.proportions.base_unit;
        self.proportions.padding = 4.0 * self.proportions.base_unit;
        self.proportions.square_icon_width = 20.0 * self.proportions.base_unit;

        self.proportions.square_button_width = self.proportions.square_icon_width;

        self.proportions.status_bar_height = self.proportions.square_button_width + 2.0 * self.proportions.padding;
        self.proportions.top_bar_height = self.proportions.square_button_width + 2.0 * self.proportions.padding;

        self.update_all(state);
    }
}
