use std::{collections::VecDeque, env, path::PathBuf};

use super::location::Location;
use super::PersistedState;

const SUPPORTED_EXTENSIONS: &[&str] = &["flac", "mp3", "ogg", "wav"];

pub struct Locations {
    pub available_locations: Vec<Location>,
    pub current_directory: PathBuf,
    pub current_file: Option<PathBuf>,
    pub queue: VecDeque<PathBuf>,
    pub selected_location: Option<usize>  // TODO: Probably make private again (used for intermediate interface text experiments)
}

impl Locations {
    pub fn allowed_extension(path: &PathBuf) -> bool {
        match path.extension() {
            Some(extension_osstr) => match extension_osstr.to_str() {
                Some(extension_str) => SUPPORTED_EXTENSIONS.contains(&extension_str.to_lowercase().as_str()),
                _ => false
            }
            None => false
        }
    }

    pub fn ascend(&mut self) -> bool {
        match self.current_directory.parent() {
            Some(path) => {
                let parent_directory = path.to_path_buf();
                self.open_directory(parent_directory);

                true
            }
            None => false
        }
    }

    /// Returns a directory with which to initialize
    /// the application's "current directory" state.
    fn best_effort_starting_directory() -> PathBuf {
        if let Some(path) = dirs::audio_dir() {
            path
        } else if let Some(path) = dirs::home_dir() {
            path
        } else if let Ok(path) = env::current_dir() {
            path
        } else {
            env::temp_dir()
        }
    }

    pub fn descend(&mut self) -> bool {
        match self.selected_location {
            Some(index) => match self.available_locations.get(index) {
                Some(location) => if location.path.is_dir() {
                    let child_directory = location.path.to_path_buf();
                    self.open_directory(child_directory);

                    true
                } else {
                    false
                }
                None => false
            }
            None => false
        }
    }

    fn get_available_locations(path: &PathBuf) -> Vec<Location> {
        let mut locations = match path.read_dir() {
            Ok(results) => results.filter_map(|dir_entry_result| {
                match dir_entry_result {
                    Ok(dir_entry) => if dir_entry.file_name().to_str().unwrap().starts_with(".") {
                        None
                    } else {
                        Location::from(dir_entry)
                    }
                    Err(_) => None
                }
            }).collect(),
            Err(_) => vec![]
        };

        locations.sort_by(|a, b| a.location_type.cmp(&b.location_type).then(a.path.cmp(&b.path)));
        locations
    }

    pub fn new(path_arg: Option<PathBuf>, config: Option<&PersistedState>) -> Locations {
        let (current_directory, current_file) = match path_arg {
            Some(path) => if path.is_dir() {
                (path, None)
            } else {
                let directory = path.parent().unwrap().to_path_buf();

                if Self::allowed_extension(&path) {
                    (directory, Some(path))
                } else {
                    (directory, None)
                }
            }
            None => match config {
                Some(config) => if let Some(ref last_file) = config.last_file {
                    if last_file.is_file() {
                        let file = Some(last_file.to_path_buf());

                        if config.last_directory.is_dir() {
                            (config.last_directory.clone(), file)
                        } else {
                            (last_file.parent().unwrap().to_path_buf(), file)
                        }
                    } else if config.last_directory.is_dir() {
                        (config.last_directory.clone(), None)
                    } else {
                        (Self::best_effort_starting_directory(), None)
                    }
                } else if config.last_directory.is_dir() {
                    (config.last_directory.clone(), None)
                } else {
                    (Self::best_effort_starting_directory(), None)
                }
                None => (Self::best_effort_starting_directory(), None)
            }
        };

        let available_locations = Self::get_available_locations(&current_directory);

        Locations {
            available_locations,
            current_directory,
            current_file,
            queue: VecDeque::new(),
            selected_location: None
        }
    }

    pub fn open_directory(&mut self, path: PathBuf) {
        self.available_locations = Self::get_available_locations(&path);
        self.current_directory = path;
        self.selected_location = None;
    }

    pub fn open_file(&mut self, path: PathBuf) -> bool {
        if Self::allowed_extension(&path) {
            self.current_file = Some(path);
            true
        } else {
            false
        }
    }

    /// Open the selected location if there is one (file or directory)
    ///
    /// Returns true when a supported file has been opened so the caller
    /// knows to decode and load it as a consequence.
    pub fn open_selected(&mut self) -> bool {
        if let Some(index) = self.selected_location {
            if let Some(location) = self.available_locations.get(index) {
                 if location.path.is_dir() {
                    let child_directory = location.path.to_path_buf();
                    self.open_directory(child_directory);
                } else {
                    let file = location.path.to_path_buf();

                    if self.open_file(file) {
                        return true;
                    }
                }
            }
        }

        false
    }

    pub fn queue_following(&mut self) {
        let mut iter = self.available_locations.iter();

        if let Some(_) = iter.find(|location| {
            location.path == *self.current_file.as_ref().unwrap_or(&PathBuf::from(""))
        }) {
            self.queue = iter.map(|location| location.path.clone()).collect();
        }
    }

    pub fn queue_selected(&mut self) {
        if let Some(index) = self.selected_location {
            if let Some(location) = self.available_locations.iter().nth(index) {
                self.queue.push_back(location.path.clone());
            }
        }
    }

    pub fn refresh_current_directory(&mut self) {
        self.available_locations = Self::get_available_locations(&self.current_directory);

        if let Some(index) = self.selected_location {
            if index > self.available_locations.len() {
                self.selected_location = Some(self.available_locations.len() - 1);
            }
        }
    }

    pub fn select_previous(&mut self) -> bool {
        match self.selected_location {
            Some(index) => {
                if index == 0 {
                    false
                } else {
                    self.selected_location = Some(index - 1);
                    true
                }
            }
            None => {
                if self.available_locations.is_empty() {
                    false
                } else {
                    self.selected_location = Some(self.available_locations.len() - 1);
                    true
                }
            }
        }
    }

    pub fn select_next(&mut self) -> bool {
        match self.selected_location {
            Some(index) => {
                if index + 1 >= self.available_locations.len() {
                    false
                } else {
                    self.selected_location = Some(index + 1);
                    true
                }
            }
            None => {
                if self.available_locations.is_empty() {
                    false
                } else {
                    self.selected_location = Some(0);
                    true
                }
            }
        }
    }
}
