//! High level representation of the currently active theme (decoupled from how it is rendered)

use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Deserialize, Serialize)]
pub enum Theme {
    Dark,
    Light
}
