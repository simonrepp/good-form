use crate::engine::regions::Region;

#[derive(Clone, PartialEq)]
pub enum CursorStyle {
    HoveringButton,
    HoveringMarker,
    MovingMarker,
    Pointing,
    SelectingRange,
    SwipingLeft,
    SwipingRight,
    SwipingUp,
    SwipingDown
}

pub struct Cursor {
    pub region: Region,
    pub style: CursorStyle,
    pub x: f32,
    pub y: f32
}

impl Cursor {
    pub fn intialize() -> Cursor {
        Cursor {
            region: Region::Outside,
            style: CursorStyle::Pointing,
            x: 0.0,
            y: 0.0
        }
    }
}
