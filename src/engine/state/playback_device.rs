use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Deserialize, Serialize)]
pub enum PlaybackDevice {
    Default,
    UserDefined {
        device_index: Option<usize>,
        host_index: Option<usize>
    }
}

impl PlaybackDevice {
    pub fn user_defined(host_index: Option<usize>, device_index: Option<usize>) -> Self {
        Self::UserDefined {
            device_index,
            host_index
        }
    }
}
