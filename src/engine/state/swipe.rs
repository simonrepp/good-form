use crate::engine::state::Cursor;

/// We evaluate a swipe to have a certain direction only after
/// the swipe has been going a certain distance (in pixels)
const MIN_SWIPE_LENGTH_PIXELS: f32 = 5.0;

#[derive(Clone, PartialEq)]
pub enum SwipeDirection {
    None,
    Up,
    Down,
    Left,
    Right
}

#[derive(PartialEq)]
pub struct Anchor {
    pub x: f32,
    pub y: f32
}

impl Anchor {
    pub fn new(cursor: &Cursor) -> Anchor {
        Anchor {
            x: cursor.x,
            y: cursor.y
        }
    }
}

#[derive(PartialEq)]
pub struct Swipe {
    pub anchor: Anchor,
    pub direction: SwipeDirection
}

impl Swipe {
    pub fn start_at(cursor: &Cursor) -> Swipe {
        Swipe {
            anchor: Anchor::new(cursor),
            direction: SwipeDirection::None
        }
    }

    pub fn update(&mut self, cursor: &Cursor) {
        let (horizontal, vertical) = (cursor.x - self.anchor.x, cursor.y - self.anchor.y);
        let length = (horizontal.powi(2) + vertical.powi(2)).sqrt().abs();

        self.direction = if length < MIN_SWIPE_LENGTH_PIXELS {
            SwipeDirection::None
        } else if horizontal.abs() >= vertical.abs() {
            if horizontal.is_sign_negative() {
                SwipeDirection::Left
            } else {
                SwipeDirection::Right
            }
        } else if vertical.is_sign_negative() {
            SwipeDirection::Up
        } else {
            SwipeDirection::Down
        }
    }
}
