//! A range abstraction that simplifies dealing with backwards defined ranges.
//!
//! We can extend the range arbitrarily in either direction and always easily access
//! the earlier and later end of it regardless of the direction it was defined in.

pub struct TimecodeRange {
    defined_backwards: bool,
    earlier_end: f64,
    later_end: f64
}

impl TimecodeRange {
    pub fn define(start: f64, end: f64) -> TimecodeRange {
        if start <= end {
            TimecodeRange {
                defined_backwards: false,
                earlier_end: start,
                later_end: end
            }
        } else {
            TimecodeRange {
                defined_backwards: true,
                earlier_end: end,
                later_end: start
            }
        }
    }

    pub fn earlier_end(&self) -> f64 {
        self.earlier_end
    }

    pub fn extend_to(&mut self, timecode: f64) {
        if self.defined_backwards {
            if timecode > self.later_end {
                self.defined_backwards = false;
                self.earlier_end = self.later_end;
                self.later_end = timecode;
            } else {
                self.earlier_end = timecode;
            }
        } else if timecode < self.earlier_end {
            self.defined_backwards = true;
            self.later_end = self.earlier_end;
            self.earlier_end = timecode;
        } else {
            self.later_end = timecode;
        }
    }

    pub fn later_end(&self) -> f64 {
        self.later_end
    }

    pub fn length(&self) -> f64 {
        self.later_end - self.earlier_end
    }

    /// Note that the defined_backwards information only has relevance
    /// if and while the range is being defined using start_at(timecode) and
    /// extend_to(timecode). As soon as it is fully defined we can let the
    /// definition direction grow outdated, we only ensure that the earlier_end
    /// stays before the later_end from that point on (as done here).
    pub fn set_earlier_end(&mut self, timecode: f64) {
        if timecode > self.later_end {
            self.earlier_end = self.later_end;
            self.later_end = timecode;
        } else {
            self.earlier_end = timecode;
        }
    }

    /// Note that the defined_backwards information only has relevance
    /// if and while the range is being defined using start_at(timecode) and
    /// extend_to(timecode). As soon as it is fully defined we can let the
    /// definition direction grow outdated, we only ensure that the earlier_end
    /// stays before the later_end from that point on (as done here).
    pub fn set_later_end(&mut self, timecode: f64) {
        if timecode < self.earlier_end {
            self.later_end = self.earlier_end;
            self.earlier_end = timecode;
        } else {
            self.later_end = timecode;
        }
    }

    pub fn start_at(timecode: f64) -> TimecodeRange {
        TimecodeRange {
            defined_backwards: false,
            earlier_end: timecode,
            later_end: timecode
        }
    }
}
