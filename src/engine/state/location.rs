use std::{fs::DirEntry, path::PathBuf};

// Heads up: Declaration order affects ordering here - one does
//           not simply modify it for cosmetic purposes.
#[derive(Eq, Ord, PartialEq, PartialOrd)]
pub enum LocationType {
    Dir,
    SymLink,
    File
}

pub struct HoveredLocation {
    pub path: PathBuf,
    pub supported: bool
}

pub struct Location {
    pub location_type: LocationType,
    pub path: PathBuf,
}

impl Location {
    pub fn file_name(&self) -> String {
        match self.location_type {
            LocationType::Dir => format!("{}/", self.path.file_name().unwrap().to_str().unwrap().to_string()),
            LocationType::File => self.path.file_name().unwrap().to_str().unwrap().to_string(),
            LocationType::SymLink => format!("{}*", self.path.file_name().unwrap().to_str().unwrap().to_string())
        }
    }

    pub fn from(dir_entry: DirEntry) -> Option<Location> {
        if let Ok(file_type) = dir_entry.file_type() {
            if file_type.is_dir() {
                Some(Location {
                    location_type: LocationType::Dir,
                    path: dir_entry.path()
                })
            } else if file_type.is_file() {
                Some(Location {
                    location_type: LocationType::File,
                    path: dir_entry.path()
                })
            } else if file_type.is_symlink() {
                Some(Location {
                    location_type: LocationType::SymLink,
                    path: dir_entry.path()
                })
            } else {
                None
            }
        } else {
            None
        }
    }
}
