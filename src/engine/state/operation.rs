use crate::engine::regions::{Button, Marker};
use super::swipe::Swipe;

#[derive(PartialEq)]
pub enum Operation {
    None,
    MovingMarker(Marker),
    PressingButton(Button),
    Seeking,
    Selecting,
    TransportSwipe(Swipe)
}
