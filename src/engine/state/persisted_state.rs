use serde::{Deserialize, Serialize};
use std::{fs, path::PathBuf};

use super::{PlaybackDevice, State, Theme};

#[derive(Deserialize, Serialize)]
pub struct PersistedState {
    pub interface_scaling: f32,
    pub last_directory: PathBuf,
    pub last_file: Option<PathBuf>,
    pub playback_device: PlaybackDevice,
    pub theme: Theme
}

impl PersistedState {
    fn file_path() -> Option<PathBuf> {
        dirs::config_dir().map(|dir| dir.join("good-form/state.bincode"))
    }

    pub fn from(state: &State) -> PersistedState {
        PersistedState {
            interface_scaling: state.interface_scaling,
            last_directory: state.locations.current_directory.clone(),
            last_file: state.locations.current_file.clone(),
            playback_device: state.playback_device,
            theme: state.theme
        }
    }

    pub fn purge() {
        if let Some(path) = PersistedState::file_path() {
            fs::remove_file(path).ok();
        }
    }

    pub fn retrieve() -> Option<PersistedState> {
        if let Some(path) = PersistedState::file_path() {
            if let Ok(bytes) = fs::read(path) {
                return bincode::deserialize::<PersistedState>(&bytes).ok();
            }
        }

        None
    }
}
