use hound::{SampleFormat, WavSpec, WavWriter};

use crate::engine::DecodedAudio;
use crate::constants::I24_MAX;
use crate::engine::state::{
    Locations,
    TimecodeRange
};

pub fn export(audio: &DecodedAudio, range: &TimecodeRange, locations: &Locations) {
    let spec = WavSpec {
        channels: audio.num_channels as u16,
        sample_rate: audio.sample_rate,
        bits_per_sample: 24,
        sample_format: SampleFormat::Int,
    };

    let mut number = 1;
    let mut path = locations.current_file.as_ref().unwrap().with_extension("01.wav");

    while path.is_file() {
        number += 1;
        path = locations.current_file.as_ref().unwrap().with_extension(format!("{:2}.wav", number));
    }

    let mut writer = WavWriter::create(path, spec).unwrap();

    let end_timecode = audio.num_samples as f64 / audio.sample_rate as f64;
    let first_sample = ((range.earlier_end() / end_timecode) * (audio.num_samples - 1) as f64) as usize;
    let last_sample = ((range.later_end() / end_timecode) * (audio.num_samples - 1) as f64) as usize;

    for index in first_sample..last_sample {
        for channel in 0..audio.num_channels as usize {
            let sample = audio.samples[channel][index];
            writer.write_sample((sample * I24_MAX as f32) as i32).unwrap();
        }
    }

    writer.finalize().unwrap();
}
