#version 460

layout (push_constant) uniform Swipe {
    layout (offset = 8) uint code;
} direction;

layout (set = 0, binding = 0) uniform StableInterfaceGlobals {
    vec3 background;
    vec4 widget_surface;
    vec4 widget_shadow;
    vec4 text;
    vec4 playhead;
    vec4 waveform;
    vec4 waveform_centerline;
    vec2 dimensions;
} stable_interface_globals;

layout(location = 0) in vec2 v_xy;
layout(location = 1) in vec2 v_uv;

layout(location = 0) out vec4 f_rgba;

void main() {
    vec2 frag_direction = v_uv - 0.5;

    if(direction.code == 0) {
      // None
      f_rgba = vec4(stable_interface_globals.widget_surface.rgb, 0.4);
    } else if(direction.code == 1) {
      // Left
      if(frag_direction.x <= -abs(frag_direction.y)) {
        f_rgba = stable_interface_globals.widget_surface;
      } else {
        f_rgba = vec4(stable_interface_globals.widget_surface.rgb, 0.4);
      }
    } else if(direction.code == 2) {
      // Right
      if(frag_direction.x >= abs(frag_direction.y)) {
        f_rgba = stable_interface_globals.widget_surface;
      } else {
        f_rgba = vec4(stable_interface_globals.widget_surface.rgb, 0.4);
      }
    } else if(direction.code == 3) {
      // Up
      if(frag_direction.y < -abs(frag_direction.x)) {
        f_rgba = stable_interface_globals.widget_surface;
      } else {
        f_rgba = vec4(stable_interface_globals.widget_surface.rgb, 0.4);
      }
    } else if(direction.code == 4) {
      // Down
      if(frag_direction.y > abs(frag_direction.x)) {
        f_rgba = stable_interface_globals.widget_surface;
      } else {
        f_rgba = vec4(stable_interface_globals.widget_surface.rgb, 0.4);
      }
    }
}
