#version 460

layout (push_constant) uniform Icon {
    layout (offset = 8) uint state;
} icon;

layout(location = 0) in vec4 v_rgba;

layout(location = 0) out vec4 f_rgba;

const uint NORMAL = 0;
const uint HOVERED = 1;
const uint CLICKED = 2;
const uint DISABLED = 3;

void main() {
    if(icon.state == NORMAL) {
        f_rgba = v_rgba;
    } else if(icon.state == HOVERED) {
        f_rgba = vec4(v_rgba.rgb, v_rgba.a * 0.7);
    } else if(icon.state == CLICKED) {
        f_rgba = vec4(0.0, 1.0, 1.0, 1.0);
    } else /* if(icon.state == DISABLED) */ {
        f_rgba = vec4(v_rgba.rgb, v_rgba.a * 0.3);
    }
}
