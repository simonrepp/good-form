#version 460

layout (set = 0, binding = 0) uniform StableInterfaceGlobals {
    vec3 background;
    vec4 widget_surface;
    vec4 widget_shadow;
    vec4 text;
    vec4 playhead;
    vec4 waveform;
    vec4 waveform_centerline;
    vec2 dimensions;
} stable_interface_globals;

layout(set = 1, binding = 0) uniform texture2D u_texture;
layout(set = 1, binding = 1) uniform sampler u_sampler;

layout(location = 0) in vec2 v_xy;
layout(location = 1) in vec2 v_uv;

layout(location = 0) out vec4 f_rgba;

void main() {
    float alpha = texture(sampler2D(u_texture, u_sampler), v_uv).r;
    f_rgba = vec4(stable_interface_globals.text.rgb, stable_interface_globals.text.a * alpha);
}
