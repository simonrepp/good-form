#version 460

layout (push_constant) uniform Waveform {
    uint channel;
} waveform;

layout (set = 0, binding = 0) uniform StableInterfaceGlobals {
    vec3 background;
    vec4 widget_surface;
    vec4 widget_shadow;
    vec4 text;
    vec4 playhead;
    vec4 waveform;
    vec4 waveform_centerline;
    vec2 dimensions;
} stable_interface_globals;

layout (set = 1, binding = 0) uniform VolatileInterfaceGlobals {
    vec2 mouse_position;  // TODO: This is currently unused but very handy for debugging so we keep it around for now (and maybe we'll reuse it)
    float playhead_position;
    float selected_range_start;
    float selected_range_end;
} volatile_interface_globals;

layout(set = 2, binding = 0) uniform texture2D u_texture;
layout(set = 2, binding = 1) uniform sampler u_sampler;

layout(location = 0) in vec2 v_xy;
layout(location = 1) in vec2 v_uv;

layout(location = 0) out vec4 f_rgba;

void main() {
    if(v_uv.t >= 0.497 && v_uv.t <= 0.503) {
        f_rgba = stable_interface_globals.waveform_centerline;
    } else {
        vec2 lookup_coordinates = vec2(v_uv.s, 1.0/4096.0 * waveform.channel);
        vec4 lookup_vec = texture(sampler2D(u_texture, u_sampler), lookup_coordinates);

        if(
            v_uv.t < 0.5 && (0.5 - v_uv.t) * 2 < lookup_vec.r ||
            v_uv.t >= 0.5 && (v_uv.t - 0.5) * 2 < lookup_vec.g
        ) {
            f_rgba = stable_interface_globals.waveform;
        } else {
            f_rgba = vec4(stable_interface_globals.waveform.rgb, 0);
        }
    }

    if(volatile_interface_globals.selected_range_start >= 0.0) {  // also implies volatile_interface_globals.selected_range_end >= 0.0
        if(
            (v_xy.x >= volatile_interface_globals.selected_range_start &&
             v_xy.x <= volatile_interface_globals.selected_range_end) ||
            (v_xy.x >= volatile_interface_globals.selected_range_end &&
             v_xy.x <= volatile_interface_globals.selected_range_start)
        ) {
            f_rgba = f_rgba * 0.9 + vec4(0, 1, 1, 1) * 0.1;
        }
    }

    if(
        v_xy.x >= volatile_interface_globals.playhead_position - 0.5 &&
        v_xy.x <= volatile_interface_globals.playhead_position + 0.5
    ) {
        f_rgba = stable_interface_globals.playhead;
    }
}
