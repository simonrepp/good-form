#version 460

layout (push_constant) uniform Translation {
    vec2 vector;
} translation;

layout (set = 0, binding = 0) uniform UniformBufferObject {
    vec3 background;
    vec4 widget_surface;
    vec4 widget_shadow;
    vec4 text;
    vec4 playhead;
    vec4 waveform;
    vec4 waveform_centerline;
    vec2 dimensions;
} interface_globals;

layout(location = 0) in vec2 a_xy;
layout(location = 1) in vec4 a_rgba;

layout(location = 0) out vec4 v_rgba;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    vec2 a_xy_translated = a_xy + translation.vector;

    // Transform from window coordinate space (0..width, 0..height, top left origin)
    // to vulkan coordinate space (-1..1, -1..1, top left origin)
    gl_Position = vec4(a_xy_translated * 2 / interface_globals.dimensions - 1, 0, 1);

    v_rgba = a_rgba;
}
