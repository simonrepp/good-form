#version 460

layout (set = 0, binding = 0) uniform UniformBufferObject {
    vec3 background;
    vec4 widget_surface;
    vec4 widget_shadow;
    vec4 text;
    vec4 playhead;
    vec4 waveform;
    vec4 waveform_centerline;
    vec2 dimensions;
} interface_globals;

layout(location = 0) in vec2 a_xy;
layout(location = 1) in vec2 a_uv;

layout(location = 0) out vec2 v_xy;
layout(location = 1) out vec2 v_uv;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    // Transform from window coordinate space (0..width, 0..height, top left origin)
    // to vulkan coordinate space (-1..1, -1..1, top left origin)
    gl_Position = vec4(a_xy * 2 / interface_globals.dimensions - 1, 0, 1);

    v_xy = a_xy;
    v_uv = a_uv;
}
