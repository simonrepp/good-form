#version 460

layout (set = 0, binding = 0) uniform StableInterfaceGlobals {
    vec3 background;
    vec4 widget_surface;
    vec4 widget_shadow;
    vec4 text;
    vec4 playhead;
    vec4 waveform;
    vec4 waveform_centerline;
    vec2 dimensions;
} stable_interface_globals;

layout(location = 0) in vec2 v_xy;
layout(location = 1) in vec2 v_uv;

layout(location = 0) out vec4 f_rgba;

vec3 black = vec3(0, 0, 0);
float padding = 0.1;

void main() {
    float x_normalized = 0.5 - abs(v_uv.x - 0.5); // 0..1 => 0..0.5..0

    if(x_normalized > padding) {
        float y_normalized = 0.5 - abs(v_uv.y - 0.5); // 0..1 => 0..0.5..0

        if(y_normalized > padding) {
            f_rgba = stable_interface_globals.widget_surface;
        } else {
            f_rgba = vec4(stable_interface_globals.widget_shadow.rgb, stable_interface_globals.widget_shadow.a * (y_normalized * 8));
        }
    } else {
        vec2 difference = vec2(x_normalized, v_uv.y) - vec2(padding, 0.5);
        float len = length(difference);

        if(len < padding) {
            f_rgba = stable_interface_globals.widget_surface;
        } else {
            f_rgba = vec4(stable_interface_globals.widget_shadow.rgb, stable_interface_globals.widget_shadow.a * (1 - len * 8));
        }
    }
}
