#version 460

layout (set = 0, binding = 0) uniform StableInterfaceGlobals {
    vec3 background;
    vec4 widget_surface;
    vec4 widget_shadow;
    vec4 text;
    vec4 playhead;
    vec4 waveform;
    vec4 waveform_centerline;
    vec2 dimensions;
} stable_interface_globals;

layout(location = 0) in vec2 v_xy;
layout(location = 1) in vec2 v_uv;

layout(location = 0) out vec4 f_rgba;

void main() {
    f_rgba = vec4(stable_interface_globals.widget_surface.rgb * (0.9 + (v_uv.x + v_uv.y) / 20.0) , 1.0);
}
