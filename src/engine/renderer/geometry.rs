use gfx_hal::buffer::SubRange;
use serde::Deserialize;
use std::{env, mem, ops::Range};

use crate::engine::regions::{Areas, Proportions, Regions};
use super::glyph_atlas::GlyphAtlas;
use super::text_renderer::{TEXT_CANVAS_WIDTH, TEXT_CANVAS_HEIGHT};

const ICONS_BINCODE: &'static [u8] = include_bytes!(concat!(env!("OUT_DIR"), "/icons.bincode"));

#[derive(Deserialize)]
struct Icons {
    vertex_range_icon_engine: Range<u32>,
    vertex_range_icon_pause: Range<u32>,
    vertex_range_icon_play: Range<u32>,
    vertex_range_icon_selection_end: Range<u32>,
    vertex_range_icon_selection_start: Range<u32>,
    vertex_range_icon_stop: Range<u32>,
    vertices: Vec<AttributeXyRgba>
}

pub enum AttributeType {
    XyRgba,
    XyUv
}

#[derive(Clone, Copy, Deserialize)]
#[repr(C)]
pub struct AttributeXyRgba {
    a_xy: [f32; 2],
    a_rgba: [u8; 4]
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct AttributeXyUv {
    a_xy: [f32; 2],
    a_uv: [f32; 2]
}

pub struct Geometry {
    pub buffer_subrange_icons: SubRange,
    pub buffer_subrange_quads: SubRange,
    pub buffer_subrange_glyph_quads: SubRange,
    pub byte_size: u64,
    pub icons: Vec<AttributeXyRgba>,
    pub quads: Vec<AttributeXyUv>,
    pub glyph_quads: Vec<AttributeXyUv>,
    pub vertex_range_icon_engine: Range<u32>,
    pub vertex_range_icon_pause: Range<u32>,
    pub vertex_range_icon_play: Range<u32>,
    pub vertex_range_icon_selection_end: Range<u32>,
    pub vertex_range_icon_selection_start: Range<u32>,
    pub vertex_range_icon_stop: Range<u32>
}

impl Geometry {
    pub const QUADS_RANGE_MONO_WAVEFORM: Range<u32> = 0..6;
    pub const QUADS_RANGE_STEREO_LEFT_WAVEFORM: Range<u32> = 6..12;
    pub const QUADS_RANGE_STEREO_RIGHT_WAVEFORM: Range<u32> = 12..18;
    // pub const QUADS_RANGE_INPUT_WIDGET: Range<u32> = 18..24;
    pub const QUADS_RANGE_SWIPE_WIDGET: Range<u32> = 24..30;
    pub const QUADS_RANGE_TEXT: Range<u32> = 30..36;
    pub const QUADS_RANGE_SURFACES: Range<u32> = 36..48; // currently top/status bar

    pub fn new(regions: &Regions, glyph_atlas: &mut GlyphAtlas) -> Geometry {
        // Icons

        let mut icons = bincode::deserialize::<Icons>(&ICONS_BINCODE[..]).unwrap();

        for vertex in &mut icons.vertices {
            vertex.a_xy[0] *= regions.proportions.square_icon_width;
            vertex.a_xy[1] *= regions.proportions.square_icon_width;
        }

        let buffer_subrange_icons = SubRange {
            offset: 0,
            size: Some((icons.vertices.len() * mem::size_of::<AttributeXyRgba>()) as u64)
        };

        // Quads

        let quads = Self::quads(regions);

        let buffer_subrange_quads = SubRange {
            offset: buffer_subrange_icons.size.unwrap(),
            size: Some((quads.len() * mem::size_of::<AttributeXyUv>()) as u64)
        };

        // Timecode quads

        let mut glyph_quads: Vec<AttributeXyUv> = Vec::with_capacity(glyph_atlas.glyph_map.len() * 6);

        for (index, glyph) in glyph_atlas.glyph_map.values_mut().enumerate() {
            glyph_quads.extend(&[
                /* ↙ */  AttributeXyUv { a_xy: [0.0, glyph.height], a_uv: [glyph.u_min, glyph.v_min] },
                /* ↘ */  AttributeXyUv { a_xy: [glyph.width, glyph.height], a_uv: [glyph.u_max, glyph.v_min] },
                /* ↗ */  AttributeXyUv { a_xy: [glyph.width, 0.0], a_uv: [glyph.u_max, glyph.v_max] },

                /* ↙ */  AttributeXyUv { a_xy: [0.0, glyph.height], a_uv: [glyph.u_min, glyph.v_min] },
                /* ↗ */  AttributeXyUv { a_xy: [glyph.width, 0.0], a_uv: [glyph.u_max, glyph.v_max] },
                /* ↖ */  AttributeXyUv { a_xy: [0.0, 0.0], a_uv: [glyph.u_min, glyph.v_max] }
            ]);

            glyph.vertex_range = (index as u32 * 6)..((index as u32 + 1) * 6);
        }

        let buffer_subrange_glyph_quads = SubRange {
            offset: buffer_subrange_quads.offset + buffer_subrange_quads.size.unwrap(),
            size: Some((glyph_quads.len() * mem::size_of::<AttributeXyUv>()) as u64)
        };

        // Totals

        let byte_size = buffer_subrange_glyph_quads.offset + buffer_subrange_glyph_quads.size.unwrap();

        Geometry {
            buffer_subrange_icons,
            buffer_subrange_quads,
            buffer_subrange_glyph_quads,
            byte_size,
            icons: icons.vertices,
            quads,
            glyph_quads,
            vertex_range_icon_engine: icons.vertex_range_icon_engine,
            vertex_range_icon_pause: icons.vertex_range_icon_pause,
            vertex_range_icon_play: icons.vertex_range_icon_play,
            vertex_range_icon_selection_end: icons.vertex_range_icon_selection_end,
            vertex_range_icon_selection_start: icons.vertex_range_icon_selection_start,
            vertex_range_icon_stop: icons.vertex_range_icon_stop
        }
    }

    fn quads(regions: &Regions) -> Vec<AttributeXyUv> {
        let Regions {
            areas: Areas {
                status_bar,
                waveform,
                ..
            },
            proportions: Proportions {
                top_bar_height,
                ..
            },
            window_size,
            ..
        } = regions;

        let window_width = window_size.width;
        let window_height = window_size.height;
        let text_width = TEXT_CANVAS_WIDTH as f32;
        let text_height = TEXT_CANVAS_HEIGHT as f32;

        let waveform_half_y = waveform.y + (status_bar.y - waveform.y) / 2.0;

        vec![
            // Mono Waveform

            /* ↙ */  AttributeXyUv { a_xy: [0.0, status_bar.y], a_uv: [0.0, 1.0] },
            /* ↘ */  AttributeXyUv { a_xy: [window_width, status_bar.y], a_uv: [1.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, waveform.y], a_uv: [1.0, 0.0] },

            /* ↙ */  AttributeXyUv { a_xy: [0.0, status_bar.y], a_uv: [0.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, waveform.y], a_uv: [1.0, 0.0] },
            /* ↖ */  AttributeXyUv { a_xy: [0.0, waveform.y], a_uv: [0.0, 0.0] },

            // Stereo L Waveform

            /* ↙ */  AttributeXyUv { a_xy: [0.0, waveform_half_y], a_uv: [0.0, 1.0] },
            /* ↘ */  AttributeXyUv { a_xy: [window_width, waveform_half_y], a_uv: [1.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, waveform.y], a_uv: [1.0, 0.0] },

            /* ↙ */  AttributeXyUv { a_xy: [0.0, waveform_half_y], a_uv: [0.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, waveform.y], a_uv: [1.0, 0.0] },
            /* ↖ */  AttributeXyUv { a_xy: [0.0, waveform.y], a_uv: [0.0, 0.0] },

            // Stereo R Waveform

            /* ↙ */  AttributeXyUv { a_xy: [0.0, status_bar.y], a_uv: [0.0, 1.0] },
            /* ↘ */  AttributeXyUv { a_xy: [window_width, status_bar.y], a_uv: [1.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, waveform_half_y], a_uv: [1.0, 0.0] },

            /* ↙ */  AttributeXyUv { a_xy: [0.0, status_bar.y], a_uv: [0.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, waveform_half_y], a_uv: [1.0, 0.0] },
            /* ↖ */  AttributeXyUv { a_xy: [0.0, waveform_half_y], a_uv: [0.0, 0.0] },

            // Widget

            /* ↙ */  AttributeXyUv { a_xy: [ 720.0, 620.0], a_uv: [0.0, 1.0] },
            /* ↘ */  AttributeXyUv { a_xy: [1140.0, 620.0], a_uv: [1.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [1140.0, 460.0], a_uv: [1.0, 0.0] },

            /* ↙ */  AttributeXyUv { a_xy: [ 720.0, 620.0], a_uv: [0.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [1140.0, 460.0], a_uv: [1.0, 0.0] },
            /* ↖ */  AttributeXyUv { a_xy: [ 720.0, 460.0], a_uv: [0.0, 0.0] },

            // Swipe Widget Diamond

            /* ← */  AttributeXyUv { a_xy: [ 0.0, 140.0], a_uv: [0.0, 0.5] },
            /* ↓ */  AttributeXyUv { a_xy: [140.0, 280.0], a_uv: [0.5, 1.0] },
            /* → */  AttributeXyUv { a_xy: [280.0, 140.0], a_uv: [1.0, 0.5] },

            /* ← */  AttributeXyUv { a_xy: [ 0.0, 140.0], a_uv: [0.0, 0.5] },
            /* → */  AttributeXyUv { a_xy: [280.0, 140.0], a_uv: [1.0, 0.5] },
            /* ↑ */  AttributeXyUv { a_xy: [ 140.0, 0.0], a_uv: [0.5, 0.0] },

            // Text

            /* ↙ */  AttributeXyUv { a_xy: [0.0, text_height], a_uv: [0.0, 1.0] },
            /* ↘ */  AttributeXyUv { a_xy: [text_width, text_height], a_uv: [1.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [text_width, 0.0], a_uv: [1.0, 0.0] },

            /* ↙ */  AttributeXyUv { a_xy: [0.0, text_height], a_uv: [0.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [text_width, 0.0], a_uv: [1.0, 0.0] },
            /* ↖ */  AttributeXyUv { a_xy: [0.0, 0.0], a_uv: [0.0, 0.0] },

            // Top bar

            /* ↙ */  AttributeXyUv { a_xy: [0.0, *top_bar_height], a_uv: [0.0, 1.0] },
            /* ↘ */  AttributeXyUv { a_xy: [window_width, *top_bar_height], a_uv: [1.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, 0.0], a_uv: [1.0, 0.0] },

            /* ↙ */  AttributeXyUv { a_xy: [0.0, *top_bar_height], a_uv: [0.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, 0.0], a_uv: [1.0, 0.0] },
            /* ↖ */  AttributeXyUv { a_xy: [0.0, 0.0], a_uv: [0.0, 0.0] },

            // Status bar

            /* ↙ */  AttributeXyUv { a_xy: [0.0, window_height], a_uv: [0.0, 1.0] },
            /* ↘ */  AttributeXyUv { a_xy: [window_width, window_height], a_uv: [1.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, status_bar.y], a_uv: [1.0, 0.0] },

            /* ↙ */  AttributeXyUv { a_xy: [0.0, window_height], a_uv: [0.0, 1.0] },
            /* ↗ */  AttributeXyUv { a_xy: [window_width, status_bar.y], a_uv: [1.0, 0.0] },
            /* ↖ */  AttributeXyUv { a_xy: [0.0, status_bar.y], a_uv: [0.0, 0.0] }
        ]
    }

    pub fn update(&mut self, regions: &Regions) {
        self.quads = Self::quads(regions);
    }
}
