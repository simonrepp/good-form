//! Theme definitions in graphics memory compatible low level aligned struct representations
//!
//! Note that the alignment of all structs and fields is super crucial here because
//! data is copied to GPU uniform buffers verbatim and therefore the graphics memory
//! alignment rules need to be respected.

mod dark;
mod light;

pub use dark::DARK_THEME_DATA;
pub use light::LIGHT_THEME_DATA;

#[repr(C, align(16))]
pub struct RgbAligned16 {
    pub r: f32,
    pub g: f32,
    pub b: f32
}

#[repr(C)]
pub struct RgbaAligned16 {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

#[repr(C)]
pub struct ThemeDataAligned112 {
    pub background: RgbAligned16,
    pub widget_surface: RgbaAligned16,
    pub widget_shadow: RgbaAligned16,
    pub text: RgbaAligned16,
    pub playhead: RgbaAligned16,
    pub waveform: RgbaAligned16,
    pub waveform_centerline: RgbaAligned16
}
