//! Abstractions for allocating/wrapping buffers along with memory and memory requirements

use gfx_hal::{
    adapter::Adapter,
    Backend,
    buffer::Usage,
    memory::{Properties, Requirements},
    prelude::*
};
use std::{
    mem::ManuallyDrop,
    ptr
};

pub struct Buffer<B: Backend> {
    pub buffer: ManuallyDrop<B::Buffer>,
    pub memory: ManuallyDrop<B::Memory>,
    pub requirements: Requirements
}

impl<B> Buffer<B>
where
    B: Backend
{
    pub fn new(
        adapter: &Adapter<B>,
        device: &B::Device,
        size: u64,
        usage: Usage
    ) -> Buffer<B> {
        let mut buffer = unsafe { device.create_buffer(size, usage) }.unwrap();

        let requirements = unsafe { device.get_buffer_requirements(&buffer) };

        let memory_types = adapter.physical_device.memory_properties().memory_types;
        let upload_type = memory_types
            .iter()
            .enumerate()
            .position(|(id, mem_type)| {
                requirements.type_mask & (1 << id) != 0 &&
                mem_type.properties.contains(Properties::CPU_VISIBLE)
            })
            .unwrap()
            .into();

        let memory = unsafe {
            let memory = device.allocate_memory(upload_type, requirements.size).unwrap();
            device.bind_buffer_memory(&memory, 0, &mut buffer).unwrap();

            memory
        };

        Buffer {
            buffer: ManuallyDrop::new(buffer),
            memory: ManuallyDrop::new(memory),
            requirements
        }
    }

    pub unsafe fn manually_drop(&self, device: &B::Device) {
        device.destroy_buffer(ManuallyDrop::into_inner(ptr::read(&self.buffer)));
        device.free_memory(ManuallyDrop::into_inner(ptr::read(&self.memory)));
    }
}
