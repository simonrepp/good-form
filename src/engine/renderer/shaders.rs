//! Functions for retrieving readily compiled and correctly aligned SPIR-V shaders used in Good Form

fn as_u32_slice(u8_slice: &[u8]) -> &[u32] {
    unsafe {
        std::slice::from_raw_parts(
            u8_slice.as_ptr() as *const u32,
            u8_slice.len() / std::mem::size_of::<u32>()
        )
    }
}

pub fn frag_icon() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/icon.frag.spv")))
}

pub fn frag_input_widget() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/input_widget.frag.spv")))
}

pub fn frag_swipe_widget() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/swipe_widget.frag.spv")))
}

pub fn frag_surface() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/surface.frag.spv")))
}

pub fn frag_text() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/text.frag.spv")))
}

pub fn frag_waveform() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/waveform.frag.spv")))
}

pub fn vert_xy_rgba_translate() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/xy_rgba_translate.vert.spv")))
}

pub fn vert_xy_uv_translate() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/xy_uv_translate.vert.spv")))
}

pub fn vert_xy_uv() -> &'static [u32] {
    as_u32_slice(include_bytes!(concat!(env!("OUT_DIR"), "/xy_uv.vert.spv")))
}
