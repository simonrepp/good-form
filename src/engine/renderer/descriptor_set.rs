//! Bundles together a set layout and a singular set created from it

use gfx_hal::{
    Backend,
    prelude::*
};
use std::{
    mem::ManuallyDrop,
    ptr
};

pub struct DescriptorSet<B: Backend> {
    pub set: B::DescriptorSet,
    pub set_layout: ManuallyDrop<B::DescriptorSetLayout>,
}

impl<B> DescriptorSet<B>
where
    B: Backend
{
    pub fn new(pool: &mut B::DescriptorPool, layout: B::DescriptorSetLayout) -> DescriptorSet<B> {
        let set = unsafe { pool.allocate_set(&layout) }.unwrap();

        DescriptorSet {
            set_layout: ManuallyDrop::new(layout),
            set
        }
    }

    pub unsafe fn manually_drop(&self, device: &B::Device) {
        device.destroy_descriptor_set_layout(ManuallyDrop::into_inner(ptr::read(&self.set_layout)));
    }
}
