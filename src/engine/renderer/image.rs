//! Encapsulates creation, storage and destruction of image, image_view, memory and requirements

use gfx_hal::{
    adapter::Adapter,
    Backend,
    format::{Format, Swizzle},
    image::{
        self,
        Kind,
        Size,
        Tiling,
        ViewCapabilities,
        ViewKind
    },
    memory::{Properties, Requirements},
    prelude::*
};
use std::{
    mem::ManuallyDrop,
    ptr
};

use super::COLOR_RANGE;

pub struct Image<B: Backend> {
    pub image: ManuallyDrop<B::Image>,
    pub image_view: ManuallyDrop<B::ImageView>,
    pub memory: ManuallyDrop<B::Memory>,
    pub requirements: Requirements
}

impl<B> Image<B>
where
    B: Backend
{
    pub fn new(
        adapter: &Adapter<B>,
        device: &B::Device,
        format: Format,
        width: u32,
        height: u32
    ) -> Image<B> {
        let memory_types = adapter.physical_device.memory_properties().memory_types;
        let kind = Kind::D2(width as Size, height as Size, 1, 1);

        let mut image = ManuallyDrop::new(
            unsafe {
                device.create_image(
                    kind,
                    1,
                    format,
                    Tiling::Optimal,
                    image::Usage::TRANSFER_DST | image::Usage::SAMPLED,
                    ViewCapabilities::empty()
                )
            }.unwrap()
        );

        let requirements = unsafe { device.get_image_requirements(&image) };

        let device_type = memory_types
            .iter()
            .enumerate()
            .position(|(id, memory_type)| {
                requirements.type_mask & (1 << id) != 0 &&
                memory_type.properties.contains(Properties::DEVICE_LOCAL)
            })
            .unwrap()
            .into();

        let memory = unsafe {
            device.allocate_memory(device_type, requirements.size)
        }.unwrap();

        unsafe { device.bind_image_memory(&memory, 0, &mut image) }.unwrap();

        let image_view = unsafe {
            device.create_image_view(
                &image,
                ViewKind::D2,
                format,
                Swizzle::NO,
                COLOR_RANGE.clone()
            )
        }.unwrap();

        Image {
            image: image,
            image_view: ManuallyDrop::new(image_view),
            memory: ManuallyDrop::new(memory),
            requirements: requirements
        }
    }

    pub unsafe fn manually_drop(&self, device: &B::Device) {
        device.destroy_image(ManuallyDrop::into_inner(ptr::read(&self.image)));
        device.destroy_image_view(ManuallyDrop::into_inner(ptr::read(&self.image_view)));
        device.free_memory(ManuallyDrop::into_inner(ptr::read(&self.memory)));
    }
}
