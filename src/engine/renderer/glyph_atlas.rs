use euclid::{Point2D, Size2D};
use font_kit::{
    canvas::{Canvas, Format, RasterizationOptions},
    hinting::HintingOptions,
    loader::FontTransform,
    loaders::freetype,
};
use harfbuzz_rs::{Face, Feature, Owned, Tag, UnicodeBuffer};
use std::{collections::HashMap, ops::Range, sync::Arc};

use crate::engine::regions::Regions;

const ATLAS_SQUARE_WIDTH: u32 = 128;
const CHARACTER_SET: &str = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!?()[]|\\/-+=:,. ffl fi";
const FONT_BYTES: &'static [u8] = include_bytes!("fonts/news_cycle/regular.ttf");
const GLYPH_TEXTURE_PADDING: f32 = 1.0;
const QUESTIONMARK_CODEPOINT: u32 = 34;

pub struct ShapedGlyph {
    pub codepoint: u32,
    pub x_advance: f32,
    pub x_offset: f32,
    pub y_advance: f32,
    pub y_offset: f32
}

pub struct RenderedGlyph {
    pub height: f32,
    pub vertex_range: Range<u32>,
    pub u_max: f32,
    pub u_min: f32,
    pub v_max: f32,
    pub v_min: f32,
    pub width: f32
}

pub struct GlyphAtlas<'a> {
    pub canvas: Canvas,
    pub glyph_map: HashMap<u32, RenderedGlyph>,
    harfbuzz_font: Owned<harfbuzz_rs::Font<'a>>
}

impl<'a> GlyphAtlas<'a> {
    pub fn new(regions: &Regions) -> GlyphAtlas<'a> {
        let face = Face::from_bytes(FONT_BYTES, 0);
        let scale = (face.upem() as f32 * (regions.proportions.font_size / 32.0)) as i32;
        let mut harfbuzz_font = harfbuzz_rs::Font::new(face);

        harfbuzz_font.set_scale(scale, scale);

        let font_bytes_refcounted = Arc::new(FONT_BYTES.iter().cloned().collect());
        let font_kit_font = freetype::Font::from_bytes(font_bytes_refcounted, 0).unwrap();

        let canvas_size = Size2D::new(ATLAS_SQUARE_WIDTH, ATLAS_SQUARE_WIDTH);
        let mut canvas = Canvas::new(&canvas_size, Format::A8);

        let buffer = UnicodeBuffer::new().add_str(CHARACTER_SET);
        let shaped_text = harfbuzz_rs::shape(&harfbuzz_font, buffer, &[]);

        let infos = shaped_text.get_glyph_infos();

        let mut max_descent = 0_f32;
        let mut x_offset = GLYPH_TEXTURE_PADDING;
        let mut y_offset = GLYPH_TEXTURE_PADDING + regions.proportions.font_size;

        let mut glyph_map = HashMap::new();

        for info in infos {
            if glyph_map.contains_key(&info.codepoint) {
                // We use ' ' as separator for special character combinations like 'ffl'
                // therefore we need to skip ' ' after we first rendered and cached it.
                continue;
            }

            let extents = harfbuzz_font.get_glyph_extents(info.codepoint).unwrap();

            let width = (extents.width + extents.x_bearing) as f32 / 64.0;
            let descent = (-extents.height - extents.y_bearing) as f32 / 64.0;
            let height = regions.proportions.font_size + descent;

            let mut next_x_offset = x_offset + GLYPH_TEXTURE_PADDING + width;

            if next_x_offset > ATLAS_SQUARE_WIDTH as f32 {
                x_offset = GLYPH_TEXTURE_PADDING;
                y_offset += regions.proportions.font_size + max_descent + GLYPH_TEXTURE_PADDING;
                max_descent = descent;
                next_x_offset = width + 2.0 * GLYPH_TEXTURE_PADDING;
            } else {
                max_descent = max_descent.max(descent);
            }

            font_kit_font.rasterize_glyph(
                &mut canvas,
                info.codepoint,
                regions.proportions.font_size,
                &FontTransform::identity(),
                &Point2D::new(x_offset, y_offset),
                HintingOptions::None,
                RasterizationOptions::GrayscaleAa
            ).unwrap();

            let rendered_glyph = RenderedGlyph {
                height,
                vertex_range: 0..0,
                u_max: (x_offset + width) / ATLAS_SQUARE_WIDTH as f32,
                u_min: x_offset / ATLAS_SQUARE_WIDTH as f32,
                v_max: (y_offset - regions.proportions.font_size) / ATLAS_SQUARE_WIDTH as f32,
                v_min: (y_offset + descent) / ATLAS_SQUARE_WIDTH as f32,
                width
            };

            glyph_map.insert(info.codepoint, rendered_glyph);

            x_offset = next_x_offset;
        }

        GlyphAtlas {
            canvas,
            glyph_map,
            harfbuzz_font
        }
    }

    pub fn get_rendered_glyph(&self, codepoint: u32) -> &RenderedGlyph {
        match self.glyph_map.get(&codepoint) {
            Some(glyph) => glyph,
            None => {
                eprintln!("Unavailable rendered glyph {} requested", codepoint);

                self.glyph_map.get(&QUESTIONMARK_CODEPOINT).unwrap()
            }
        }
    }

    pub fn shape(&mut self, text: String, numerals: bool) -> Vec<ShapedGlyph> {
        let buffer = UnicodeBuffer::new().add_str(&text);

        let shaped_text = if numerals {
            let tnum_tag = Tag::new('t', 'n', 'u', 'm'); // tnum = replace numerals with glyphs of uniform width
            let tnum_feature = Feature::new(tnum_tag, 0, 0..);

            harfbuzz_rs::shape(&self.harfbuzz_font, buffer, &[tnum_feature])
        } else {
            harfbuzz_rs::shape(&self.harfbuzz_font, buffer, &[])
        };

        let positions = shaped_text.get_glyph_positions();
        let infos = shaped_text.get_glyph_infos();

        positions.iter().zip(infos).map(|(positions, info)| {
            ShapedGlyph {
                codepoint: info.codepoint,
                x_advance: positions.x_advance as f32 / 64.0,
                x_offset: positions.x_offset as f32 / 64.0,
                y_advance: positions.y_advance as f32 / 64.0,
                y_offset: positions.y_offset as f32 / 64.0
            }
        }).collect()
    }
}
