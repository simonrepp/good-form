//! Static theme data for the dark theme

use super::{
    RgbAligned16,
    RgbaAligned16,
    ThemeDataAligned112
};

pub const DARK_THEME_DATA: ThemeDataAligned112 = ThemeDataAligned112 {
    background: RgbAligned16 {
        r: 0.2,
        g: 0.2,
        b: 0.2
    },
    widget_surface: RgbaAligned16 {
        r: 0.3,
        g: 0.3,
        b: 0.3,
        a: 1.0
    },
    widget_shadow: RgbaAligned16 {
        r: 0.0,
        g: 0.0,
        b: 0.0,
        a: 1.0
    },
    text: RgbaAligned16 {
        r: 0.9,
        g: 0.9,
        b: 0.9,
        a: 1.0
    },
    playhead: RgbaAligned16 {
        r: 1.0,
        g: 1.0,
        b: 1.0,
        a: 1.0
    },
    waveform: RgbaAligned16 {
        r: 0.4,
        g: 0.4,
        b: 0.4,
        a: 1.0
    },
    waveform_centerline: RgbaAligned16 {
        r: 0.5,
        g: 0.5,
        b: 0.5,
        a: 1.0
    }
};
