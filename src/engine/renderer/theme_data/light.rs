//! Static theme data for the light theme

use super::{
    RgbAligned16,
    RgbaAligned16,
    ThemeDataAligned112
};

pub const LIGHT_THEME_DATA: ThemeDataAligned112 = ThemeDataAligned112 {
    background: RgbAligned16 {
        r: 0.8,
        g: 0.8,
        b: 0.8
    },
    widget_surface: RgbaAligned16 {
        r: 0.9,
        g: 0.9,
        b: 0.9,
        a: 1.0
    },
    widget_shadow: RgbaAligned16 {
        r: 0.0,
        g: 0.0,
        b: 0.0,
        a: 1.0
    },
    text: RgbaAligned16 {
        r: 0.1,
        g: 0.1,
        b: 0.1,
        a: 1.0
    },
    playhead: RgbaAligned16 {
        r: 0.0,
        g: 0.0,
        b: 0.0,
        a: 1.0
    },
    waveform: RgbaAligned16 {
        r: 0.6,
        g: 0.6,
        b: 0.6,
        a: 1.0
    },
    waveform_centerline: RgbaAligned16 {
        r: 0.5,
        g: 0.5,
        b: 0.5,
        a: 1.0
    }
};
