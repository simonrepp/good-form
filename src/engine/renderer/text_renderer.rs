use euclid::{Point2D, Size2D};
use font_kit::{
    canvas::{Canvas, Format, RasterizationOptions},
    hinting::HintingOptions,
    loader::FontTransform,
    loaders::freetype,
};
use harfbuzz_rs::{
    Face,
    Owned,
    UnicodeBuffer
};
use std::sync::Arc;

const FONT_BYTES: &'static [u8] = include_bytes!("fonts/news_cycle/regular.ttf");
const FONT_SIZE: f32 = 18.0;
pub const TEXT_CANVAS_WIDTH: u32 = 1280;
pub const TEXT_CANVAS_HEIGHT: u32 = 720;

pub struct TextRenderer<'a> {
    pub canvas: Canvas,
    font_kit_font: freetype::Font,
    harfbuzz_font: Owned<harfbuzz_rs::Font<'a>>
}

impl<'a> TextRenderer<'a> {
    pub fn new() -> TextRenderer<'a> {
        let face = Face::from_bytes(FONT_BYTES, 0);
        let scale = (face.upem() as f32 * (FONT_SIZE / 32.0)) as i32;
        let mut harfbuzz_font = harfbuzz_rs::Font::new(face);

        harfbuzz_font.set_scale(scale, scale);

        let font_bytes_refcounted = Arc::new(FONT_BYTES.iter().cloned().collect());
        let font_kit_font = freetype::Font::from_bytes(font_bytes_refcounted, 0).unwrap();

        let canvas_size = Size2D::new(TEXT_CANVAS_WIDTH, TEXT_CANVAS_HEIGHT);
        let canvas = Canvas::new(&canvas_size, Format::A8);

        TextRenderer {
            canvas,
            font_kit_font,
            harfbuzz_font
        }
    }

    /// Note that the positions info (e.g. x_advance) is provided by harfbuzz as
    /// the 64-fold value of the original font_size reference unit, therefore we
    /// need to divide these values by 64 to translate them back into our spatial
    /// frame of reference.
    /// https://stackoverflow.com/questions/50292283/units-used-by-hb-position-t-in-harfbuzz
    pub fn render_canvas(&mut self, lines: Vec<String>) {
        for y in 0..self.canvas.size.height {
            for x in 0..self.canvas.size.width {
                self.canvas.pixels[(y * self.canvas.size.width + x) as usize] = 0;
            }
        }

        let mut y_offset = FONT_SIZE * 2.0;

        for line in lines {
            let buffer = UnicodeBuffer::new().add_str(&line);
            let shaped_text = harfbuzz_rs::shape(&self.harfbuzz_font, buffer, &[]);

            let positions = shaped_text.get_glyph_positions();
            let infos = shaped_text.get_glyph_infos();

            let mut x_offset: f32 = FONT_SIZE;

            for (position, info) in positions.iter().zip(infos) {
                self.font_kit_font.rasterize_glyph(
                    &mut self.canvas,
                    info.codepoint,
                    FONT_SIZE,
                    &FontTransform::identity(),
                    &Point2D::new(x_offset, y_offset),
                    HintingOptions::None,
                    RasterizationOptions::GrayscaleAa
                ).unwrap();

                x_offset += position.x_advance as f32 / 64.0;
            }

            y_offset += FONT_SIZE;
        }
    }
}
