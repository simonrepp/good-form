//! Encapsulates the creation, storage and destruction of the application-tailored graphic pipelines used in Good Form

use gfx_hal::{
    Backend,
    prelude::*,
    pso::ShaderStageFlags
};
use std::{
    iter,
    mem::ManuallyDrop
};

use super::DescriptorSets;
use super::geometry::AttributeType;
use super::pipeline::Pipeline;
use super::shaders;

pub struct Pipelines<B: Backend> {
    pub glyph_atlas_pipeline: ManuallyDrop<Pipeline<B>>,
    pub icon_pipeline: ManuallyDrop<Pipeline<B>>,
    pub surface_pipeline: ManuallyDrop<Pipeline<B>>,
    pub swipe_widget_pipeline: ManuallyDrop<Pipeline<B>>,
    pub text_pipeline: ManuallyDrop<Pipeline<B>>,
    pub waveform_pipeline: ManuallyDrop<Pipeline<B>>,
    pub widget_pipeline: ManuallyDrop<Pipeline<B>>,
}

impl<B> Pipelines<B>
where
    B: Backend
{
    pub fn new(
        device: &B::Device,
        render_pass: &ManuallyDrop<B::RenderPass>,
        descriptor_sets: &DescriptorSets<B>
    ) -> Pipelines<B> {
        let glyph_atlas_pipeline_layout = unsafe {
            device.create_pipeline_layout(
                vec![
                    &*descriptor_sets.stable_interface_globals.set_layout,
                    &*descriptor_sets.glyph_atlas.set_layout
                ],
                &[(ShaderStageFlags::VERTEX, 0..8)]
            )
        }.expect("Can't create pipeline layout");

        let glyph_atlas_pipeline = Pipeline::<B>::new(
            &device,
            &render_pass,
            glyph_atlas_pipeline_layout,
            AttributeType::XyUv,
            shaders::vert_xy_uv_translate(),
            shaders::frag_text()
        );

        let icon_pipeline_layout = unsafe {
            device.create_pipeline_layout(
                iter::once(&*descriptor_sets.stable_interface_globals.set_layout),
                &[(ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT, 0..12)]
            )
        }.expect("Can't create pipeline layout");

        let icon_pipeline = Pipeline::<B>::new(
            &device,
            &render_pass,
            icon_pipeline_layout,
            AttributeType::XyRgba,
            shaders::vert_xy_rgba_translate(),
            shaders::frag_icon()
        );

        let surface_pipeline_layout = unsafe {
            device.create_pipeline_layout(
                iter::once(&*descriptor_sets.stable_interface_globals.set_layout),
                &[]
            )
        }.expect("Can't create pipeline layout");

        let surface_pipeline = Pipeline::<B>::new(
            &device,
            &render_pass,
            surface_pipeline_layout,
            AttributeType::XyUv,
            shaders::vert_xy_uv(),
            shaders::frag_surface()
        );

        let swipe_widget_pipeline_layout = unsafe {
            device.create_pipeline_layout(
                iter::once(&*descriptor_sets.stable_interface_globals.set_layout),
                &[(ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT, 0..12)]
            )
        }.expect("Can't create pipeline layout");

        let swipe_widget_pipeline = Pipeline::<B>::new(
            &device,
            &render_pass,
            swipe_widget_pipeline_layout,
            AttributeType::XyUv,
            shaders::vert_xy_uv_translate(),
            shaders::frag_swipe_widget()
        );

        let text_pipeline_layout = unsafe {
            device.create_pipeline_layout(
                vec![
                    &*descriptor_sets.stable_interface_globals.set_layout,
                    &*descriptor_sets.text.set_layout
                ],
                &[]
            )
        }.expect("Can't create pipeline layout");

        let text_pipeline = Pipeline::<B>::new(
            &device,
            &render_pass,
            text_pipeline_layout,
            AttributeType::XyUv,
            shaders::vert_xy_uv(), // TODO: This is reused in other pipelines, reuse it thus instead of repeatedly calling
            shaders::frag_text()
        );

        let waveform_pipeline_layout = unsafe {
            device.create_pipeline_layout(
                vec![
                    &*descriptor_sets.stable_interface_globals.set_layout,
                    &*descriptor_sets.volatile_interface_globals.set_layout,
                    &*descriptor_sets.waveform.set_layout
                ],
                &[(ShaderStageFlags::FRAGMENT, 0..4)]
            )
        }.expect("Can't create pipeline layout");

        let waveform_pipeline = Pipeline::<B>::new(
            &device,
            &render_pass,
            waveform_pipeline_layout,
            AttributeType::XyUv,
            shaders::vert_xy_uv(),
            shaders::frag_waveform()
        );

        let widget_pipeline_layout = unsafe {
            device.create_pipeline_layout(
                iter::once(&*descriptor_sets.stable_interface_globals.set_layout),
                &[]
            )
        }.expect("Can't create pipeline layout");

        let widget_pipeline = Pipeline::<B>::new(
            &device,
            &render_pass,
            widget_pipeline_layout,
            AttributeType::XyUv,
            shaders::vert_xy_uv(),
            shaders::frag_input_widget()
        );

        Pipelines {
            glyph_atlas_pipeline: ManuallyDrop::new(glyph_atlas_pipeline),
            icon_pipeline: ManuallyDrop::new(icon_pipeline),
            surface_pipeline: ManuallyDrop::new(surface_pipeline),
            swipe_widget_pipeline: ManuallyDrop::new(swipe_widget_pipeline),
            text_pipeline: ManuallyDrop::new(text_pipeline),
            waveform_pipeline: ManuallyDrop::new(waveform_pipeline),
            widget_pipeline: ManuallyDrop::new(widget_pipeline)
        }
    }

    pub unsafe fn manually_drop(&self, device: &B::Device) {
        self.glyph_atlas_pipeline.manually_drop(device);
        self.icon_pipeline.manually_drop(device);
        self.surface_pipeline.manually_drop(device);
        self.swipe_widget_pipeline.manually_drop(device);
        self.text_pipeline.manually_drop(device);
        self.waveform_pipeline.manually_drop(device);
        self.widget_pipeline.manually_drop(device);
    }
}
