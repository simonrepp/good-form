//! Semi-generic encapsulation of a pipeline and its creation routine

use gfx_hal::{
    Backend,
    format::Format,
    pass::Subpass,
    prelude::*,
    pso::{
        AttributeDesc,
        BlendState,
        ColorBlendDesc,
        ColorMask,
        Element,
        EntryPoint,
        GraphicsPipelineDesc,
        GraphicsShaderSet,
        Primitive,
        Rasterizer,
        Specialization,
        VertexBufferDesc,
        VertexInputRate
    }
};
use std::{
    mem::{self, ManuallyDrop},
    ptr
};

use super::geometry::{AttributeType, AttributeXyRgba, AttributeXyUv};

const ENTRY_NAME: &str = "main";

pub struct Pipeline<B: Backend> {
    pub pipeline: ManuallyDrop<B::GraphicsPipeline>,
    pub pipeline_layout: ManuallyDrop<B::PipelineLayout>
}

impl<B> Pipeline<B>
where
    B: Backend
{
    pub fn new(
        device: &B::Device,
        render_pass: &ManuallyDrop<B::RenderPass>,
        pipeline_layout: B::PipelineLayout,
        attribute_type: AttributeType,
        vert_shader: &[u32],
        frag_shader: &[u32]
    ) -> Pipeline<B> {
        let vert_shader_module = unsafe {
            device.create_shader_module(&vert_shader)
        }.unwrap();

        let frag_shader_module = unsafe {
            device.create_shader_module(&frag_shader)
        }.unwrap();

        let pipeline = {
            let vert_shader_entry = EntryPoint {
                entry: ENTRY_NAME,
                module: &vert_shader_module,
                specialization: gfx_hal::spec_const_list![0.8f32]
            };

            let frag_shader_entry = EntryPoint {
                entry: ENTRY_NAME,
                module: &frag_shader_module,
                specialization: Specialization::default()
            };

            let shader_entries = GraphicsShaderSet {
                vertex: vert_shader_entry,
                hull: None,
                domain: None,
                geometry: None,
                fragment: Some(frag_shader_entry)
            };

            let subpass = Subpass {
                index: 0,
                main_pass: &**render_pass
            };

            let mut pipeline_desc = GraphicsPipelineDesc::new(
                shader_entries,
                Primitive::TriangleList,
                Rasterizer::FILL,
                &pipeline_layout,
                subpass
            );

            pipeline_desc.blender.targets.push(ColorBlendDesc {
                mask: ColorMask::ALL,
                blend: Some(BlendState::ALPHA)
            });

            pipeline_desc.vertex_buffers.push(VertexBufferDesc {
                binding: 0,
                stride: match attribute_type {
                    AttributeType::XyRgba => mem::size_of::<AttributeXyRgba>() as u32,
                    AttributeType::XyUv => mem::size_of::<AttributeXyUv>() as u32
                },
                rate: VertexInputRate::Vertex
            });



            pipeline_desc.attributes.push(AttributeDesc {
                location: 0,
                binding: 0,
                element: Element {
                    format: Format::Rg32Sfloat,
                    offset: 0
                }
            });

            pipeline_desc.attributes.push(AttributeDesc {
                location: 1,
                binding: 0,
                element: Element {
                    format: match attribute_type {
                        AttributeType::XyRgba => Format::Rgba8Unorm,
                        AttributeType::XyUv => Format::Rg32Sfloat
                    },
                    offset: 8
                },
            });

            unsafe { device.create_graphics_pipeline(&pipeline_desc, None) }.unwrap()
        };

        unsafe {
            device.destroy_shader_module(vert_shader_module);
            device.destroy_shader_module(frag_shader_module);
        }

        Pipeline {
            pipeline: ManuallyDrop::new(pipeline),
            pipeline_layout: ManuallyDrop::new(pipeline_layout)
        }
    }

    pub unsafe fn manually_drop(&self, device: &B::Device) {
        device.destroy_graphics_pipeline(ManuallyDrop::into_inner(ptr::read(&self.pipeline)));
        device.destroy_pipeline_layout(ManuallyDrop::into_inner(ptr::read(&self.pipeline_layout)));
    }
}
