//! Encapsulates creation of descriptor sets, their layouts, and the pool used for their creation

use gfx_hal::{
    Backend,
    prelude::*,
    pso::{
        BufferDescriptorFormat,
        BufferDescriptorType,
        DescriptorPoolCreateFlags,
        DescriptorRangeDesc,
        DescriptorSetLayoutBinding,
        DescriptorType,
        ImageDescriptorType,
        ShaderStageFlags
    }
};
use std::{
    mem::ManuallyDrop,
    ptr
};

use super::descriptor_set::DescriptorSet;

pub struct DescriptorSets<B: Backend> {
    pool: ManuallyDrop<B::DescriptorPool>,
    pub glyph_atlas: ManuallyDrop<DescriptorSet<B>>,
    pub stable_interface_globals: ManuallyDrop<DescriptorSet<B>>,
    pub volatile_interface_globals: ManuallyDrop<DescriptorSet<B>>,
    pub text: ManuallyDrop<DescriptorSet<B>>,
    pub waveform: ManuallyDrop<DescriptorSet<B>>
}

impl<B> DescriptorSets<B>
where
    B: Backend
{
    pub fn new(device: &B::Device) -> DescriptorSets<B> {
        let uniform_buffer_descriptor_type = DescriptorType::Buffer {
            format: BufferDescriptorFormat::Structured { dynamic_offset: false },
            ty: BufferDescriptorType::Uniform
        };

        let sampled_image_descriptor_type = DescriptorType::Image {
            // TODO: Clarify significance of the with_sampler flag
            //       (and why with_sampler: true thus segfaults)
            ty: ImageDescriptorType::Sampled { with_sampler: false }
        };

        let mut pool = unsafe {
            device.create_descriptor_pool(
                5, // sets
                &[
                    DescriptorRangeDesc {
                        ty: uniform_buffer_descriptor_type,
                        count: 2
                    },
                    DescriptorRangeDesc {
                        ty: sampled_image_descriptor_type,
                        count: 3
                    },
                    DescriptorRangeDesc {
                        ty: DescriptorType::Sampler,
                        count: 3
                    }
                ],
                DescriptorPoolCreateFlags::empty()
            )
        }.expect("Can't create descriptor pool");

        let glyph_atlas_layout = unsafe {
            device.create_descriptor_set_layout(
                &[
                    DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: sampled_image_descriptor_type,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    },
                    DescriptorSetLayoutBinding {
                        binding: 1,
                        ty: DescriptorType::Sampler,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    }
                ],
                &[]
            )
        }.expect("Can't create descriptor set layout");

        let glyph_atlas = DescriptorSet::new(&mut pool, glyph_atlas_layout);

        let stable_interface_globals_layout = unsafe {
            device.create_descriptor_set_layout(
                &[
                    DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: uniform_buffer_descriptor_type,
                        count: 1,
                        stage_flags: ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    }
                ],
                &[]
            )
        }.expect("Can't create descriptor set layout");

        let stable_interface_globals = DescriptorSet::new(&mut pool, stable_interface_globals_layout);

        let volatile_interface_globals_layout = unsafe {
            device.create_descriptor_set_layout(
                &[
                    DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: uniform_buffer_descriptor_type,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    }
                ],
                &[]
            )
        }.expect("Can't create descriptor set layout");

        let volatile_interface_globals = DescriptorSet::new(&mut pool, volatile_interface_globals_layout);

        let text_layout = unsafe {
            device.create_descriptor_set_layout(
                &[
                    DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: sampled_image_descriptor_type,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    },
                    DescriptorSetLayoutBinding {
                        binding: 1,
                        ty: DescriptorType::Sampler,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    }
                ],
                &[]
            )
        }.expect("Can't create descriptor set layout");

        let text = DescriptorSet::new(&mut pool, text_layout);

        let waveform_layout = unsafe {
            device.create_descriptor_set_layout(
                &[
                    DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: sampled_image_descriptor_type,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    },
                    DescriptorSetLayoutBinding {
                        binding: 1,
                        ty: DescriptorType::Sampler,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false
                    }
                ],
                &[]
            )
        }.expect("Can't create descriptor set layout");

        let waveform = DescriptorSet::new(&mut pool, waveform_layout);

        DescriptorSets {
            pool: ManuallyDrop::new(pool),
            glyph_atlas: ManuallyDrop::new(glyph_atlas),
            stable_interface_globals: ManuallyDrop::new(stable_interface_globals),
            volatile_interface_globals: ManuallyDrop::new(volatile_interface_globals),
            text: ManuallyDrop::new(text),
            waveform: ManuallyDrop::new(waveform)
        }
    }

    pub unsafe fn manually_drop(&self, device: &B::Device) {
        device.destroy_descriptor_pool(ManuallyDrop::into_inner(ptr::read(&self.pool)));

        self.glyph_atlas.manually_drop(device);
        self.stable_interface_globals.manually_drop(device);
        self.volatile_interface_globals.manually_drop(device);
        self.text.manually_drop(device);
        self.waveform.manually_drop(device);
    }
}
