use cpal::{
    traits::{DeviceTrait, EventLoopTrait, HostTrait},
    Format,
    SampleFormat,
    SampleRate,
    StreamData,
    UnknownTypeOutputBuffer
};
use std::{
    sync::Arc,
    sync::mpsc::{self, Receiver, Sender},
    thread
};

use crate::engine::{
    decoder::DecodedAudio,
    state::playback_device::PlaybackDevice
};

pub enum PlaybackResponse {
    Crashed(String),
    Online(String)
}

pub enum PlaybackSignal {
    Eject,
    Play,
    Pause,
    Seek(f64),
    SeekAndPlay(f64),
    Terminate,
    Use(Arc<DecodedAudio>)
}

pub enum PlaybackEngine {
    Crashed(String),
    Online {
        device_name: String,
        num_channels: u8,
        sample_rate: u32,
        response_receiver: Receiver<PlaybackResponse>,
        signal_sender: Sender<PlaybackSignal>
    },
    Offline
}

pub fn initialize(
    playback_device: PlaybackDevice,
    mut audio: Option<Arc<DecodedAudio>>,
    mut playing: bool
) -> PlaybackEngine {
    let (signal_sender, signal_receiver) = mpsc::channel();
    let (response_sender, response_receiver) = mpsc::channel();

    let (num_channels, sample_rate) = match audio {
        Some(ref audio) => (audio.num_channels, audio.sample_rate),
        None => (2, 44100)
    };

    thread::spawn(move || {
        let host = match playback_device {
            PlaybackDevice::UserDefined {
                host_index: Some(index),
                ..
            } => match cpal::available_hosts().iter().nth(index) {
                Some(host_id) => match cpal::host_from_id(*host_id) {
                    Ok(host) => host,
                    Err(err) => {
                        response_sender.send(PlaybackResponse::Crashed(err.to_string())).ok();
                        panic!();
                    }
                },
                None => {
                    let err = format!("Invalid host index {} (there is no host available at this index)", index);
                    response_sender.send(PlaybackResponse::Crashed(err)).ok();
                    panic!();
                }
            }
            _ => cpal::default_host()
        };

        let device = match playback_device {
            PlaybackDevice::UserDefined {
                device_index: Some(index),
                ..
            } => match host.output_devices() {
                Ok(mut devices_iter) => match devices_iter.nth(index) {
                    Some(device) => device,
                    None => {
                        let err = format!("Invalid device index {} (there is no device available at this index)", index);
                        response_sender.send(PlaybackResponse::Crashed(err)).ok();
                        panic!();
                    }
                }
                Err(err) => {
                    response_sender.send(PlaybackResponse::Crashed(err.to_string())).ok();
                    panic!();
                }
            }
            _ => match host.default_output_device() {
                Some(device) => device,
                None => {
                    let err = String::from("Failed to get default output device");
                    response_sender.send(PlaybackResponse::Crashed(err)).ok();
                    panic!();
                }
            }
        };

        // TODO: Proper iteration of supported output formats to see if we can use our format.
        let format = Format {
            channels: num_channels as u16,
            data_type: SampleFormat::F32,
            sample_rate: SampleRate(sample_rate)
        };

        let event_loop = host.event_loop();

        let stream_id = match event_loop.build_output_stream(&device, &format) {
            Ok(id) => id,
            Err(err) => {
                response_sender.send(PlaybackResponse::Crashed(err.to_string())).ok();
                panic!();
            }
        };

        if let Err(err) = event_loop.play_stream(stream_id) {
            response_sender.send(PlaybackResponse::Crashed(err.to_string())).ok();
            panic!();
        }

        let device_name = format!(
            "{}/{}",
            host.id().name(),
            device.name().unwrap_or(String::from("Unnamed"))
        );
        response_sender.send(PlaybackResponse::Online(device_name)).ok();

        let mut sample_index: usize = 0;

        event_loop.run(move |stream_id, stream_result| {
            while let Ok(signal) = signal_receiver.try_recv() {
                // TODO: When audio is None we probably shouldn't be listening
                //       to any but the Use signal (Neither should something
                //       else but the Use signal be transmitted from the main
                //       loop either). Room for improvement here and there.
                match signal {
                    PlaybackSignal::Eject => {
                        audio = None;
                        sample_index = 0;
                    }
                    PlaybackSignal::Play => {
                        playing = true;
                    }
                    PlaybackSignal::Pause => {
                        playing = false;
                    }
                    PlaybackSignal::Seek(timecode) => {
                        sample_index = (timecode * audio.as_ref().map_or(0.0, |audio| audio.sample_rate as f64)).floor() as usize;
                    }
                    PlaybackSignal::SeekAndPlay(timecode) => {
                        playing = true;
                        sample_index = (timecode * audio.as_ref().map_or(0.0, |audio| audio.sample_rate as f64)).floor() as usize;
                    }
                    PlaybackSignal::Terminate => panic!("Intentionally terminating playback engine"),
                    PlaybackSignal::Use(new_audio) => {
                        audio = Some(new_audio);
                        sample_index = 0;
                    }
                }
            }

            let stream_data = match stream_result {
                Ok(data) => data,
                Err(err) => {
                    eprintln!("an error occurred on stream {:?}: {}", stream_id, err);
                    return;
                }
            };

            match stream_data {
                // TODO: Consider/research whether we can drop the U16 and I16 implementations.
                //       Ideally we want to only have a floating point pipeline anyway?
                //       The question thus is whether there is hardware that does not support it?
                StreamData::Output { buffer: UnknownTypeOutputBuffer::U16(mut buffer) } => {
                    for elem in buffer.iter_mut() {
                        *elem = u16::max_value() / 2;
                    }
                }
                StreamData::Output { buffer: UnknownTypeOutputBuffer::I16(mut buffer) } => {
                    for elem in buffer.iter_mut() {
                        *elem = 0;
                    }
                }
                StreamData::Output { buffer: UnknownTypeOutputBuffer::F32(mut buffer) } => {
                    if audio.is_some() && playing {
                        let audio = audio.as_ref().unwrap();

                        for (index, output_sample) in buffer.iter_mut().enumerate() {
                            let channel = index % format.channels as usize;

                            *output_sample = audio.samples[channel][sample_index];

                            if channel == audio.num_channels as usize - 1 {
                                sample_index = (sample_index + 1) % audio.num_samples as usize;
                            }
                        }
                    } else {
                        for output_sample in buffer.iter_mut() {
                            *output_sample = 0.0;
                        }
                    }
                }
                _ => ()
            }
        });
    });

    PlaybackEngine::Online {
        device_name: String::from("Unknown device"),
        num_channels,
        response_receiver,
        sample_rate,
        signal_sender
    }
}
