use simplemad::Decoder;
use std::fs::File;
use std::path::PathBuf;

use super::{DecodedAudio, FileFormat, SampleFormat};

pub fn decode(file_path: PathBuf) -> DecodedAudio {
    let file = File::open(file_path).unwrap();
    let decoder = Decoder::decode(file).unwrap();

    let mut audio = DecodedAudio {
        file_format: FileFormat::Mp3,
        num_channels: 0,
        num_samples: 0,
        sample_format: SampleFormat::LossilyCompressed,
        sample_rate: 0,
        samples: vec![]
    };

    let mut first_frame = true;

    for decoding_result in decoder {
        match decoding_result {
            Ok(frame) => {
                if first_frame {
                    audio.num_channels = frame.samples.len() as u8;
                    audio.sample_rate = frame.sample_rate;

                    for _ in 0..audio.num_channels {
                        audio.samples.push(vec![]);
                    }

                    first_frame = false;
                }

                for channel in 0..audio.num_channels {
                    audio.samples[channel as usize].reserve(frame.samples[channel as usize].len());

                    for sample in &frame.samples[channel as usize] {
                        audio.samples[channel as usize].push(sample.to_f32());
                    }
                }
            }
            Err(_) => ()  // TODO: According to the documentation errors can mostly be ignored.
                          //       At some point we should also cover those cases where this is not the case.
        }
    }

    audio.num_samples = audio.samples[0].len() as u32;

    audio
}
