use lewton::inside_ogg::OggStreamReader;
use std::fs::File;
use std::path::PathBuf;

use super::{DecodedAudio, FileFormat, SampleFormat};

pub fn decode(file_path: PathBuf) -> DecodedAudio {
    let file = File::open(file_path).unwrap();
    let mut reader = OggStreamReader::new(file).unwrap();

    let mut audio = DecodedAudio {
        file_format: FileFormat::Ogg,
        num_channels: reader.ident_hdr.audio_channels,
        num_samples: 0,
        sample_format: SampleFormat::LossilyCompressed,
        sample_rate: reader.ident_hdr.audio_sample_rate,
        samples: (0..reader.ident_hdr.audio_channels).map(|_| vec![]).collect()
    };

    while let Some(packet_samples) = reader.read_dec_packet_itl().unwrap() {
        for channel in 0..audio.num_channels {
            audio.samples[channel as usize].reserve(packet_samples.len() / audio.num_channels as usize);
        }

        for (index, sample) in packet_samples.iter().enumerate() {
            audio.samples[index % audio.num_channels as usize].push(*sample as f32 / std::i16::MAX as f32);
        }
	}

    audio.num_samples = audio.samples[0].len() as u32;

    audio
}
