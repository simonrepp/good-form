use claxon::{Block, FlacReader};
use std::path::PathBuf;

use crate::constants::I24_MAX;
use super::{DecodedAudio, FileFormat, SampleFormat};

pub fn decode(file_path: PathBuf) -> DecodedAudio {
    let mut reader = FlacReader::open(file_path).unwrap();
    let streaminfo = reader.streaminfo();

    let mut frame_reader = reader.blocks();
    let mut block = Block::empty();

    let mut audio = DecodedAudio {
        file_format: FileFormat::Flac,
        num_channels: streaminfo.channels as u8,
        num_samples: 0,
        sample_format: match streaminfo.bits_per_sample {
            8 => SampleFormat::SignedInt8,
            16 => SampleFormat::SignedInt16,
            24 => SampleFormat::SignedInt24,

            // TODO: Research if there are other possible bitdepths and implement them too.
            //       The spec at https://xiph.org/flac/format.html seems to indicate
            //       that 12 and 20 bits per sample are also a possibility, but confirm
            //       this first before implementing it.
            _ => unimplemented!()
        },
        sample_rate: streaminfo.sample_rate,
        samples: (0..streaminfo.channels).map(|_| vec![]).collect()
    };

    loop {
        match frame_reader.read_next_or_eof(block.into_buffer()) {
            Ok(Some(next_block)) => block = next_block,
            Ok(None) => break,
            Err(error) => panic!("{}", error)
        }

        for channel in 0..audio.num_channels {
            audio.samples[channel as usize].reserve(block.duration() as usize);

            for sample in block.channel(channel as u32) {
                let normalized_sample = match audio.sample_format {
                    SampleFormat::SignedInt8 => *sample as f32 / std::i8::MAX as f32,
                    SampleFormat::SignedInt16 => *sample as f32 / std::i16::MAX as f32,
                    SampleFormat::SignedInt24 => *sample as f32 / I24_MAX as f32,
                    _ => unreachable!()
                };

                audio.samples[channel as usize].push(normalized_sample);
            }
        }
    }

    audio.num_samples = audio.samples[0].len() as u32;

    audio
}
