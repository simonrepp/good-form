use hound::WavReader;
use std::path::PathBuf;

use crate::constants::I24_MAX;
use super::{DecodedAudio, FileFormat, SampleFormat};

pub fn decode(file_path: PathBuf) -> DecodedAudio {
    let mut reader = WavReader::open(file_path).unwrap();
    let spec = reader.spec();

    let mut audio = DecodedAudio {
        file_format: FileFormat::Wav,
        num_channels: spec.channels as u8,
        num_samples: reader.duration(),
        sample_format: match spec.sample_format {
            hound::SampleFormat::Float => SampleFormat::Float32,
            hound::SampleFormat::Int => match spec.bits_per_sample {
                8 => SampleFormat::SignedInt8,
                16 => SampleFormat::SignedInt16,
                24 => SampleFormat::SignedInt24,
                32 => SampleFormat::SignedInt32,
                _ => unimplemented!()  // TODO: Verify that we cover all possible bitdepths the spec allows
            }
        },
        sample_rate: spec.sample_rate,
        samples: (0..spec.channels).map(|_| Vec::with_capacity(reader.duration() as usize)).collect()
    };

    // TODO: Verify that we handle all possible formats correctly (i.e. wasn't there something
    //       about inconsistency in signed/unsignedness depending on bits per sample used?)
    match audio.sample_format {
        SampleFormat::Float32 => {
            for (index, sample) in reader.samples::<f32>().enumerate() {
                audio.samples[index % audio.num_channels as usize].push(sample.unwrap());
            }
        }
        SampleFormat::SignedInt8 => {
            for (index, sample) in reader.samples::<i8>().enumerate() {
                let normalized_sample = sample.unwrap() as f32 / std::i8::MAX as f32;
                audio.samples[index % audio.num_channels as usize].push(normalized_sample);
            }
        }
        SampleFormat::SignedInt16 => {
            for (index, sample) in reader.samples::<i16>().enumerate() {
                let normalized_sample = sample.unwrap() as f32 / std::i16::MAX as f32;
                audio.samples[index % audio.num_channels as usize].push(normalized_sample);
            }
        }
        SampleFormat::SignedInt24 => {
            for (index, sample) in reader.samples::<i32>().enumerate() {
                let normalized_sample = sample.unwrap() as f32 / I24_MAX as f32;
                audio.samples[index % audio.num_channels as usize].push(normalized_sample);
            }
        }
        SampleFormat::SignedInt32 => {
            for (index, sample) in reader.samples::<i32>().enumerate() {
                let normalized_sample = sample.unwrap() as f32 / std::i32::MAX as f32;
                audio.samples[index % audio.num_channels as usize].push(normalized_sample);
            }
        }
        _ => unreachable!()
    }

    audio
}
