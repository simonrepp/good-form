use std::{
    path::PathBuf,
    sync::Arc,
    sync::mpsc::{self, Receiver},
    thread
};

mod flac;
mod mp3;
mod ogg;
mod wav;

pub struct DecodingThread {
    pub play_after_completion: bool,
    pub receiver: Receiver<Arc<DecodedAudio>>
}

pub struct DecodedAudio {
    pub file_format: FileFormat,
    pub num_channels: u8,
    pub num_samples: u32,
    pub sample_format: SampleFormat,
    pub sample_rate: u32,
    pub samples: Vec<Vec<f32>>
}

impl DecodedAudio {
    pub fn num_channels_humanized(&self) -> String {
        match self.num_channels {
            1 => String::from("Mono"),
            2 => String::from("Stereo"),
            _ => format!("{} Channels", self.num_channels)
        }
    }
}

pub enum FileFormat {
    Flac,
    Mp3,
    Ogg,
    Wav
}

impl FileFormat {
    pub fn humanize(&self) -> &str {
        match self {
            Self::Flac => "FLAC",
            Self::Mp3 => "MP3",
            Self::Ogg => "OGG",
            Self::Wav => "WAVE"
        }
    }
}

pub enum SampleFormat {
    Float32,
    LossilyCompressed,
    SignedInt8,
    SignedInt16,
    SignedInt24,
    SignedInt32
}

impl SampleFormat {
    pub fn humanize(&self) -> &str {
        match self {
            Self::Float32 => "Lossless 32 Bit Floating Point",
            Self::LossilyCompressed => "Lossily Compressed",
            Self::SignedInt8 => "Lossless 8 Bit Integer",
            Self::SignedInt16 => "Lossless 16 Bit Integer",
            Self::SignedInt24 => "Lossless 24 Bit Integer",
            Self::SignedInt32 => "Lossless 32 Bit Integer"
        }
    }
}

pub fn new_decode_thread(file_path: PathBuf, play_after_completion: bool) -> DecodingThread {
    let (sender, receiver) = mpsc::channel();

    thread::spawn(move || {
        let decoded_audio = match file_path.extension() {
            // TODO: Matching needs be done against lower-case extension!
            //       More ideally just carry the already determined format information
            //       over from earlier though (when we "opened it in the state module")
            Some(extension) => match extension.to_str() {
                Some("flac") => flac::decode(file_path),
                Some("mp3") => mp3::decode(file_path),
                Some("ogg") => ogg::decode(file_path),
                Some("wav") => wav::decode(file_path),
                _ => unreachable!()
            }
            None => unreachable!()
        };

        sender.send(Arc::new(decoded_audio)).ok();
    });

    DecodingThread {
        play_after_completion,
        receiver
    }
}
