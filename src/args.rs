use clap::Clap;

#[clap(version = "alpha")]
#[derive(Clap)]
pub struct Args {
    /// An optional index of an audio device to use instead of the default device.
    /// Use the --list-devices flag to look up available devices and their indices.
    #[clap(long = "device")]
    pub device_index: Option<usize>,

    /// Purge all previously persisted user settings and start with factory defaults
    #[clap(long = "factory-reset")]
    pub factory_reset: bool,

    /// An optional index of an audio host (alsa, jack, etc.) to use instead of the default host.
    /// Use the --list-hosts flag to look up available hosts and their indices.
    #[clap(long = "host")]
    pub host_index: Option<usize>,

    /// Don't start Good Form but instead print a list of available audio devices for the default host
    #[clap(long = "list-devices")]
    pub list_devices: bool,

    /// Don't start Good Form but instead print a list of available audio hosts
    #[clap(long = "list-hosts")]
    pub list_hosts: bool,

    /// A path to an audio file or directory to open right away at startup
    pub path: Option<String>
}
