use cpal::traits::{DeviceTrait, HostTrait};

pub fn list_devices() {
    let host = cpal::default_host();

    let mut devices = vec![];
    match host.output_devices() {
        Ok(output_devices) => {
            for device in output_devices {
                devices.push(device);
            }
        }
        Err(_) => panic!("Could not query the output devices for the current host.")
    }

    for (index, device) in devices.iter().enumerate() {
        println!("[{}] {}", index, device.name().unwrap());
    }
}

pub fn list_hosts() {
    let hosts = cpal::available_hosts();

    for (index, host) in hosts.iter().enumerate() {
        println!("[{}] {}", index, host.name());
    }
}
