use gfx_hal::{adapter::Adapter, Backend};
use std::{path::PathBuf, sync::Arc, time::Instant};
use winit::window::Window;

use crate::args::Args;

mod decoder;
mod downsampler;
mod exporter;
mod playback_engine;
pub mod regions;
mod renderer;

pub mod state;

use decoder::{DecodedAudio, DecodingThread};
use playback_engine::{PlaybackEngine, PlaybackResponse, PlaybackSignal};
use regions::{Button, Marker, Region, Regions};
use renderer::Renderer;
use state::{
    cursor::CursorStyle,
    location::HoveredLocation,
    locations::Locations,
    operation::Operation,
    playback_device::PlaybackDevice,
    swipe::{Swipe, SwipeDirection},
    theme::Theme,
    timecode_range::TimecodeRange,
    State
};

pub struct Engine<'a, B: Backend> {
    pub decoding_thread: Option<DecodingThread>,
    pub last_playing_instant: Instant,
    pub playback_engine: PlaybackEngine,
    pub regions: Regions,
    pub renderer: Renderer<'a, B>,
    pub state: State
}

impl<'a, B> Engine<'a, B>
where
    B: Backend
{
    pub fn advance_playhead(&mut self) {
        if self.state.playing {
            let elapsed_seconds = self.last_playing_instant.elapsed().as_nanos() as f64  / 1000000000.0;

            self.last_playing_instant = Instant::now();
            self.state.playhead_timecode += elapsed_seconds;

            if self.state.repeat {
                if let Some(ref selected_range) = self.state.selected_range {
                    let end_overshoot = self.state.playhead_timecode - selected_range.later_end();

                    if end_overshoot > 0.0 {
                        self.state.playhead_timecode = if selected_range.length() > 0.0 {
                             selected_range.earlier_end() + end_overshoot.rem_euclid(selected_range.length())
                        } else {
                            selected_range.earlier_end()
                        };

                        self.signal_playback(PlaybackSignal::Seek(self.state.playhead_timecode));
                    }
                } else {
                    self.state.repeat = false;
                }
            }

            if !self.state.repeat && self.state.playhead_timecode > self.state.end_timecode {
                if !self.play_queued() {
                    self.state.playhead_timecode = self.state.end_timecode;
                    self.state.playing = false;
                    self.signal_playback(PlaybackSignal::Pause);
                }
            }
        }
    }

    pub fn ascend_location(&mut self) {
        if self.state.locations.ascend() {
            self.update_text();
        }
    }

    pub fn begin_moving_marker(&mut self, marker: Marker) {
        self.state.operation = Operation::MovingMarker(marker);
    }

    pub fn begin_pressing_button(&mut self, button: Button) {
        self.state.operation = Operation::PressingButton(button);
    }

    pub fn begin_seeking(&mut self) {
        self.state.operation = Operation::Seeking;
        self.seek_to_cursor();
    }

    pub fn begin_selecting(&mut self) {
        self.state.operation = Operation::Selecting;
        self.state.selected_range = Some(TimecodeRange::start_at(self.cursor_timecode()));
        self.regions.update_selection_markers(&self.state);
    }

    pub fn begin_transport_swipe(&mut self) {
        let swipe = Swipe::start_at(&self.state.cursor);
        self.state.operation = Operation::TransportSwipe(swipe);
    }

    pub fn end_moving_marker(&mut self) {
        self.state.operation = Operation::None;
    }

    pub fn end_pressing_button(&mut self) {
        self.state.operation = Operation::None;
    }

    fn cursor_timecode(&mut self) -> f64 {
        let mouse_x_clamped = self.state.cursor.x.max(0.0).min(self.state.window_size.width as f32);

        (mouse_x_clamped / self.state.window_size.width as f32) as f64 * self.state.end_timecode
    }

    pub fn decrease_interface_scaling(&mut self) {
        self.state.interface_scaling = (self.state.interface_scaling - 0.1).max(1.0);

        self.regions.update_proportions(&self.state);
        self.renderer.update_geometry(&self.regions);
    }

    pub fn descend_location(&mut self) {
        if self.state.locations.descend() {
            self.update_text();
        }
    }

    pub fn discard_hovered_location(&mut self) {
        self.state.hovered_location = None;

        self.update_text();
    }

    fn eject_current_file(&mut self) {
        self.signal_playback(PlaybackSignal::Eject);

        self.state.audio = None;
        self.state.end_timecode = 0.0;
        self.state.pause_point_timecode = None;
        self.state.play_point_timecode = None;
        self.state.playhead_timecode = 0.0;
        self.state.playing = false;
        self.state.repeat = false;
        self.state.selected_range = None;

        self.regions.update_markers(&self.state);
    }

    pub fn end_seeking(&mut self) {
        self.state.operation = Operation::None;
    }

    pub fn end_selecting(&mut self) {
        self.state.operation = Operation::None;

        if let Some(ref range) = self.state.selected_range {
            if range.earlier_end() == range.later_end() {
                self.state.selected_range = None;
                self.regions.update_selection_markers(&self.state);
            }
        }
    }

    /// We only end the swipe if the currently active swipe
    /// is the swipe who is requested to be ended
    ///
    /// In other words: A swipe may be started ("A"), but
    /// before it ends another swipe could be started ("B"),
    /// so when there is a request to end A we just ignore it,
    /// because B and only B is now the active swipe.
    pub fn end_transport_swipe(&mut self) {
        if let Operation::TransportSwipe(ref swipe) = self.state.operation {
            let new_playhead_timecode = match swipe.direction {
                SwipeDirection::Left => match self.state.play_point_timecode {
                    Some(timecode) => Some(timecode),
                    None => Some(0.0)
                }
                SwipeDirection::Right => match self.state.pause_point_timecode {
                    Some(timecode) => Some(timecode),
                    None => Some(self.state.end_timecode)
                }
                SwipeDirection::Up => Some(0.0),
                SwipeDirection::Down => Some(self.state.end_timecode),
                SwipeDirection::None => None
            };

            if let Some(timecode) = new_playhead_timecode {
                self.seek_to_timecode(timecode)
            }

            self.state.operation = Operation::None;
        }
    }

    pub fn export(&mut self) {
        if let (Some(audio), Some(range)) = (&self.state.audio, &self.state.selected_range) {
            exporter::export(audio, range, &self.state.locations);

            self.state.locations.refresh_current_directory();
            self.update_text();
        }
    }

    pub fn increase_interface_scaling(&mut self) {
        self.state.interface_scaling = (self.state.interface_scaling + 0.1).min(2.0);

        self.regions.update_proportions(&self.state);
        self.renderer.update_geometry(&self.regions);
    }

    /// Initializes the global application state by setting up
    /// static defaults, as well as handling the optional path
    /// argument (either a file or directory) for opening a
    /// file or directory right at startup.
    pub fn initialize(
        instance: Option<B::Instance>,
        surface: B::Surface,
        adapter: Adapter<B>,
        args: Args
    ) -> Engine<'a, B> {
        let state = State::initialize(args);
        let regions = Regions::intialize(&state);
        let renderer = Renderer::new(instance, surface, adapter, &regions, &state);

        let mut engine = Engine {
            decoding_thread: None,
            last_playing_instant: Instant::now(),
            playback_engine: playback_engine::initialize(state.playback_device, None, false),
            regions,
            renderer,
            state
        };

        if let Some(ref path) = engine.state.locations.current_file {
            let path = path.to_path_buf();
            engine.open(path);
        }

        engine.update_text();

        engine
    }

    pub fn jump_seconds_backward(&mut self, seconds: u32) {
        self.seek_to_timecode((self.state.playhead_timecode - seconds as f64).max(0.0));
    }

    pub fn jump_seconds_forward(&mut self, seconds: u32) {
        self.seek_to_timecode((self.state.playhead_timecode + seconds as f64).min(self.state.end_timecode));
    }

    pub fn jump_to_end(&mut self) {
        self.seek_to_timecode(self.state.end_timecode);
    }

    pub fn jump_to_start(&mut self) {
        self.seek_to_timecode(0.0);
    }

    pub fn load_audio(&mut self, audio: Arc<DecodedAudio>, autoplay: bool) {
        self.state.end_timecode = audio.num_samples as f64 / audio.sample_rate as f64;

        if let PlaybackEngine::Online {
            num_channels,
            sample_rate,
            ..
        } = self.playback_engine {
            if audio.num_channels != num_channels ||
               audio.sample_rate != sample_rate {
                self.signal_playback(PlaybackSignal::Terminate);
                // TODO: Consider joining threads (waiting for the playback thread to terminate)
                //       before we intialize a new one to prevent weird concurrency issues.
                self.playback_engine = playback_engine::initialize(self.state.playback_device, Some(audio.clone()), self.state.playing);
            } else {
                self.signal_playback(PlaybackSignal::Use(audio.clone()));
            }

            if autoplay {
                self.state.playing = true;
                self.last_playing_instant = Instant::now();

                self.signal_playback(PlaybackSignal::Play);
            }
        }

        self.state.audio = Some(audio);
    }

    pub fn move_marker_to_cursor(&mut self, marker: Marker) {
        let cursor_timecode = self.cursor_timecode();

        match marker {
            Marker::PlayPoint => {
                self.state.play_point_timecode.replace(cursor_timecode);
                self.regions.update_transport_markers(&self.state);
            }
            Marker::PausePoint => {
                self.state.pause_point_timecode.replace(cursor_timecode);
                self.regions.update_transport_markers(&self.state);
            }
            Marker::SelectionEnd => {
                if let Some(range) = &mut self.state.selected_range {
                    range.set_later_end(cursor_timecode);
                    self.regions.update_selection_markers(&self.state);
                }
            }
            Marker::SelectionStart => {
                if let Some(range) = &mut self.state.selected_range {
                    range.set_earlier_end(cursor_timecode);
                    self.regions.update_selection_markers(&self.state);
                }
            }
        }
    }

    pub fn open(&mut self, path: PathBuf) -> bool {
        if path.is_dir() {
            self.state.locations.open_directory(path);

            false
        } else if self.state.locations.open_file(path) {
            self.decoding_thread = Some(decoder::new_decode_thread(
                self.state.locations.current_file.as_ref().unwrap().to_path_buf(),
                self.state.playing
            ));

            self.eject_current_file();

            true
        } else {
            false
        }
    }

    pub fn open_hovered_location(&mut self) {
        if let Some(location) = self.state.hovered_location.take() {
            if location.supported {
                // TODO: open(...) will do the "supported" checks again, can be optimized
                self.open(location.path);
            }
        }

        self.update_text();
    }

    pub fn open_selected_location(&mut self) {
        if self.state.locations.open_selected() {
            self.eject_current_file();

            self.decoding_thread = Some(decoder::new_decode_thread(
                self.state.locations.current_file.as_ref().unwrap().to_path_buf(),
                true
            ));

            self.state.locations.queue.clear();
        }

        // TODO: As an optimization only do this when needed
        self.update_text();
    }

    pub fn open_selected_location_and_queue_following(&mut self) {
        if self.state.locations.open_selected() {
            self.eject_current_file();

            self.decoding_thread = Some(decoder::new_decode_thread(
                self.state.locations.current_file.as_ref().unwrap().to_path_buf(),
                true
            ));

            self.state.locations.queue_following();
        }

        // TODO: As an optimization only do this when needed
        self.update_text();
    }

    pub fn pause(&mut self) {
        self.state.pause_point_timecode = Some(self.state.playhead_timecode);
        self.state.playing = false;
        self.state.repeat = false;

        self.regions.update_transport_markers(&self.state);

        self.signal_playback(PlaybackSignal::Pause);
    }

    pub fn persist_state(&mut self) {
        self.state.persist();
    }

    pub fn play(&mut self) {
        self.state.play_point_timecode = Some(self.state.playhead_timecode);

        if self.state.play_point_timecode == self.state.pause_point_timecode {
            self.state.pause_point_timecode = None;
        }

        self.regions.update_transport_markers(&self.state);

        self.state.playing = true;
        self.last_playing_instant = Instant::now();

        self.signal_playback(PlaybackSignal::Play);
    }

    pub fn play_and_repeat(&mut self) {
        if self.state.selected_range.is_none() {
            let (start, end) = if let Some(play_point_timecode) = self.state.play_point_timecode {
                let pause_point_or_playhead_timecode = self.state.pause_point_timecode.unwrap_or(self.state.playhead_timecode);
                (play_point_timecode, pause_point_or_playhead_timecode)
            } else {
                (0.0, self.state.end_timecode)
            };

            self.state.selected_range = Some(TimecodeRange::define(start, end));
            self.regions.update_selection_markers(&self.state);
        }

        self.state.playhead_timecode = self.state.selected_range.as_ref().unwrap().earlier_end();

        if self.state.playing {
            self.state.play_point_timecode = Some(self.state.playhead_timecode);

            if self.state.play_point_timecode == self.state.pause_point_timecode {
                self.state.pause_point_timecode = None;
            }

            self.regions.update_transport_markers(&self.state);
        } else {
            self.play();
        }

        self.state.repeat = true;

        self.signal_playback(PlaybackSignal::SeekAndPlay(self.state.playhead_timecode));
    }

    pub fn play_from_marker(&mut self, marker: Marker) {
        let marker_timecode = match marker {
            Marker::PlayPoint => self.state.play_point_timecode,
            Marker::PausePoint => self.state.pause_point_timecode,
            Marker::SelectionEnd => self.state.selected_range.as_ref().map(|range| range.later_end()),
            Marker::SelectionStart => self.state.selected_range.as_ref().map(|range| range.earlier_end())
        };

        if let Some(timecode) = marker_timecode {
            self.seek_to_timecode(timecode);

            self.state.play_point_timecode = Some(self.state.playhead_timecode);

            if self.state.play_point_timecode == self.state.pause_point_timecode {
                self.state.pause_point_timecode = None;
            }

            self.regions.update_transport_markers(&self.state);

            if !self.state.playing {
                self.state.playing = true;
                self.last_playing_instant = Instant::now();

                self.signal_playback(PlaybackSignal::Play);
            }
        }
    }

    fn play_queued(&mut self) -> bool {
        while let Some(path) = self.state.locations.queue.pop_front() {
            // TODO: This needlessly checks if the path is a directory,
            //       refactor this general area generously at some point.
            if self.open(path) {
                return true;
            }
        }

        false
    }

    pub fn queue_selected_file(&mut self) {
        self.state.locations.queue_selected();

        self.update_text();
    }

    /// If there is pending decoding going on, this checks if a result has been sent
    /// through the message passing channel and receives it if that's the case.
    ///
    /// Returns true if decoded audio was received in order to signal the caller to
    /// update interface text (this might go away soon though due to changing responsibilities)
    pub fn receive_decoded(&mut self, window: &Window) {
        if let Some(ref thread) = self.decoding_thread {
            if let Ok(decoded) = thread.receiver.try_recv() {
                let autoplay = thread.play_after_completion;

                let peaks = downsampler::downsample(&decoded.samples);
                // TODO: We seem to get only 4092 (4095?) pairs instead of 4096 (not sure if still applies?)
                self.renderer.update_waveform_image(peaks);

                self.load_audio(decoded, autoplay);

                self.update_text();

                window.set_title(&self.window_title());
            }
        }
    }

    pub fn receive_playback_responses(&mut self) {
        if let PlaybackEngine::Online {
            ref mut device_name,
            ref response_receiver,
            ..
        } = self.playback_engine {
            if let Ok(response) = response_receiver.try_recv() {
                match response {
                    PlaybackResponse::Crashed(message) => {
                        self.playback_engine = PlaybackEngine::Crashed(message);
                    }
                    PlaybackResponse::Online(message) => {
                        *device_name = message;
                    }
                }
            }
        }
    }

    pub fn register_hovered_location(&mut self, path: PathBuf) {
        let supported = path.is_dir() || Locations::allowed_extension(&path);

        self.state.hovered_location = Some(HoveredLocation { path, supported });

        self.update_text();
    }

    pub fn render(&mut self) {
        self.renderer.render(&self.playback_engine, &self.regions, &self.state);
    }

    pub fn restart_playback_engine(&mut self, use_defaults: bool) {
        if use_defaults {
            self.state.playback_device = PlaybackDevice::Default;
        }

        if let PlaybackEngine::Online { .. } = self.playback_engine {
            self.signal_playback(PlaybackSignal::Terminate);
            self.playback_engine = PlaybackEngine::Offline;

            if use_defaults {
                self.playback_engine = playback_engine::initialize(self.state.playback_device, self.state.audio.clone(), self.state.playing);
                self.signal_playback(PlaybackSignal::Seek(self.state.playhead_timecode));
            }
        } else {
            self.playback_engine = playback_engine::initialize(self.state.playback_device, self.state.audio.clone(), self.state.playing);
            self.signal_playback(PlaybackSignal::Seek(self.state.playhead_timecode));
        }
    }

    pub fn seek_to_timecode(&mut self, timecode: f64) {
        self.state.playhead_timecode = timecode;
        self.signal_playback(PlaybackSignal::Seek(self.state.playhead_timecode));
    }

    pub fn seek_to_cursor(&mut self) {
        self.state.playhead_timecode = self.cursor_timecode();
        self.signal_playback(PlaybackSignal::Seek(self.state.playhead_timecode));
    }

    pub fn select_to_cursor(&mut self) {
        let cursor_timecode = self.cursor_timecode();

        if let Some(ref mut range) = self.state.selected_range {
            range.extend_to(cursor_timecode);
        } else {
            self.state.selected_range = Some(TimecodeRange::start_at(cursor_timecode));
        }

        self.regions.update_selection_markers(&self.state);
    }

    pub fn select_next_location(&mut self) {
        if self.state.locations.select_next() {
            self.update_text();
        }
    }

    pub fn select_previous_location(&mut self) {
        if self.state.locations.select_previous() {
            self.update_text();
        }
    }

    fn signal_playback(&mut self, signal: PlaybackSignal) {
        if let PlaybackEngine::Online {
            ref signal_sender,
            ..
        } = self.playback_engine {
            signal_sender.send(signal).ok();
        }
    }

    pub fn stop_playing(&mut self) {
        if self.state.playing {
            self.pause();
        }

        self.jump_to_start();
    }

    pub fn toggle_playing(&mut self) {
        if self.state.playing {
            self.pause();
        } else {
            self.play();
        }
    }

    pub fn toggle_theme(&mut self) {
        self.state.theme = match self.state.theme {
            Theme::Dark => Theme::Light,
            Theme::Light => Theme::Dark
        };

        self.renderer.update_theme(&self.state);
    }

    pub fn update_cursor_position(&mut self, x: f32, y: f32) {
        self.state.cursor.region = self.regions.region_at(x, y);
        self.state.cursor.x = x;
        self.state.cursor.y = y;

        if let Operation::TransportSwipe(ref mut swipe) = self.state.operation {
            swipe.update(&self.state.cursor);
        }
    }

    pub fn update_cursor_style(&mut self) -> Option<CursorStyle> {
        let new_style = match self.state.operation {
            Operation::None => match self.state.cursor.region {
                Region::Button(_) => CursorStyle::HoveringButton,
                Region::Marker(_) => CursorStyle::HoveringMarker,
                _ => CursorStyle::Pointing
            }
            Operation::MovingMarker(_) => CursorStyle::MovingMarker,
            Operation::Selecting => CursorStyle::SelectingRange,
            Operation::TransportSwipe(ref swipe) => match swipe.direction {
                SwipeDirection::Left => CursorStyle::SwipingLeft,
                SwipeDirection::Right => CursorStyle::SwipingRight,
                SwipeDirection::Up => CursorStyle::SwipingUp,
                SwipeDirection::Down => CursorStyle::SwipingDown,
                SwipeDirection::None => CursorStyle::Pointing
            }
            _ => CursorStyle::Pointing
        };

        if new_style != self.state.cursor.style {
            self.state.cursor.style = new_style.clone();

            Some(new_style)
        } else {
            None
        }
    }

    pub fn update_text(&mut self) {
        if let Some(ref location) = self.state.hovered_location {
            let line = if location.supported {
                "Drop the directory/file to open it".to_string()
            } else {
                "The file type you are dragging is not supported".to_string()
            };

            self.renderer.update_text(vec![line]);
        } else {
            let current_file = match self.state.locations.current_file {
                Some(ref path) => path.file_name().unwrap().to_str().unwrap(),
                None => "-"
            };

            let metadata = match self.state.audio {
                Some(ref audio) => format!(
                    "[{}Hz {} {} {}]",
                    audio.sample_rate,
                    audio.num_channels_humanized(),
                    audio.file_format.humanize(),
                    audio.sample_format.humanize()
                ),
                None => String::from("")
            };

            let queued = match self.state.locations.queue.len() {
                0 => String::from(""),
                1 => format!(
                    " Queued: {}",
                    self.state.locations.queue[0].file_name().unwrap().to_str().unwrap()
                ),
                _ => format!(
                    " Queued: {} and {} more",
                    self.state.locations.queue[0].file_name().unwrap().to_str().unwrap(),
                    self.state.locations.queue.len() - 1
                ),
            };

            let mut lines = vec![
                format!(" Current file: {} {}", current_file, metadata),
                queued,
                "".to_string(),
                format!(" Browsing {}", self.state.locations.current_directory.to_str().unwrap()),
                " ------------".to_string()
            ];

            let skip_index = (self.state.locations.selected_location.unwrap_or(0) as i16 - 31).max(0);

            for (index, location) in self.state.locations.available_locations.iter().enumerate().skip(skip_index as usize).take(32) {
                let selected = match self.state.locations.selected_location {
                    Some(selected_index) => if index == selected_index {
                        "> "
                    } else {
                        "  "
                    }
                    None => "  "
                };

                lines.push(format!("{} {}", selected, location.file_name()));
            }

            self.renderer.update_text(lines);
        }
    }

    pub fn update_window_size(&mut self, width: u32, height: u32) {
        self.state.window_size.width = width;
        self.state.window_size.height = height;

        self.regions.update_all(&self.state);
        self.renderer.resize(&self.regions, &self.state);
    }

    pub fn window_title(&self) -> String {
        if let Some(ref current_file) = self.state.locations.current_file {
            let file_name = current_file.file_name().unwrap().to_str().unwrap();

            format!("{}  –  Good Form", file_name)
        } else {
            "Good Form".to_string()
        }
    }
}
