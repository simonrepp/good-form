# Good Book

The user manual for the Good Form audio player.

![A screenshot of Good Form's interface](https://simonrepp.com/good-form/screenshot.png)
