## Commandline arguments

### Help

Run this to print available flags and options to the console:

```
good-form --help
```

### Opening a file or folder

Just pass a single positional argument to open Good Form with a specific file or directory:

```
good-form path/to/my/file.wav
```

### Listing audio hosts and devices

Run this to list available hosts (ALSA, CoreAudio, WASAPI, etc.):

```
good-form --list-hosts
```

Run this to list available devices (Sound cards, Screens, etc.):

```
good-form --list-devices
```

### Configuring audio hosts and devices

With the output `--list-hosts` and/or `--list-devices` gives you, you can instruct Good Form to start with a specific playback configuration, utilizing the indices displayed in the listing output. Let's say for instance `--list-devices` gave us this listing:

```
[0] default
[1] pulse
[2] sysdefault:CARD=Pro71992192
[3] sysdefault:CARD=PCH
[4] front:CARD=PCH,DEV=0
```

Then we can use the following argument to use the `pulse` device:

```
good-form --device 1
```

Similarly if we query the hosts with `--list-hosts` and get this listing:

```
[0] ALSA
[1] EXAMPLE
```

Then we can the following argument to use the `ALSA` device:

```
good-form --host 0
```

### Factory reset

Use this flag to purge all previously persisted user settings and start with factory defaults:

```
good-form --factory-reset
```
