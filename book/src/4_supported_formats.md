## Supported Formats

At present Good Form supports the following formats:
- Free Lossless Audio Codec – `flac`
- MPEG-1/2 Audio Layer III – `mp3`
- Ogg Vorbis – `ogg`
- Waveform Audio File Format – `wav`

Within these formats all common bit depth/sample rate combinations should work. You might encounter issues with uncommon sample formats, you can help to correct those by submitting a detailed report on the [issue tracker](https://codeberg.org/suchbliss/good-form).
