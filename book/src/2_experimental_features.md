## Experimental Features

### Interface Scaling

Note that this is **not yet fully implemented** and will currently just look weird for the most part.

- Press `Ctrl` + `+` to increase the size of all user interface elements
- Press `Ctrl` + `-` to decrease the size of all user interface elements
