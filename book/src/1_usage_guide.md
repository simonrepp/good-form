## Usage Guide

### Quick start

Drag and drop a directory or file onto the window to open it!

### Transport and playhead

- Press `Space` or click the `Play/Pause Button` in the top bar to play/pause
- Press the `Stop Button` in the top bar to stop and move the playhead to the start
- `Left-click` or `Left-drag` anywhere around the waveform to reposition the playhead
- Press `Arrow Right` and `Arrow Left` to jump forward/backward, additionally hold `Ctrl` to make larger jumps
- Press `Home` and `End` to jump to the start/end
- Hold `Tab`, move the mouse in some direction and release `Tab` to reposition the playhead
  - `Left` -> Last play point
  - `Right` -> Last pause point
  - `Up` -> Start
  - `Down` -> End

### Play/Pause Points

- A play point marker is automatically placed whenever you start playback
- A pause point marker is automatically placed Whenever you pause playback
- `Left-drag` a play or pause point marker to reposition it
- `Double-click` a play or pause point marker to play from its position

### Selections

- `Shift` + `Left-drag` to define a selection range
- `Left-drag` the selection end markers (`[`/`]`) to adjust the selection
- Press `R` to play the selected range on repeat (loop)
  - If there is no selection range one is set automatically based on the play/pause points
  - If there are no play/pause points the entire time range is selected
- `Double-click` selection end markers to play from their position
- `Shift` + `Left-click` anywhere to discard an existing selection

### Exporting selections

Define a selection, then press `Ctrl` + `E` to export it to a 24bit WAV file in the same folder

### Navigation and Queue

- Use `⬇` and `⬆` to select a directory or file in the filetree
- Use `Alt` + `⬆`/`⬇` to ascend/descend in the filetree
- Press `Enter` to play the selected file or open the selected directory
- Press `Shift` + `Enter` to play the selected file and queue all that follow in the directory
- Press `Q` to queue the currently selected file

### Playback Engine

- Press `F5` to toggle the sound playback engine on or off
- Press `Shift` + `F5` to revert to the default audio device and restart the engine

### Theme

- Press `T` to toggle between the dark and light theme
