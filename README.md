# Good Form

A cross-platform audio player with a waveform-centric interface

![A screenshot of Good Form's interface](https://simonrepp.com/good-form/screenshot.png)

## Research & Development

Good Form is presently in alpha. It already offers the solid technical foundation and base functionality of an audio player and works very reliably on compatible systems. However due to limited user testing as of now there might just as well be systems on which it will entirely refuse to run. The user interface does not yet represent anything of what Good Form intends to be - this is for now just a usable foundation to work from.

## Design philosophy

Good Form positions itself somewhere between a professional DAW and a casual player - a *professional player* probably. It is designed for intent listening and workflows such as browsing samples, evaluating recordings, studying musical pieces, analyzing audio material ... and at the end of the day enjoying music with a beautiful player.


## Technological and political mission

Good Form is solidly engineered, cross-platform software, written in Rust. It is built around modern graphics (Vulkan, Metal) and also runs on older systems through OpenGL. It is free and open source software and always will be.

## User Manual

All available features are documented in the [Good Book](https://simonrepp.com/good-form/book/). Given that much of Good Form's featureset is for now only available through keyboard shortcuts this is a must read if you're interested.

## Download

Currently you can choose from two experimental snapshot packages provided for debian-based (Debian, Ubuntu) and rpm-based (CentOS, Fedora, RHEL) 64bit systems,
both of which are configured for modern graphics only (your system needs to support Vulkan).

For now please **use at your own risk and anticipate that the package might not yet work for you.**\
Note: You can make a valuable contribution by reporting issues you run into on your system!

Still with me? Awesome!

[Experimental Debian/Ubuntu 64bit .deb snapshot package](https://simonrepp.com/good-form/good-form_alpha-snapshot_amd64.deb)\
[Experimental CentOS/Fedora/RHEL 64bit .rpm snapshot package](https://simonrepp.com/good-form/good-form-alpha-snapshot.x86_64.rpm)

## Building Good Form

You will most likely need to install dependencies, see further below for instructions.

### Linux/Windows with support for Vulkan

```
cargo build --features=vulkan --release
```

### macOS with support for Metal

```
cargo build --features=metal --release
```

### Platforms that only support OpenGL

```
cargo build --features=gl --release
```

Note that this currently only compiles, but does not run due to unresolved issues in the `gfx-backend-gl` crate. I am currently in touch with the gfx-rs developers to work out solutions. You can follow up on progress with the most recent blocker in this [issue on GitHub](https://github.com/gfx-rs/gfx/issues/3192).

## Dependencies

This is a best-effort listing of dependencies that might be required on
different systems, along with installation instructions.

### macOS Catalina

You need `cmake` installed, for this you can for instance install [Homebrew](https://brew.sh) and then run:

```
brew install cmake
```

### Ubuntu 19.10

Just install all dependencies through the apt package manager:

```
sudo apt install cmake libasound2-dev libexpat-dev libfreetype6-dev libxml2-dev libx11-dev python3-distutils
```

### Windows 10

You need to install CMake and Ninja, for instance through the [CMake installer](https://cmake.org/download/) and [Chocolatey Ninja package](https://github.com/ninja-build/ninja/wiki/Pre-built-Ninja-packages).

## Building Good Book

The markdown files that comprise the documentation for Good Form in the [Good Book](https://simonrepp.com/good-form/book/) can be edited in the `book/src/` directory.

To compile the book first install mdBook through cargo:

```
cargo install mdbook
```

Then run the following command inside the `book/` directory:

```
mdbook serve --open
```

The compiled book is written to `target/book/`, see the [mdBook documentation](https://rust-lang.github.io/mdBook/) for detailed information regarding the build process.
