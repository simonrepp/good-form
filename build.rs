use gfx_hal::pso;
use lyon::math::Point;
use lyon::tessellation::*;
use regex::Regex;
use serde::Serialize;
use shaderc::{Compiler, ShaderKind};
use std::{
    env,
    fs,
    io::Cursor,
    mem,
    ops::Range,
    slice
};

const ICONS_DIR: &'static str = "src/engine/renderer/icons";
const SHADERS_DIR: &'static str = "src/engine/renderer/shaders";

#[derive(Copy, Clone)]
struct MyVertex {
    position: [f32; 2]
}

#[derive(Serialize)]
struct Icons {
    vertex_range_icon_engine: Range<u32>,
    vertex_range_icon_pause: Range<u32>,
    vertex_range_icon_play: Range<u32>,
    vertex_range_icon_selection_end: Range<u32>,
    vertex_range_icon_selection_start: Range<u32>,
    vertex_range_icon_stop: Range<u32>,
    vertices: Vec<AttributeXyRgba>
}

#[derive(Serialize)]
#[repr(C)]
pub struct AttributeXyRgba {
    a_xy: [f32; 2],
    a_rgba: [u8; 4]
}

/// Compiles all GLSL shaders in SHADER_DIRECTORY into SPIR-V format.
/// The compiled binary .spv shader files are exported to OUT_DIR.
fn compile_shaders() {
    let mut compiler = Compiler::new().unwrap();
    let out_dir = env::var("OUT_DIR").unwrap();

    for shader in fs::read_dir(SHADERS_DIR).unwrap() {
        let path = shader.unwrap().path();
        let filename = path.file_name().unwrap().to_str().unwrap();
        let extension = path.extension().unwrap().to_str().unwrap();

        let shader_kind = match extension {
            "frag" => ShaderKind::Fragment,
            "vert" => ShaderKind::Vertex,
            _ => panic!("Unsupported extension found on shader {}", filename)
        };

        let glsl = fs::read_to_string(&path).unwrap();
        let spirv = match compiler.compile_into_spirv(&glsl, shader_kind, filename, "main", None) {
            Ok(binary) => binary,
            Err(error) => panic!("{:?}", error)
        };

        let aligned_spirv = pso::read_spirv(Cursor::new(&spirv.as_binary_u8()[..])).unwrap();

        let aligned_spirv_u8: &[u8] = unsafe {
            slice::from_raw_parts(
                aligned_spirv.as_ptr() as *const u8,
                aligned_spirv.len() * mem::size_of::<u32>(),
            )
        };

        let target = format!("{}/{}.spv", out_dir, filename);

        fs::write(target, aligned_spirv_u8).unwrap();
    }
}

fn triangulate_icons() {
    let mut vertices = vec![];
    let vertex_range_icon_engine = triangulate_icon("engine", &mut vertices);
    let vertex_range_icon_pause = triangulate_icon("pause", &mut vertices);
    let vertex_range_icon_play = triangulate_icon("play", &mut vertices);
    let vertex_range_icon_selection_end = triangulate_icon("selection_end", &mut vertices);
    let vertex_range_icon_selection_start = triangulate_icon("selection_start", &mut vertices);
    let vertex_range_icon_stop = triangulate_icon("stop", &mut vertices);

    let icons = Icons {
        vertex_range_icon_engine,
        vertex_range_icon_pause,
        vertex_range_icon_play,
        vertex_range_icon_selection_end,
        vertex_range_icon_selection_start,
        vertex_range_icon_stop,
        vertices
    };

    let serialized = bincode::serialize::<Icons>(&icons).unwrap();

    let out_dir = env::var("OUT_DIR").unwrap();
    let icons_file = format!("{}/icons.bincode", out_dir);

    fs::write(icons_file, &serialized).unwrap();
}

/// The svg-represented icons are assumed to be a single path (where the d="..."
/// attribute is extracted to produce the triangulated geometry from), and the
/// document size - and especially and more importantly the viewBox - is assumed to
/// be 1000 units wide (note that this may be represented as "1e3" as well),
/// so the scaling is correctly reproduced upon importing the geometry.
fn triangulate_icon(name: &str, vertices: &mut Vec<AttributeXyRgba>) -> Range<u32> {
    let svg_path_regex = Regex::new("<path d=\"([^\"]+)\"\\s*/>").unwrap();

    let path = format!("{}/{}.svg", ICONS_DIR, name);

    let svg = fs::read_to_string(&path).unwrap();
    let svg_path = svg_path_regex.captures(&svg)
                                 .unwrap()
                                 .get(1)
                                 .unwrap()
                                 .as_str();

     let svg_builder = lyon::path::Path::builder().with_svg();
     let lyon_path = lyon_svg::path_utils::build_path(svg_builder, svg_path);

     let mut geometry: VertexBuffers<MyVertex, u16> = VertexBuffers::new();
     let mut tessellator = FillTessellator::new();

     tessellator.tessellate_path(
         &lyon_path.unwrap(),
         &FillOptions::default(),
         &mut BuffersBuilder::new(&mut geometry, |position: Point, _: FillAttributes| {
             MyVertex {
                 position: position.to_array()
             }
         }),
     ).unwrap();

     let previous_vertices_len = vertices.len() as u32;

     for index in geometry.indices {
         vertices.push(AttributeXyRgba {
            a_xy: [
                geometry.vertices[index as usize].position[0] / 1000.0,
                geometry.vertices[index as usize].position[1] / 1000.0
            ],
            a_rgba: [255, 255, 255, 255]
        });
     }

     previous_vertices_len..vertices.len() as u32
}

fn main() {
    compile_shaders();
    triangulate_icons();
}
